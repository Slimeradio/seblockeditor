# SEBlockEditor

This program was originaly made by **mi-ctan** and further supported by **slimeradio**

Compiled version stays here: out\artifacts\SEBlockEditor_jar\SEBlockEditor.jar

To run this program you need exactly Java 8 or higher.

How to use program:

https://youtu.be/UW151bou80o

https://youtu.be/1xkTPNX6feY

https://youtu.be/UhSI6MJHYmc

https://youtu.be/uyKmIR8VigQ


# Command line

```
java -jar C:\Users\slime\Desktop\seblockeditor-master\out\artifacts\SEBlockEditor_jar\SEBlockEditor.jar 
CSVD \t 
loadSaveCSV B2!K73:BW1189 .\components.txt https://docs.google.com/spreadsheets/d/1bNGUpMtLWU...EjIopnd2o9THD_aw/edit#gid=434988878
importfile C:\Program Files (x86)\Steam\steamapps\common\SpaceEngineers\Content\Data\CubeBlocks\CubeBlocks_Production.sbc 
setComponentsPriorities .\priors.txt 
editCrafts .\components.txt 
editProperties .\props.txt 
exportfile C:\Program Files (x86)\Steam\steamapps\common\SpaceEngineers\Content\Data\CubeBlocks\CubeBlocks_Production.sbc
```
CSVD - sets delimiter of csv file from , to \t 

loadSaveCSV - load csv from google spreadsheets from address "https://docs.google.com/spreadsheets/d/1bNGUpMtLWU...EjIopnd2o9THD_aw/edit#gid=434988878" with sheet name = "B2" and A1 range = "K73:BW1189" and save his to ".\components.txt". 
This command requires presence GoogleLoader from jar directory (C:\Users\slime\Desktop\seblockeditor-master\out\artifacts\SEBlockEditor_jar\GoogleLoader\GoogleLoader.jar and other)