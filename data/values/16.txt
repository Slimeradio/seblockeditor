typeId / subtypeId	MaxPowerOutput	RequiredPowerInput	MaxStoredPower
LargeBlockBatteryBlock	6.00	18.00	1.50
SmallBlockBatteryBlock	1.00	3.00	0.25
SlimBattery	0.35	1.04	0.09
SmallBlockSmallBatteryBlock	0.05	0.5	0.05
LargeBlockNanoBattery	0.05	0.5	0.05