package helpers

import org.w3c.dom.Document
import org.w3c.dom.Element
import org.w3c.dom.Node
import org.w3c.dom.bootstrap.DOMImplementationRegistry
import org.w3c.dom.ls.DOMImplementationLS
import org.w3c.dom.ls.LSOutput
import org.xml.sax.InputSource
import org.xml.sax.SAXParseException
import java.io.*
import java.nio.charset.Charset
import javax.xml.parsers.DocumentBuilderFactory
import javax.xml.transform.TransformerException
import javax.xml.transform.TransformerFactory
import javax.xml.transform.dom.DOMSource
import javax.xml.transform.stream.StreamResult


val documentFactory = DocumentBuilderFactory.newInstance()

inline fun parseXML(string: String) = try{
    documentFactory.newDocumentBuilder().parse(InputSource(StringReader(string)))
} catch (e: SAXParseException){
    println("cannot parse xml:\n$string")
    throw e
}

inline fun emptyDocument() = documentFactory.newDocumentBuilder().newDocument()

inline fun Element.child(name: String) = this.getElementsByTagName(name).let {
    if(it.length > 0){
        it.item(0) as Element
    } else {
        null
    }
}

inline fun Element.children(name: String) = this.getElementsByTagName(name).let {
    object : Iterator<Element>{
        var idx = 0
        override fun hasNext(): Boolean = idx < it.length

        override fun next(): Element {
            idx++
            return it.item(idx - 1) as Element
        }
    }
}

inline fun Node.eachChildren(block: (Node) -> Unit){
    childNodes.run {
        for (i in 0 until length){
            block(item(i))
        }
    }
}

inline fun Node.firstChildren(predicate: (Node) -> Boolean): Node?{
    childNodes.run {
        for (i in 0 until length){
            val item = item(i)
            if(predicate(item)){
                return item
            }
        }
    }
    return null
}

inline fun<reified T: Node> Node.firstChildrenByType(predicate: (T) -> Boolean): T?{
    childNodes.run {
        for (i in 0 until length){
            val item = item(i)
            if(item is T && predicate(item)){
                return item
            }
        }
    }
    return null
}

inline fun <T> Element.attr(name: String, parse: (String) -> T): T? {
    return if(this.hasAttribute(name)) {
        this.getAttribute(name)?.let(parse)
    } else {
        null
    }
}

inline fun <T> Element.value(parse: (String) -> T) = this.textContent.let(parse)//???

inline fun Element.value() = this.textContent///???

inline fun Element.text() = this.textContent ?: ""

inline fun Element.addChildren(block: Document.() -> Iterable<Element>): Element = apply {
    val children = ownerDocument.block()
    for (child in children) {
        appendChild(child)
    }
}

inline fun Element.addChild(block: Document.() -> Element) = apply {
    appendChild(ownerDocument.block())
}

inline fun Element.removeChilds(filter: (Node) -> Boolean) = apply {
    childNodes.run {
        var cur = 0
        while (cur < length){
            val item = item(cur)
            if(filter(item)){
                removeChild(item)
            } else {
                cur++
            }
        }
    }
}

inline fun Document.element(name: String) = createElement(name)

inline fun <T: Node> Document.copy(e: T, deep: Boolean = true) = importNode(e, deep) as T

inline fun <T> Element.attr(name: String, value: T, converter: (T) -> String? = { it?.toString() }): Element = apply {
    converter(value)?.also {
        this.setAttribute(name, it)
    }
}

inline fun <T> Element.attrR(name: String, value: T?, converter: (T?) -> String? = { it?.toString() }): Element = apply {
    converter(value)?.also {
        this.setAttribute(name, it)
    } ?: this.removeAttribute(name)
}

inline fun <T> Element.value(value: T, converter: (T) -> String? = { it?.toString() }): Element = apply {
    this.textContent = converter(value)
}

inline fun <T: Node> T.appendTo(root: Element): T = apply {
    root.appendChild(this)
}

fun Node.write(os: OutputStream) {
    try {
        val tr = TransformerFactory.newInstance().newTransformer()
        val source = DOMSource(this)
        val result = StreamResult(os)
        tr.transform(source, result)
    } catch (e: TransformerException) {
        e.printStackTrace(System.out)
    } catch (e: IOException) {
        e.printStackTrace(System.out)
    }
}

fun Node.write2(charset: Charset = Charsets.UTF_8): String{
    return try {
        val reg = DOMImplementationRegistry.newInstance()
        val impl = reg.getDOMImplementation("LS") as DOMImplementationLS
        val serializer = impl.createLSSerializer()
        val domConfig = serializer.domConfig
        domConfig.setParameter("format-pretty-print", true)
        domConfig.setParameter("element-content-whitespace", true)
        domConfig.setParameter("cdata-sections", true)
        serializer.newLine = "\r\n"
        //serializer.writeToString(this)
        DomOutput(charset).also {
            serializer.write(this, it)
        }.writer.use { it.toString() }
    } catch (e: Exception){
        e.printStackTrace()
        ""
    }
}

private class DomOutput(var charset: Charset?, var writer: Writer = StringWriter()): LSOutput{

    override fun getEncoding(): String? = charset?.name()

    override fun setCharacterStream(characterStream: Writer?) {
        this.writer = characterStream!!
    }

    override fun getCharacterStream(): Writer = writer

    override fun setEncoding(encoding: String?) {
        this.charset = encoding?.let { Charset.forName(it) }
    }

    override fun getSystemId(): String? = null

    override fun getByteStream(): OutputStream? = null

    override fun setSystemId(systemId: String?) {
        throw java.lang.Exception("systemId is not supported by this class")
    }

    override fun setByteStream(byteStream: OutputStream?) {
        throw java.lang.Exception("byteStream is not supported by this class")
    }
}