package helpers

import java.io.*
import java.nio.charset.Charset
import java.text.ParseException
import java.util.function.Consumer
import java.util.function.Predicate
import kotlin.math.min

interface ITableReader: Iterable<ITableReader.Table>, Closeable{
    operator fun get(idx: Int): Table
    override fun close()

    interface Table: Iterable<Map<String, Any?>>{
        fun name(): String?
        fun getHeader(): Array<String>
        fun setHeader(arr: Array<String>)
    }
}

interface ITableWriter: Closeable{
    fun startTable(idx: Int, name: String?, header: Array<String>)
    fun putRow(idx: Int, map: Map<String, Any?>)

    fun flush()
    override fun close()
}

interface ITableReaderTarget{
    fun addFrom(input: ITableReader)
}

interface ITableWriterSource{
    fun addTo(output: ITableWriter)
}

inline fun <T: ITableReaderTarget> T.from(input: ITableReader): T {
    this.addFrom(input)
    return this
}

inline fun <T: ITableWriterSource> T.to(output: ITableWriter): T {
    this.addTo(output)
    return this
}

class TableConverter: ITableReaderTarget, ITableWriterSource{
    private var input = ArrayList<ITableReader>()
    private var output = ArrayList<Pair<ITableWriter, Predicate<Map<String, Any?>>?>>()
    private var rowPredicate: Predicate<Map<String, Any?>>? = null
    private var headerUpdate: Consumer<Array<String>>? = null
    private val processors: ArrayList<Consumer<MutableMap<String, Any?>>> = ArrayList()
    var joinStr = ";"
    private var logger: PrintStream? = null
    private var listener: IListener? = null

    fun process(idx: Int = -1){
        if(input.isEmpty() || output.isEmpty()){
            throw IllegalStateException("input / output not defined!")
        }
        if(idx >= 0){
            processTable(idx, input[0].get(idx), 0)
        } else {
            var grow = 0
            for(inp in input) {
                for ((i, table) in inp.withIndex()) {
                    grow = processTable(i, table, grow)
                }
            }
        }
    }

    private fun processTable(idx: Int, t: ITableReader.Table, startRowNum: Int): Int{
        val header = t.getHeader()
        if(headerUpdate != null){
            headerUpdate!!.accept(header)
            t.setHeader(header)
        }
        listener?.setTable(idx, t.name(), header)
        output.forEach {
            it.first.startTable(idx, t.name(), header)
        }
        var rowNum = startRowNum
        var logTimestamp = 0L
        var lastRow: MutableMap<String, Any?>? = null
        try{
            for ((i, map) in t.withIndex()) {
                listener?.row(i, map)
                val mutableMap = map.toMutableMap()
                for (adapter in processors) {
                    adapter.accept(mutableMap)
                }
                if(rowPredicate?.test(mutableMap) == false && lastRow != null){
                    for ((k, v) in mutableMap.entries) {
                        if(!v?.toString().isNullOrBlank()){
                            var oldValue = (lastRow[k] ?: "").toString()
                            if(oldValue.isNotBlank()){
                                oldValue += joinStr
                            }
                            lastRow[k] = oldValue + v.toString()
                        }
                    }
                } else {
                    if(lastRow != null){
                        val out = lastRow
                        output.forEach {
                            if(it.second?.test(out) != false){
                                it.first.putRow(rowNum, out)
                            }
                        }
                    }
                    rowNum++
                    lastRow = mutableMap
                    if(logger != null){
                        if(logTimestamp < System.currentTimeMillis() - 2000) {
                            logger!!.println("processed $rowNum rows")
                            logTimestamp = System.currentTimeMillis()
                        }
                    }
                }
            }
        } finally {
            if(lastRow != null){
                val out = lastRow
                output.forEach {
                    if(it.second?.test(out) != false){
                        it.first.putRow(rowNum, out)
                    }
                }
            }
            output.forEach { it.first.flush() }
        }
        return rowNum
    }

    fun close(){
        input.forEach { it.close() }
        output.forEach { it.first.close() }
    }

    fun log(log: PrintStream?): TableConverter{
        logger = log
        return this
    }

    override fun addFrom(input: ITableReader){
        this.input.add(input)
    }

    override fun addTo(output: ITableWriter){
        this.output.add(output to null)
    }

    fun setOutPredicate(predicate: Predicate<Map<String, Any?>>?, idx: Int = output.size - 1){
        output[idx] = output[idx].first to predicate
    }

    fun setPredicate(predicate: ((Map<String, Any?>) -> Boolean)?): TableConverter {
        rowPredicate = if(predicate != null){
            Predicate { predicate(it) }
        } else {
            null
        }
        return this
    }

    fun addProcessor(adapter: Consumer<MutableMap<String, Any?>>): TableConverter {
        processors.add(adapter)
        return this
    }

    fun setHeaderUpdater(headerUpdater: Consumer<Array<String>>?): TableConverter {
        headerUpdate = headerUpdater
        return this
    }

    fun setListener(listener: IListener?): TableConverter {
        this.listener = listener
        return this
    }

    interface IListener{
        fun setTable(idx: Int, name: String?, header: Array<String>)
        fun row(idx: Int, row: Map<String, Any?>)
    }
}

inline fun TableConverter.addProcessor(noinline adapter: (MutableMap<String, Any?>) -> Unit): TableConverter {
    return addProcessor(Consumer { adapter(it) })
}

inline fun TableConverter.setHeaderUpdater(noinline headerUpdater: ((Array<String>) -> Unit)?): TableConverter {
    return if(headerUpdater != null){
        setHeaderUpdater(Consumer { headerUpdater(it) })
    } else {
        setHeaderUpdater(null as Consumer<Array<String>>?)
    }
}

inline fun TableConverter.whenRow(predicate: Predicate<Map<String, Any?>>?): TableConverter{
    setOutPredicate(predicate)
    return this
}

class TableHeaderGetter: ITableReaderTarget{
    private lateinit var input: ITableReader

    override fun addFrom(input: ITableReader){
        this.input = input
    }

    fun get(): Array<Array<String>>{
        if(!this::input.isInitialized) {
            throw IllegalStateException("input not defined!")
        }
        val headers = ArrayList<Array<String>>()
        for (table in input) {
            headers.add(table.getHeader())
        }
        return Array(headers.size) { headers[it] }
    }

    fun get(index: Int): Array<String>{
        if(!this::input.isInitialized) {
            throw IllegalStateException("input not defined!")
        }
        return input.get(index).getHeader()
    }

    fun close(){
        input.close()
    }
}

class TableSortHelper: ITableReaderTarget, ITableWriterSource{
    private val inputs = ArrayList<ITableReader>()
    private val outputs = ArrayList<ITableWriter>()
    private var log: PrintStream? = null

    override fun addFrom(input: ITableReader) {
        inputs.add(input)
    }

    override fun addTo(output: ITableWriter) {
        outputs.add(output)
    }

    fun log(logger: PrintStream?): TableSortHelper {
        log = logger
        return this
    }

    fun sort(comparator: Comparator<Map<String, Any?>>){
        var step = 1
        val header = inputs[0].get(0).getHeader()
        log?.println("split $step items")
        var f1 = File.createTempFile("zxk_sort1", null)
        var f2 = File.createTempFile("zxk_sort2", null)
        val totalCount = split(inputs, f1, f2, step, header)
        while (step * 2 <= totalCount){
            log?.println("merge $step items")
            val fmerge = File.createTempFile("zxk_sortmerge", null)
            val outputs = arrayListOf(CSVWriter(fmerge, header, ',') as ITableWriter)
            outputs[0].startTable(0, null, header)
            join(f1, f2, comparator, outputs, step)
            outputs[0].flush()
            outputs[0].close()
            f1.delete()
            f2.delete()
            step *= 2
            log?.println("split $step items")
            f1 = File.createTempFile("zxk_sort1", null)
            f2 = File.createTempFile("zxk_sort2", null)
            val sourcesList = arrayListOf(CSVReader(fmerge, ',', Charsets.UTF_8) as ITableReader)
            split(sourcesList, f1, f2, step, header)
            sourcesList[0].close()
            fmerge.delete()
        }
        log?.println("merge $step items")
        for (writer in outputs) {
            writer.startTable(0, null, header)
        }
        join(f1, f2, comparator, outputs, step)
        log?.println("save last files...")
        for (writer in outputs) {
            writer.flush()
        }
        f1.delete()
        f2.delete()
    }

    fun close(){
        for (reader in inputs) {
            reader.close()
        }
        for (writer in outputs) {
            writer.close()
        }
    }

    private fun split(sourcesList: ArrayList<ITableReader>, f1: File, f2: File, step: Int, header: Array<String>): Int{
        val f1Writer = CSVWriter(f1, header, ',')
        val f2Writer = CSVWriter(f2, header, ',')
        var second = true
        var count = 0
        var tablesCount = 0
        //val debugList = if(step >= 512) null else ArrayList<Map<String, Any?>>()
        for(src in sourcesList){
            for (table in src) {
                f1Writer.startTable(tablesCount, table.name(), header)
                f2Writer.startTable(tablesCount, table.name(), header)
                for (row in table) {
                    if(count % step == 0){
                        second = second.not()
                        //debugList?.clear()
                    }
                    //debugList?.add(row)
                    if(second){
                        f2Writer.putRow(count, row)
                    } else {
                        f1Writer.putRow(count, row)
                    }
                    count++
                }
                tablesCount++
            }
        }
        f1Writer.flush()
        f1Writer.close()
        f2Writer.flush()
        f2Writer.close()
        return count
    }

    private fun join(f1: File, f2: File, comparator: Comparator<Map<String, Any?>>, outputs: ArrayList<ITableWriter>, step: Int){
        val f1Reader = CSVReader(f1, ',', Charsets.UTF_8)
        val f2Reader = CSVReader(f2, ',', Charsets.UTF_8)
        val f1Iterator =  f1Reader.get(0).iterator()
        val f2Iterator =  f2Reader.get(0).iterator()
        var count = 0
        var f1count = 0
        var f2count = 0
        var row1: Map<String, Any?>? = null
        var row2: Map<String, Any?>? = null
        while (f1Iterator.hasNext() || f2Iterator.hasNext()){
            if(row1 == null && f1Iterator.hasNext() && f1count < step){
                row1 = f1Iterator.next()
                f1count++
            }
            if(row2 == null && f2Iterator.hasNext() && f2count < step){
                row2 = f2Iterator.next()
                f2count++
            }
            if(row1 != null && row2 != null){
                val i = comparator.compare(row1, row2)
                if(i <= 0){
                    putRow(outputs, count, row1)
                    row1 = null
                } else {
                    putRow(outputs, count, row2)
                    row2 = null
                }
            } else if(row1 != null){
                putRow(outputs, count, row1)
                row1 = null
            } else if(row2 != null){
                putRow(outputs, count, row2)
                row2 = null
            } else {
                f1count = 0
                f2count = 0
                count--
            }
            count++
        }
        if(row1 != null){
            putRow(outputs, count, row1)
        } else if(row2 != null){
            putRow(outputs, count, row2)
        }
        f1Reader.close()
        f2Reader.close()
    }

    private fun putRow(outputs: ArrayList<ITableWriter>, rowNum: Int, row: Map<String, Any?>){
        for (writer in outputs) {
            writer.putRow(rowNum, row)
        }
    }
}

class TableClearHelper: ITableReaderTarget, ITableWriterSource{
    private val inputs = ArrayList<ITableReader>()
    private val outputs = ArrayList<ITableWriter>()
    private val cleaners = ArrayList<ICleaner>()
    private var log: PrintStream? = null

    override fun addFrom(input: ITableReader) {
        inputs.add(input)
    }

    override fun addTo(output: ITableWriter) {
        outputs.add(output)
    }

    fun addCleaner(cleaner: ICleaner): TableClearHelper{
        cleaners.add(cleaner)
        return this
    }

    fun addCleaner(cleaner: (Map<String, Any?>, Map<String, Any?>?) -> Boolean): TableClearHelper {
        return addCleaner(object : ICleaner{
            override fun needRemove(it: Map<String, Any?>, previous: Map<String, Any?>?): Boolean = cleaner(it, previous)
        })
    }

    fun log(logger: PrintStream?): TableClearHelper {
        log = logger
        return this
    }

    fun run(){
        var previous: Map<String, Any?>? = null
        var tableIdx = 0
        for (reader in inputs) {
            for (table in reader) {
                val header = table.getHeader()
                val name = table.name()
                for (writer in outputs) {
                    writer.startTable(tableIdx, name, header)
                }
                var rowNum = 0
                for (row in table) {
                    var remove = false
                    for (cleaner in cleaners) {
                        if(cleaner.needRemove(row, previous)){
                            remove = true
                            break
                        }
                    }
                    if(!remove){
                        for (writer in outputs) {
                            writer.putRow(rowNum, row)
                        }
                        rowNum++
                    }
                    previous = row
                }
                tableIdx++
            }
        }
        for (writer in outputs) {
            writer.flush()
        }
    }

    fun close(){
        for (reader in inputs) {
            reader.close()
        }
        for (writer in outputs) {
            writer.close()
        }
    }

    interface ICleaner{
        fun needRemove(it: Map<String, Any?>, previous: Map<String, Any?>?): Boolean
    }
}

class ClearCollsProcessor(private val rules: Map<String, ArrayList<ARule>>): Consumer<MutableMap<String, Any?>>, TableConverter.IListener{
    private val lRules = mutableMapOf(*rules.entries.map { it.key to it.value }.toTypedArray())
    private val lRulesListeners = ArrayList<TableConverter.IListener>()
    override fun setTable(idx: Int, name: String?, header: Array<String>) {
        lRules.clear()
        lRulesListeners.clear()
        for ((k, rules) in rules) {
            if(header.contains(k)){
                lRules[k] = rules
                for (rule in rules) {
                    if(rule is TableConverter.IListener){
                        if(!lRulesListeners.contains(rule)) {
                            lRulesListeners.add(rule)
                            rule.setTable(idx, name, header)
                        }
                    }
                }
            }
        }
    }

    override fun row(idx: Int, row: Map<String, Any?>) {
        for (rule in lRulesListeners) {
            rule.row(idx, row)
        }
    }

    override fun accept(t: MutableMap<String, Any?>) {
        for ((k, rules) in lRules) {
            for (rule in rules) {
                rule.accept(t to k)
            }
        }
    }

    override fun toString(): String {
        val builder = StringBuilder("ClearCollsProcessor {\r\n")
        for ((col, r) in rules) {
            builder.append(col).append("{\r\n")
            for (rule in r) {
                builder.append(rule.toString()).append("\r\n")
            }
            builder.append("}\r\n")
        }
        return builder.append('}').toString()
    }

    abstract class ARule(protected val predicate: ARulePredicate): Consumer<Pair<MutableMap<String, Any?>, String>>{
        abstract fun execute(row: MutableMap<String, Any?>, colName: String)

        override fun accept(t: Pair<MutableMap<String, Any?>, String>) {
            val row = t.first
            val colName = t.second
            if(!row.containsKey(colName)){//?
                return
            }
            if(predicate.test(row[colName])){
                execute(row, colName)
            }
        }

        override fun toString(): String {
            return predicate.toString() + "->"
        }

        abstract class ARulePredicate: Predicate<Any?>{
            open fun getTemplateMembers(t: Any?): List<String> = listOf((t ?: "").toString())
        }
    }

    class ReplaceRule(predicate: ARulePredicate, private val replacement: Any?): ARule(predicate) {
        override fun execute(row: MutableMap<String, Any?>, colName: String) {
            row[colName] = replacement
        }

        override fun toString(): String = super.toString() + when (replacement) {
            null -> ""
            is String -> "\"${replacement.escapeQuotes()}\""
            else -> replacement.toString()
        }
    }

    class ReplaceTemplateRule(predicate: ARulePredicate, private val template: Template): ARule(predicate) {
        override fun execute(row: MutableMap<String, Any?>, colName: String) {
            val list = predicate.getTemplateMembers(row[colName])
            row[colName] = template.precess(list)
        }

        override fun toString(): String = super.toString() + template.toString()

        class Template(private val replacement: Array<Any>){

            fun precess(targets: List<String>): String{
                return replacement.joinToString("") { if(it is Number) targets[it.toInt()] else it.toString() }
            }

            override fun toString(): String = "/${replacement.joinToString("") { if(it is Number) "[" + it.toString() + "]" else it.toString().escapeQuotes().replace("[", "\\[").replace("/", "\\/") }}/"
        }
    }

    class PrintRule(predicate: ARulePredicate, private val log: PrintStream?, private val flagContainNowCol: String? = null): ARule(predicate), TableConverter.IListener {
        private var tableName: String? = null
        private var rowIdx: Int = -1
        override fun setTable(idx: Int, name: String?, header: Array<String>) {
            tableName = name
            rowIdx = 0
        }

        override fun row(idx: Int, row: Map<String, Any?>) {
            rowIdx = idx
        }

        override fun execute(row: MutableMap<String, Any?>, colName: String) {
            if(log == null){
                return
            }
            if(flagContainNowCol == null || row[flagContainNowCol] != colName) {
                if(flagContainNowCol == null || row[flagContainNowCol] == null) {
                    log.println("in row $rowIdx:")
                    for ((k, v) in row) {
                        log.println("$k -> ${prepareToPrint(v)}")
                    }
                }
                log.println("found replacement cell $colName (${prepareToPrint(row[colName])})")
            }
            if(flagContainNowCol != null){
                row[flagContainNowCol] = colName
            }
        }

        override fun toString(): String = super.toString() + "_"

        private fun prepareToPrint(v: Any?): String{
            return when(v){
                null -> ""
                is String -> "\"${v.escapeQuotes()}\""
                else -> v.toString()
            }
        }
    }

    class NeedActiveRule(predicate: ARulePredicate, private val input: BufferedReader, private val output: PrintStream): ARule(predicate), TableConverter.IListener {
        private var tableName: String? = null
        private var rowIdx: Int = -1
        override fun setTable(idx: Int, name: String?, header: Array<String>) {
            tableName = name
            rowIdx = 0
        }

        override fun row(idx: Int, row: Map<String, Any?>) {
            rowIdx = idx
        }

        override fun execute(row: MutableMap<String, Any?>, colName: String) {
            output.println("in row: ($rowIdx)")
            for ((k, v) in row) {
                output.println("$k -> ${prepareToPrint(v)}")
            }
            output.println("found replacement cell $colName (${prepareToPrint(row[colName])})")
            output.println("please write how to need change this row. Format is colname:new value")
            output.println("where new value empty -> null, quoted -> string, number -> number, any -> string")
            while (true){
                val line = input.readLine()
                if(line.isBlank()){
                    return
                } else {
                    val split = line.split(":", limit = 2)
                    if(split.size <= 1){
                        output.println("incorrect input")
                        continue
                    }
                    val name = split[0]
                    val value = prepareToReplace(split[1])
                    row[name] = value
                }
            }
        }

        override fun toString(): String = super.toString() + '?'

        private fun prepareToPrint(v: Any?): String{
            return when(v){
                null -> ""
                is String -> "\"${v.escapeQuotes()}\""
                else -> v.toString()
            }
        }

        private fun prepareToReplace(v: String): Any?{
            return when{
                v.isEmpty() -> null
                v.startsWith('"') && v.endsWith('"') && v.length > 1 -> v.substring(1, v.lastIndex).recoveryQuotes()
                v.contains(NUM_REG) -> {
                    if(v.contains('.')){
                        v.toDoubleOrNull() ?: v
                    } else {
                        v.toLongOrNull() ?: v
                    }
                }
                else -> v
            }
        }
    }

    companion object {

        fun fromFile(f: File, input: BufferedReader, output: PrintStream, log: PrintStream?): ClearCollsProcessor?{
            val reader = FileInputStream(f).bufferedReader(Charset.defaultCharset())
            reader.use {
                return fromLines(it.lineSequence(), input, output, log)
            }
        }

        fun fromLines(lines: Sequence<String>, input: BufferedReader, output: PrintStream, log: PrintStream?): ClearCollsProcessor?{
            val ret = HashMap<String, ArrayList<ARule>>()
            val cols = ArrayList<String>()
            val sb = StringBuilder()
            val out = Array(1) {0}
            for (line in lines) {
                if(line.startsWith('\'')){
                    continue//comment
                } else if(cols.isEmpty()){
                    if(line.endsWith('{')){
                        //cols.addAll(line.substring(0, line.length - 1).split(','))
                        val colsStr = line.substring(0, line.length - 1)
                        var offset = 0
                        while (offset < colsStr.length){
                            val add = parseAny(colsStr.substring(offset), {it}, out, ",", sb)
                            cols.add(add.toString())
                            offset += out[0] + 1
                        }
                    }
                } else if(line == "}"){
                    cols.clear()
                } else {
                    val rez = parseAny(line, {Regex(it)}, out, "->", sb)
                    val predicate = if (rez is Regex) {
                        RegexPredicate(rez)
                    } else {
                        EqPredicate(rez)
                    }
                    if(predicate is RegexPredicate && line[min(out[0], line.length - 1)] == 'i'){
                        predicate.ignoreCase = true
                    }
                    val toStart = line.indexOf("->", out[0])
                    if(toStart >= 0){
                        val add2 = parseAny(line.substring(toStart + 2), {parseTemplateForTemplRule(it, sb)}, out, "'", sb)
                        val rule = if(add2 is ReplaceTemplateRule.Template){
                            ReplaceTemplateRule(predicate, add2)
                        } else if (add2 == "?" && out[0] == 1) {
                            NeedActiveRule(predicate, input, output)
                        } else if (add2 == "_" && out[0] == 1) {
                            PrintRule(predicate, log, "FLAG_CONTAIN_VALUE_IN_LOG")
                        } else {
                            ReplaceRule(predicate, add2)
                        }
                        for (col in cols) {
                            if(!ret.containsKey(col)){
                                ret[col] = ArrayList()
                            }
                            ret[col]!!.add(rule)
                        }
                    } else {
                        output.println("error in parse str \"$line\"")
                    }
                }
            }
            return if(ret.isEmpty()){
                null
            } else {
                ClearCollsProcessor(ret)
            }
        }

        private fun parseTemplateForTemplRule(str: String, sb: StringBuilder): ReplaceTemplateRule.Template{
            sb.setLength(0)
            val ret = ArrayList<Any>()
            var escaped = false
            var indexed = false
            for (c in str) {
                if(indexed){
                    if(c == ']'){
                        ret.add(sb.toString().toInt())
                        sb.setLength(0)
                        indexed = false
                    } else {
                        sb.append(c)
                    }
                } else if(escaped){
                    sb.append(c)
                    escaped = false
                } else if(c == '\\'){
                    escaped = true
                } else if(c == '['){
                    ret.add(sb.toString())
                    sb.setLength(0)
                    indexed = true
                } else {
                    sb.append(c)
                }
            }
            if(indexed){
                throw ParseException("not end index in /${str.escapeQuotes()}/", str.length)
            }
            ret.add(sb.toString())
            return ReplaceTemplateRule.Template(ret.toTypedArray())
        }

        private inline fun parseAny(line: String, noinline templateFactory: (String) -> Any, out: Array<Int>, ends: String, sb: StringBuilder = StringBuilder()): Any?{
            return parseAny(line, mapOf('"' to ('"' to {it -> it}), '/' to ('/' to templateFactory)), out, listOf(ends), sb)
        }

        class RegexPredicate(private var regex: Regex): ARule.ARulePredicate() {
            var ignoreCase = false
            set(value) {
                field = value
                regex = Regex(regex.pattern, if(value) setOf(RegexOption.IGNORE_CASE) else emptySet())
            }
            override fun test(t: Any?): Boolean {
                return (t ?: "").toString().contains(regex)
            }

            override fun getTemplateMembers(t: Any?): List<String> {
                val find = regex.find((t ?: "").toString()) ?: return emptyList()
                return find.groupValues
            }

            override fun toString(): String {
                return "/${regex.pattern.escapeQuotes().replace("/", "\\/")}/"
            }
        }

        class EqPredicate(private val o: Any?): ARule.ARulePredicate() {
            override fun test(t: Any?): Boolean {
                return t == o
            }

            override fun toString(): String {
                return when(o){
                    null -> ""
                    is String -> "\"${o.escapeQuotes()}\""
                    else -> o.toString()
                }
            }
        }
    }
}

class RenameCollsProcessor(val map: Map<String, String>): Consumer<MutableMap<String, Any?>> {
    override fun accept(t: MutableMap<String, Any?>) {
        for (key in map.keys) {
            if(t.containsKey(key)){
                t[map[key] ?: ""] = t.remove(key)
            }
        }
    }
}


