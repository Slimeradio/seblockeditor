package helpers

import java.lang.reflect.Field

val NUM_REG = Regex("^[+\\-]?(0|[1-9]\\d*)(\\.(0|\\d*[1-9]))?$")

inline fun String.escapeQuotes(): String = replace("\\", "\\\\").replace("\"", "\\\"").replace("\'", "\\\'")

inline fun String.recoveryQuotes(): String = replace("\\\"", "\"").replace("\\\'", "\'").replace("\\\\", "\\")

inline fun <R, T: R> Iterable<T>.mergeToOne(transform: T.(R) -> R): R? = iterator().mergeToOne(transform)

inline fun <R, T: R> Sequence<T>.mergeToOne(transform: T.(R) -> R): R? = iterator().mergeToOne(transform)

inline fun <R, T: R> Iterator<T>.mergeToOne(transform: T.(R) -> R): R?{
    if(!hasNext()){//is empty
        return null
    }
    var ret: R = next()
    while (hasNext()){
        ret = next().transform(ret)
    }
    return ret
}

inline fun Int.hasFlags(mask: Int): Boolean = getFlags(mask) == mask

inline fun Long.hasFlags(mask: Long): Boolean = getFlags(mask) == mask

inline fun Int.hasOneOfFlags(mask: Int): Boolean = getFlags(mask) != 0

inline fun Long.hasOneOfFlags(mask: Long): Boolean = getFlags(mask) != 0L

inline fun Int.setFlags(flags: Int, contain: Boolean): Int = if(contain) addFlags(flags) else removeFlags(flags)

inline fun Long.setFlags(flags: Long, contain: Boolean): Long = if(contain) addFlags(flags) else removeFlags(flags)

inline fun Int.addFlags(flags: Int): Int = this or flags

inline fun Long.addFlags(flags: Long): Long = this or flags

inline fun Int.removeFlags(flags: Int): Int = this and flags.inv()

inline fun Long.removeFlags(flags: Long): Long = this and flags.inv()

inline fun Int.getFlags(mask: Int): Int = this and mask

inline fun Long.getFlags(mask: Long): Long = this and mask

//TODO ("...", "...", ")...")
fun parseAny(line: String, templateFactories: Map<Char, Pair<Char, (String) -> Any>>, out: Array<Int>, ends: Collection<String>, sb: StringBuilder = StringBuilder()): Any?{
    val target =  templateFactories[line[0]]
    if(target != null){
        sb.setLength(0)
        val end = findEndChar(line.substring(1), sb, target.first)
        if(end > 0){
            out[0] = end + 2
            return target.second(sb.toString())
        }
    }
    val i = line.indexOfAny(ends)
    val simple = if(i >= 0) line.substring(0, i) else line
    out[0] = if(i >= 0) i else line.length
    return when {
        simple.isBlank() -> null
        simple.contains(NUM_REG) -> {
            if(simple.contains('.')){
                simple.toDoubleOrNull() ?: simple
            } else {
                simple.toLongOrNull() ?: simple
            }
        }
        else -> simple
    }
}

private fun findEndChar(line: String, out: StringBuilder, terminate: Char): Int{
    var escaped = false
    for ((i, c) in line.withIndex()) {
        when {
            escaped -> {
                out.append(c)
                escaped = false
            }
            c == '\\' -> escaped = true
            c != terminate -> out.append(c)
            else -> return i
        }
    }
    return -1
}

inline fun Any?.toLongOrString() = when(this){
    null -> null
    is Number -> this.toLong()
    is String -> this.toLongOrNull() ?: this
    else -> this.toString()
}

inline fun <T> MutableCollection<T>.addAll(arg: Iterator<T>): Boolean{
    var ret = false
    for (t in arg) {
        ret = this.add(t) or ret
    }
    return ret
}

inline fun <T> Iterator<T>.firstOrNull(): T? = if(hasNext()) next() else null

inline fun <T> Iterator<T>.limited(limit: Int) = LimitIterator(limit, this)

class LimitIterator<T>(val count: Int, val base: Iterator<T>): Iterator<T> {
    var i = 0
    override fun hasNext(): Boolean = i < count || base.hasNext()

    override fun next(): T {
        i++
        return base.next()
    }

    inline fun toList(): List<T>{
        val ret = ArrayList<T>(this.count)
        for (t in this) {
            ret.add(t)
        }
        return ret
    }
}

inline fun <T> Iterator<T>.toList(): List<T>{
    val ret = ArrayList<T>((this as? LimitIterator)?.count ?: 0)
    for (t in this) {
        ret.add(t)
    }
    return ret
}

inline fun <reified T> T._debugGetProp(prop: String): Any?{
    try {
        val clazz = T::class.java
        val field = try{
            clazz.getDeclaredField(prop)
        } catch (e: NoSuchFieldException){
            var ret: Field? = null
            var sclazz = clazz.superclass
            while (sclazz != null){
                try{
                    ret = sclazz.getDeclaredField(prop)
                    break
                } catch (ignore: NoSuchFieldException){
                    //DO NOTHING
                }
                sclazz = sclazz.superclass
            }
            ret ?: throw e
        }
        val accessible = field.isAccessible
        try {
            field.isAccessible = true
            return field.get(this)
        } finally {
            field.isAccessible = accessible
        }
    } catch (e: SecurityException){
        e.printStackTrace()
        return null
    } catch (e: IllegalAccessException){
        e.printStackTrace()
        return null
    } catch (e: NoSuchFieldException){
        e.printStackTrace()
        return null
    }
}