package sample.data

import helpers.MPair

class BlockId (var blockId: String?, var blockSubtypeId: String?) {
    override fun toString(): String {
        return blockSubtypeId.takeUnless { it.isNullOrBlank() } ?: blockId ?: ""
    }
}

class ComponentItem( var amount: Int = 0, var component: String?, var deconstructId: BlockId? = null)

class BuildProgressModellItem(var progress: Float = 0.0f, var file: String?)

class XYZInt(var x: Int = 0, var y: Int = 0, var z: Int = 0) {
    companion object { }
}

class XYZFloat(var x: Float = 0.0f, var y: Float = 0.0f, var z: Float = 0.0f) {
    companion object { }
}

class XYZAxis(var x: String = "", var y: String = "", var z: String = "") {
    companion object { }
}

data class EditorControllerBlockList(val components: MutableList<ComponentItem>, val critical: MPair<String, Int>? = null)

class MountPointItem(val side: String, val startX: Float, val startY: Float, val endX: Float, val endY: Float, val default: Boolean = false)

class Sound{
    lateinit var id: String
    lateinit var name: String
}