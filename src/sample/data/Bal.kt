package sample.data


import javafx.beans.property.SimpleIntegerProperty
import javafx.beans.property.SimpleStringProperty
import javafx.collections.FXCollections


class Bal{
    val blockId = SimpleStringProperty(this, "BLOCK")
    val companents = FXCollections.observableHashMap<String, BalItem>()

    companion object {
        const val STR_FULL_COUNT = "Total"
        const val STR_PRIORITY = "Priority"
        const val STR_ADDITIONAL = "Extra"
        const val STR_ADDITIONAL_PRIORITY = "Extra Priority"
    }
}

class BalItem{
    val fullCount = SimpleIntegerProperty(this, Bal.STR_FULL_COUNT)
    val priority = SimpleIntegerProperty(this, Bal.STR_PRIORITY)
    val additional = SimpleIntegerProperty(this, Bal.STR_ADDITIONAL)
    val additionalPriority = SimpleIntegerProperty(this, Bal.STR_ADDITIONAL_PRIORITY)
}