package sample.data

import helpers.*
import javafx.scene.paint.Color
import org.w3c.dom.Document
import org.w3c.dom.Element
import java.lang.NumberFormatException
import kotlin.math.min
import kotlin.reflect.KMutableProperty

fun colorFromXML(e : Element) : Color {
    return Color(
        e.child("X")?.value { it.toDoubleOrNull() } ?: 0.0,
        e.child("Y")?.value { it.toDoubleOrNull() } ?: 0.0,
        e.child("Z")?.value { it.toDoubleOrNull() } ?: 0.0,
        e.child("W")?.value { it.toDoubleOrNull() } ?: 0.0
    )
}

fun XYZFloat.Companion.fromXML(e : Element, def : Float = 1f) : XYZFloat {
    return XYZFloat(
        x = e.attr("x") { it.toFloat() } ?: e.child("X")?.value { it.toFloat() } ?: def,
        y = e.attr("y") { it.toFloat() } ?: e.child("Y")?.value { it.toFloat() } ?: def,
        z = e.attr("z") { it.toFloat() } ?: e.child("Z")?.value { it.toFloat() } ?: def
    )
}

fun XYZInt.Companion.fromXML(e : Element, def : Int = 1) : XYZInt {
    return XYZInt(
        x = e.attr("x") { it.toInt() } ?: e.child("X")?.value { it.toInt() } ?: def,
        y = e.attr("y") { it.toInt() } ?: e.child("Y")?.value { it.toInt() } ?: def,
        z = e.attr("z") { it.toInt() } ?: e.child("Z")?.value { it.toInt() } ?: def
    )
}



// block.MinHeight?.also { doc.element(TAG_MIN_HEIGHT).value(it).appendTo(root) }







fun Block.readStrings (element : Element, vararg params : KMutableProperty<String?>) {
    for (x in params) {
        val v = element.child(x.name)?.value()
        x.setter.call(this, v)
    }
}

fun Block.readFloats (element : Element, vararg params : KMutableProperty<Float?>) {
    for (x in params) {
        val v = element.child(x.name)
        if (v != null && v.value() != null) {
            x.setter.call(this, v.value().toFloat())
        }
    }
}


fun Block.readInts (element : Element, vararg params : KMutableProperty<Int?>) {
    for (x in params) {
        val v = element.child(x.name)
        if (v != null && v.value() != null) {
            x.setter.call(this, v.value().toInt())
        }
    }
}


fun Block.readDoubles (element : Element, vararg params : KMutableProperty<Int?>) {
    for (x in params) {
        val v = element.child(x.name)
        if (v != null && v.value() != null) {
            x.setter.call(this, v.value().toInt())
        }
    }
}


fun Document.appendInt (root: Element, block: Block,p : KMutableProperty<Int?>) {
    val x = p.getter.call(block) ?: return
    this.element(p.name).value(x).appendTo(root)
}

fun Document.appendFloat (root: Element, block: Block, p : KMutableProperty<Float?>) {
    val x = p.getter.call(block) ?: return
    this.element(p.name).value(x).appendTo(root)
}

fun Document.appendDouble (root: Element, block: Block,p : KMutableProperty<Double?>) {
    val x = p.getter.call(block) ?: return
    this.element(p.name).value(x).appendTo(root)
}

fun Document.appendString (root: Element, block: Block,p : KMutableProperty<String?>) {
    val x = p.getter.call(block) ?: return
    this.element(p.name).value(x).appendTo(root)
}

inline fun <T> Document.addChangeOrDelExt(root: Element, tagName: String, value: T?, olds: MutableMap<String, Element>, change: Element.(value: T) -> Unit){
    addChangeOrDel(root, tagName, value, olds[tagName], change)
    olds.remove(tagName)
}

inline fun <T> Document.addChangeOrDel(root: Element, tagName: String, value: T?, old: Element? = root.child(tagName), change: Element.(value: T) -> Unit){
    if(value != null){
        (old ?: element(tagName).appendTo(root)).change(value)
    } else if(old != null){
        root.removeChild(old)
    }
}

inline fun <T> Document.addOrChangeExt(root: Element, tagName: String, value: T?, olds: MutableMap<String, Element>, change: Element.(value: T?) -> Unit){
    addOrChange(root, tagName, value, olds[tagName], change)
    olds.remove(tagName)
}

inline fun <T> Document.addOrChange(root: Element, tagName: String, value: T?, old: Element? = root.child(tagName), change: Element.(value: T?) -> Unit){
    (old ?: element(tagName).appendTo(root)).change(value)
}




class Block() {
    var XsiType: String? = null
    var BlockId: BlockId? = null
    var DisplayName: String? = null
    var Description: String? = null
    var Raw: String? = null
    var BlockPairName: String? = null
    var EdgeType: String? = null
    var ResourceSourceGroup: String? = null
    var ResourceSinkGroup: String? = null
    var ThrusterType: String? = null
    var IsPublic: Boolean = true//???
    var GuiVisible: Boolean = false
    var SilenceableByShipSoundSystem: Boolean = false//???
    var BlockVariants: MutableList<BlockId>? = null

    var PCU: Int = 1
    var BuildTimeSeconds: Float? = 10f
    var DisassembleRatio: Float? = null
    var DeformationRatio: Float? = null
    var GeneralDamageMultiplier: Float? = null
    var InventoryMaxVolume: Float = 1.0f
    var InventorySize: XYZFloat? = null
    var Component: MutableList<ComponentItem>? = null
    var CriticalComponent: MPair<String, Int>? = null

    var Icon: String? = null
    var Model: String? = null
    var BuildProgressModels: MutableList<BuildProgressModellItem>? = null
    var CubeSize: String = "Small"
    var IsAirTight: Boolean? = null
    var Size: XYZInt? = null
    var ModelOffset: XYZFloat? = null
    var Center: XYZInt? = null
    var Mirroring: XYZAxis? = null
    var MountPoints: MutableList<MountPointItem>? = null
    var BlockTopology: String? = null
    var DamageEffectId: Int? = null
    var DamageEffectName: String? = null
    var DamagedSound: String? = null
    var PrimarySound: String? = null
    var ActionSound: String? = null
    var DestroySound: String? = null
    var DestroyEffect: String? = null
    var GenerateSound: String? = null
    var IdleSound: String? = null
    var OverlayTexture: String? = null
    var UseModelIntersection: Boolean = false
    var AutorotateMode: String? = null

    var RequiredPowerInput: Float? = null
    var BasePowerInput: Float? = null
    var ConsumptionPower: Float? = null
    var MaxPowerOutput: Float? = null
    var StandbyPowerConsumption: Float? = null
    var OperationalPowerConsumption: Float? = null
    var PowerInput: Float? = null
    var PowerConsumptionIdle: Float? = null
    var PowerConsumptionMoving: Float? = null
    var MaxPowerConsumption: Float? = null
    var MinPowerConsumption: Float? = null
    var MaxStoredPower: Float? = null
    var InitialStoredPowerRatio: Float? = null
    var FuelCapacity: Float? = null
    var FuelProductionToCapacityMultiplier: Float? = null


    var FlameFullColor: Color? = null
    var FlameIdleColor: Color? = null
    var FlameDamageLengthScale: Float? = null
    var FlameLengthScale: Float? = null
    var NeedsAtmosphereForInfluence: Boolean? = null
    var MinPlanetaryInfluence: Float? = null
    var MaxPlanetaryInfluence: Float? = null
    var EffectivenessAtMinInfluence: Float? = null
    var EffectivenessAtMaxInfluence: Float? = null
    var SlowdownFactor: Float? = null
    var FlamePointMaterial: String? = null
    var FlameLengthMaterial: String? = null
    var FlameFlare: String? = null
    var FlameVisibilityDistance: Int? = null
    var FlameGlareQuerySize: Float? = null
    var ForceMagnitude: Double? = null

    var PowerNeededForJump: Int? = null
    var MaxJumpDistance: Int? = null
    var MaxJumpMass: Int? = null
    var RefineSpeed: Float? = null
    var MaterialEfficiency: Float? = null
    var SensorRadius: Float? = null
    var SensorOffset: Float? = null
    var CutOutRadius: Float? = null
    var CutOutOffset: Float? = null
    var EmissiveColorPreset: String? = null
    var OreAmountPerPullRequest: Int? = null
    var Capacity: Long? = null
    var MaximumRange: Int? = null

    var WeaponDefinitionId: String? = null
    var MinElevationDegrees: Int? = null
    var MaxElevationDegrees: Int? = null
    var MinAzimuthDegrees: Int? = null
    var MaxAzimuthDegrees: Int? = null
    var RotationSpeed: Float? = null
    var ElevationSpeed: Float? = null
    var IdleRotation: Boolean? = null
    var MaxRangeMeters: Int? = null
    var MinFov: Float? = null
    var MaxFov: Float? = null

    var MaxForceMagnitude: Float? = null
    var DangerousTorqueThreshold: Float? = null
    var RotorPart: String? = null
    var PropulsionForce: Int? = null
    var MinHeight: Float? = null
    var MaxHeight: Float? = null
    var SafetyDetach: Int? = null
    var SafetyDetachMax: Int? = null
    var AxleFriction: Int? = null
    var SteeringSpeed: Float? = null
    var AirShockMinSpeed: Int? = null
    var AirShockMaxSpeed: Int? = null
    var AirShockActivationDelay: Int? = null

    var MaterialSubtype: String? = null
    var MaterialDeployCost: Int? = null
    var RadiusMultiplier: Float? = null
    var MinimumAtmosphereLevel: Float? = null
    var ReefAtmosphereLevel: Float? = null
    var DragCoefficient: Float? = null

    var IceConsumptionPerSecond: Int? = null


    val criticalIndex : Int get() {
            var z = CriticalComponent!!.second
            for ((j, x) in Component!!.withIndex()) {
                if (CriticalComponent!!.first == x.component) {
                    if (z == 0) return j
                    else z--;
                }
            }

            return 0
        }



    companion object {
        val REGEX_DOCTYPE = Regex("^<\\?xml( (\\w+=\"[^\"]*\"))*\\?>\r?\n?")

        const val TAG_ID = "Id"
        const val TAG_DISPLAY_NAME = "DisplayName"
        const val TAG_DESCRIPTION = "Description"
        const val TAG_BLOCK_PAIR_NAME = "BlockPairName"
        const val TAG_EDGE_TYPE = "EdgeType"
        const val TAG_RESOURCE_SOURCE_GROUP = "ResourceSourceGroup"
        const val TAG_RESOURCE_SINK_GROUP = "ResourceSinkGroup"
        const val TAG_PUBLIC = "Public"
        const val TAG_GUI_VISIBLE = "GuiVisible"
        const val TAG_THRUSTER_TYPE = "ThrusterType"
        const val TAG_SILENCEABLE_BY_SHIP_SOUND_SYSTEM = "SilenceableByShipSoundSystem"
        const val TAG_BLOCK_VARIANTS = "BlockVariants"

        const val TAG_PCU = "PCU"
        const val TAG_BUILD_TIME_SECONDS = "BuildTimeSeconds"
        const val TAG_DISASSEMBLE_RATIO = "DisassembleRatio"
        const val TAG_DEFORMATION_RATIO = "DeformationRatio"
        const val TAG_INVENTORY_MAX_VOLUME = "InventoryMaxVolume"
        const val TAG_INVENTORY_SIZE = "InventorySize"
        const val TAG_COMPONENTS = "Components"
        const val TAG_CRITICAL_COMPONENT = "CriticalComponent"

        const val TAG_ICON = "Icon"
        const val TAG_MODEL = "Model"
        const val TAG_BUILD_PROGRESS_MODELS = "BuildProgressModels"
        const val TAG_CUBE_SIZE = "CubeSize"
        const val TAG_IS_AIR_TIGHT = "IsAirTight"
        const val TAG_SIZE = "Size"
        const val TAG_MODEL_OFFSET = "ModelOffset"
        const val TAG_CENTER = "Center"
        const val TAG_MIRRORING_X = "MirroringX"
        const val TAG_MIRRORING_Y = "MirroringY"
        const val TAG_MIRRORING_Z = "MirroringZ"
        const val TAG_MOUNT_POINTS = "MountPoints"
        const val TAG_BLOCK_TOPOLOGY = "BlockTopology"
        const val TAG_DAMAGE_EFFECT_ID = "DamageEffectId"
        const val TAG_DAMAGE_EFFECT_NAME = "DamageEffectName"
        const val TAG_DAMAGED_SOUND = "DamagedSound"
        const val TAG_PRIMARY_SOUND = "PrimarySound"
        const val TAG_ACTION_SOUND = "ActionSound"
        const val TAG_DESTROY_SOUND = "DestroySound"
        const val TAG_DESTROY_EFFECT = "DestroyEffect"
        const val TAG_GENERATE_SOUND = "GenerateSound"
        const val TAG_IDLE_SOUND = "IdleSound"
        const val TAG_OVERLAY_TEXTURE = "OverlayTexture"
        const val TAG_USE_MODEL_INTERSECTION = "UseModelIntersection"
        const val TAG_AUTOROTATE_MODE = "AutorotateMode"

        const val TAG_REQUIRED_POWER_INPUT = "RequiredPowerInput"
        const val TAG_BASE_POWER_INPUT = "BasePowerInput"
        const val TAG_CONSUMPTION_POWER = "ConsumptionPower"
        const val TAG_MAX_POWER_OUTPUT = "MaxPowerOutput"
        const val TAG_STAND_BY_POWER_CONSUMPTION = "StandbyPowerConsumption"
        const val TAG_OPERATIONAL_POWER_CONSUMPTION = "OperationalPowerConsumption"
        const val TAG_POWER_INPUT = "PowerInput"
        const val TAG_POWER_CONSUMPTION_IDLE = "PowerConsumptionIdle"
        const val TAG_POWER_CONSUMPTION_MOVING = "PowerConsumptionMoving"
        const val TAG_MAX_POWER_CONSUMPTION = "MaxPowerConsumption"
        const val TAG_MIN_POWER_CONSUMPTION = "MinPowerConsumption"
        const val TAG_MAX_STORED_POWER = "MaxStoredPower"
        const val TAG_INITIAL_STORED_POWER_RATIO = "InitialStoredPowerRatio"

        const val TAG_FLAME_FULL_COLOR = "FlameFullColor"
        const val TAG_FLAME_IDLE_COLOR = "FlameIdleColor"
        const val TAG_FLAME_DAMAGE_LENGTH_SCALE = "FlameDamageLengthScale"
        const val TAG_FLAME_LENGTH_SCALE = "FlameLengthScale"
        const val TAG_NEEDS_ATMOSPHERE_FOR_INFLUENCE = "NeedsAtmosphereForInfluence"
        const val TAG_MIN_PLANETARY_INFLUENCE = "MinPlanetaryInfluence"
        const val TAG_MAX_PLANETARY_INFLUENCE = "MaxPlanetaryInfluence"
        const val TAG_EFFECTIVENESS_AT_MIN_INFLUENCE = "EffectivenessAtMinInfluence"
        const val TAG_EFFECTIVENESS_AT_MAX_INFLUENCE = "EffectivenessAtMaxInfluence"
        const val TAG_SLOWDOWN_FACTOR = "SlowdownFactor"
        const val TAG_FLAME_POINT_MATERIAL = "FlamePointMaterial"
        const val TAG_FLAME_LENGTH_MATERIAL = "FlameLengthMaterial"
        const val TAG_FLAME_FLARE = "FlameFlare"
        const val TAG_FLAME_VISIBILITY_DISTANCE = "FlameVisibilityDistance"
        const val TAG_FLAME_GLARE_QUERY_SIZE = "FlameGlareQuerySize"
        const val TAG_FORCE_MAGNITUDE = "ForceMagnitude"
        const val TAG_GENERAL_DAMAGE_MULTIPLIER = "GeneralDamageMultiplier"

        const val TAG_POWER_NEEDED_FOR_JUMP = "PowerNeededForJump"
        const val TAG_MAX_JUMP_DISTANCE = "MaxJumpDistance"
        const val TAG_MAX_JUMP_MASS = "MaxJumpMass"
        const val TAG_REFINE_SPEED = "RefineSpeed"
        const val TAG_MATERIAL_EFFICIENCY = "MaterialEfficiency"
        const val TAG_SENSOR_RADIUS = "SensorRadius"
        const val TAG_SENSOR_OFFSET = "SensorOffset"
        const val TAG_CUT_OUT_RADIUS = "CutOutRadius"
        const val TAG_CUT_OUT_OFFSET = "CutOutOffset"
        const val TAG_EMISSIVE_COLOR_PRESET = "EmissiveColorPreset"
        const val TAG_ORE_AMOUNT_PER_PULL_REQUEST = "OreAmountPerPullRequest"
        const val TAG_CAPACITY = "Capacity"
        const val TAG_MAXIMUM_RANGE = "MaximumRange"

        const val TAG_WEAPON_DEFINITION_ID = "WeaponDefinitionId"
        const val TAG_MIN_ELEVATION_DEGREES = "MinElevationDegrees"
        const val TAG_MAX_ELEVATION_DEGREES = "MaxElevationDegrees"
        const val TAG_MIN_AZIMUTH_DEGREES = "MinAzimuthDegrees"
        const val TAG_MAX_AZIMUTH_DEGREES = "MaxAzimuthDegrees"
        const val TAG_ROTATION_SPEED = "RotationSpeed"
        const val TAG_ELEVATION_SPEED = "ElevationSpeed"
        const val TAG_IDLE_ROTATION = "IdleRotation"
        const val TAG_MAX_RANGE_METERS = "MaxRangeMeters"
        const val TAG_MIN_FOV = "MinFov"
        const val TAG_MAX_FOV = "MaxFov"

        const val TAG_MAX_FORCE_MAGNITUDE = "MaxForceMagnitude"
        const val TAG_DANGEROUS_TORQUE_THRESHOLD = "DangerousTorqueThreshold"
        const val TAG_ROTOR_PART = "RotorPart"
        const val TAG_PROPULSION_FORCE = "PropulsionForce"
        const val TAG_MIN_HEIGHT = "MinHeight"
        const val TAG_MAX_HEIGHT = "MaxHeight"
        const val TAG_SAFETY_DETACH = "SafetyDetach"
        const val TAG_SAFETY_DETACHMAX = "SafetyDetachMax"
        const val TAG_AXLE_FRICTION = "AxleFriction"
        const val TAG_STEERING_SPEED = "SteeringSpeed"
        const val TAG_AIR_SHOCK_MIN_SPEED = "AirShockMinSpeed"
        const val TAG_AIR_SHOCK_MAX_SPEED = "AirShockMaxSpeed"
        const val TAG_AIR_SHOCK_ACTIVATION_DELAY = "AirShockActivationDelay"
        const val TAG_FUEL_CAPACITY = "FuelCapacity"
        const val TAG_FUEL_PRODUCTION_TO_CAPACITY_MULTIPLIER = "FuelProductionToCapacityMultiplier"

        const val TAG_MATERIAL_SUBTYPE = "MaterialSubtype"
        const val TAG_MATERIAL_DEPLOY_COST = "MaterialDeployCost"
        const val TAG_RADIUS_MULTIPLIER = "RadiusMultiplier"
        const val TAG_MINIMUM_ATMOSPHERE_LEVEL = "MinimumAtmosphereLevel"
        const val TAG_REEF_ATMOSPHERE_LEVEL = "ReefAtmosphereLevel"
        const val TAG_DRAG_COEFFICIENT = "DragCoefficient"

        const val TAG_ICE_CONSUMPTION_PER_SECOND = "IceConsumptionPerSecond"


        val EXISTING_TAGS = setOf(
            TAG_ID,


            TAG_FUEL_CAPACITY,
            TAG_FUEL_PRODUCTION_TO_CAPACITY_MULTIPLIER,
            TAG_GENERAL_DAMAGE_MULTIPLIER,

            TAG_DISPLAY_NAME,
            TAG_DESCRIPTION,
            TAG_BLOCK_PAIR_NAME,
            TAG_EDGE_TYPE,
            TAG_RESOURCE_SOURCE_GROUP,
            TAG_RESOURCE_SINK_GROUP,
            TAG_PUBLIC,
            TAG_GUI_VISIBLE,
            TAG_THRUSTER_TYPE,
            TAG_SILENCEABLE_BY_SHIP_SOUND_SYSTEM,
            TAG_BLOCK_VARIANTS,

            TAG_PCU,
            TAG_BUILD_TIME_SECONDS,
            TAG_DISASSEMBLE_RATIO,
            TAG_DEFORMATION_RATIO,
            TAG_INVENTORY_MAX_VOLUME,
            TAG_INVENTORY_SIZE,
            TAG_COMPONENTS,
            TAG_CRITICAL_COMPONENT,

            TAG_ICON,
            TAG_MODEL,
            TAG_BUILD_PROGRESS_MODELS,
            TAG_CUBE_SIZE,
            TAG_IS_AIR_TIGHT,
            TAG_SIZE,
            TAG_MODEL_OFFSET,
            TAG_CENTER,
            TAG_MIRRORING_X,
            TAG_MIRRORING_Y,
            TAG_MIRRORING_Z,
            TAG_MOUNT_POINTS,
            TAG_BLOCK_TOPOLOGY,
            TAG_DAMAGE_EFFECT_ID,
            TAG_DAMAGE_EFFECT_NAME,
            TAG_DAMAGED_SOUND,
            TAG_PRIMARY_SOUND,
            TAG_ACTION_SOUND,
            TAG_DESTROY_SOUND,
            TAG_DESTROY_EFFECT,
            TAG_GENERATE_SOUND,
            TAG_IDLE_SOUND,
            TAG_OVERLAY_TEXTURE,
            TAG_USE_MODEL_INTERSECTION,
            TAG_AUTOROTATE_MODE,

            TAG_REQUIRED_POWER_INPUT,
            TAG_BASE_POWER_INPUT,
            TAG_CONSUMPTION_POWER,
            TAG_MAX_POWER_OUTPUT,
            TAG_STAND_BY_POWER_CONSUMPTION,
            TAG_OPERATIONAL_POWER_CONSUMPTION,
            TAG_POWER_INPUT,
            TAG_POWER_CONSUMPTION_IDLE,
            TAG_POWER_CONSUMPTION_MOVING,
            TAG_MAX_POWER_CONSUMPTION,
            TAG_MIN_POWER_CONSUMPTION,
            TAG_MAX_STORED_POWER,
            TAG_INITIAL_STORED_POWER_RATIO,

            TAG_FLAME_FULL_COLOR,
            TAG_FLAME_IDLE_COLOR,
            TAG_FLAME_DAMAGE_LENGTH_SCALE,
            TAG_FLAME_LENGTH_SCALE,
            TAG_NEEDS_ATMOSPHERE_FOR_INFLUENCE,
            TAG_MIN_PLANETARY_INFLUENCE,
            TAG_MAX_PLANETARY_INFLUENCE,
            TAG_EFFECTIVENESS_AT_MIN_INFLUENCE,
            TAG_EFFECTIVENESS_AT_MAX_INFLUENCE,
            TAG_SLOWDOWN_FACTOR,
            TAG_FLAME_POINT_MATERIAL,
            TAG_FLAME_LENGTH_MATERIAL,
            TAG_FLAME_FLARE,
            TAG_FLAME_VISIBILITY_DISTANCE,
            TAG_FLAME_GLARE_QUERY_SIZE,
            TAG_FORCE_MAGNITUDE,

            TAG_POWER_NEEDED_FOR_JUMP,
            TAG_MAX_JUMP_DISTANCE,
            TAG_MAX_JUMP_MASS,
            TAG_REFINE_SPEED,
            TAG_MATERIAL_EFFICIENCY,
            TAG_SENSOR_RADIUS,
            TAG_SENSOR_OFFSET,
            TAG_CUT_OUT_RADIUS,
            TAG_CUT_OUT_OFFSET,
            TAG_EMISSIVE_COLOR_PRESET,
            TAG_ORE_AMOUNT_PER_PULL_REQUEST,
            TAG_CAPACITY,
            TAG_MAXIMUM_RANGE,

            TAG_WEAPON_DEFINITION_ID,
            TAG_MIN_ELEVATION_DEGREES,
            TAG_MAX_ELEVATION_DEGREES,
            TAG_MIN_AZIMUTH_DEGREES,
            TAG_MAX_AZIMUTH_DEGREES,
            TAG_ROTATION_SPEED,
            TAG_ELEVATION_SPEED,
            TAG_IDLE_ROTATION,
            TAG_MAX_RANGE_METERS,
            TAG_MIN_FOV,
            TAG_MAX_FOV,

            TAG_MAX_FORCE_MAGNITUDE,
            TAG_DANGEROUS_TORQUE_THRESHOLD,
            TAG_ROTOR_PART,
            TAG_PROPULSION_FORCE,
            TAG_MIN_HEIGHT,
            TAG_MAX_HEIGHT,
            TAG_SAFETY_DETACH,
            TAG_SAFETY_DETACHMAX,
            TAG_AXLE_FRICTION,
            TAG_STEERING_SPEED,
            TAG_AIR_SHOCK_MIN_SPEED,
            TAG_AIR_SHOCK_MAX_SPEED,
            TAG_AIR_SHOCK_ACTIVATION_DELAY,

            TAG_MATERIAL_SUBTYPE ,
            TAG_MATERIAL_DEPLOY_COST ,
            TAG_RADIUS_MULTIPLIER ,
            TAG_MINIMUM_ATMOSPHERE_LEVEL ,
            TAG_REEF_ATMOSPHERE_LEVEL ,
            TAG_DRAG_COEFFICIENT,

            TAG_ICE_CONSUMPTION_PER_SECOND)





        fun fromXML(element: Element): Block{
            val ret = Block()
            ret.XsiType = element.attr("xsi:type") {it.removePrefix("MyObjectBuilder_")}
            if(!element.hasChildNodes()){
                System.out.println("empty block!!!")
                return ret
            }
            ret.BlockId = element.child(TAG_ID)?.let { BlockId(it.child("TypeId")?.value(), it.child("SubtypeId")?.value()) }
            ret.DisplayName = element.child(TAG_DISPLAY_NAME)?.value()
            ret.Description = element.child(TAG_DESCRIPTION)?.value()
            ret.BlockPairName = element.child(TAG_BLOCK_PAIR_NAME)?.value()
            ret.EdgeType = element.child(TAG_EDGE_TYPE)?.value()
            ret.ResourceSourceGroup = element.child(TAG_RESOURCE_SOURCE_GROUP)?.value()
            ret.ResourceSinkGroup = element.child(TAG_RESOURCE_SINK_GROUP)?.value()
            ret.ThrusterType = element.child(TAG_THRUSTER_TYPE)?.value()
            ret.IsPublic = element.child(TAG_PUBLIC)?.value { it.toBoolean() } ?: ret.IsPublic
            ret.GuiVisible = element.child(TAG_GUI_VISIBLE)?.value { it.toBoolean() } ?: ret.GuiVisible
            ret.SilenceableByShipSoundSystem = element.child(TAG_SILENCEABLE_BY_SHIP_SOUND_SYSTEM)?.value { it.toBoolean() } ?: ret.SilenceableByShipSoundSystem
            ret.BlockVariants = element.child(TAG_BLOCK_VARIANTS)?.let {
                it.children("BlockVariant").asSequence().mapNotNull {
                    BlockId(it.child("TypeId")?.value(), it.child("SubtypeId")?.value())
                }.toMutableList()
            } ?: arrayListOf()

            ret.PCU = element.child(TAG_PCU)?.value { it.toIntOrNull() } ?: ret.PCU
            ret.BuildTimeSeconds = element.child(TAG_BUILD_TIME_SECONDS)?.value { it.toFloatOrNull() } ?: ret.BuildTimeSeconds
            ret.DisassembleRatio = element.child(TAG_DISASSEMBLE_RATIO)?.value { it.toFloatOrNull() }
            ret.DeformationRatio = element.child(TAG_DEFORMATION_RATIO)?.value { it.toFloatOrNull() }
            ret.InventoryMaxVolume = element.child(TAG_INVENTORY_MAX_VOLUME)?.value { it.toFloatOrNull() } ?: ret.InventoryMaxVolume
            ret.InventorySize = element.child(TAG_INVENTORY_SIZE)?.let {
                XYZFloat(
                        x = it.child("X")?.value { it.toFloat() } ?: 0.0f,
                        y = it.child("Y")?.value { it.toFloat() } ?: 0.0f,
                        z = it.child("Z")?.value { it.toFloat() } ?: 0.0f)
            }
            ret.Component = element.child(TAG_COMPONENTS)?.let {
                it.children("Component").asSequence().mapNotNull {
                    try{
                        ComponentItem(it.attr("Count") { it.toInt() } ?: 0, it.getAttribute("Subtype"), it.child("DeconstructId")?.let {
                            BlockId(it.child("TypeId")?.value(), it.child("SubtypeId")?.value())
                        })
                    } catch (e: NumberFormatException){//WTF
                        println("cannot parse component ${it.write2()} from ${element.write2()}")
                        e.printStackTrace(System.out)
                        null
                    }
                }.toMutableList()
            } ?: arrayListOf()
            ret.CriticalComponent = element.child(TAG_CRITICAL_COMPONENT)?.let {
                MPair(it.attr("Subtype") {it} ?: "", it.attr("Index") {it.toIntOrNull()} ?: 0)
            }

            ret.Icon = element.child(TAG_ICON)?.value()
            ret.Model = element.child(TAG_MODEL)?.value()
            ret.BuildProgressModels = element.child(TAG_BUILD_PROGRESS_MODELS)?.let{
                it.children("Model").asSequence().map {
                    BuildProgressModellItem(it.attr("BuildPercentUpperBound") { it.toFloat() } ?: 0.0f, it.getAttribute("File"))
                }.toMutableList()
            } ?: arrayListOf()
            ret.CubeSize = element.child(TAG_CUBE_SIZE)?.value() ?: ret.CubeSize
            ret.IsAirTight = element.child(TAG_IS_AIR_TIGHT)?.value { it.toBoolean() } ?: ret.IsAirTight
            ret.Size = element.child(TAG_SIZE)?.let { XYZInt.fromXML(it,1) }
            ret.ModelOffset = element.child(TAG_MODEL_OFFSET)?.let { XYZFloat.fromXML(it,1f) }
            ret.Center = element.child(TAG_CENTER)?.let { XYZInt.fromXML(it,1) }
            ret.Mirroring = XYZAxis(
                    x = element.child(TAG_MIRRORING_X)?.value() ?: "",// { Axis.valueOf(it.toUpperCase()) } ?: Companion.Axis.X,
                    y = element.child(TAG_MIRRORING_Y)?.value() ?: "",// { Axis.valueOf(it.toUpperCase()) } ?: Companion.Axis.Y,
                    z = element.child(TAG_MIRRORING_Z)?.value() ?: ""// { Axis.valueOf(it.toUpperCase()) } ?: Companion.Axis.Z
            )
            ret.MountPoints = element.child(TAG_MOUNT_POINTS)?.let {
                it.children("MountPoint").asSequence().map {
                    MountPointItem(
                            side = it.getAttribute("Side"),
                            startX = it.attr("StartX") { it.toFloat() } ?: 0.0f,
                            endX = it.attr("EndX") { it.toFloat() } ?: 0.0f,
                            startY = it.attr("StartY") { it.toFloat() } ?: 0.0f,
                            endY = it.attr("EndY") { it.toFloat() } ?: 0.0f,
                            default = it.attr("Default") { it.toBoolean() } ?: false)
                }.toMutableList()
            } ?: ret.MountPoints



            ret.readStrings(element, Block::BlockTopology,Block::DamageEffectName,Block::DamagedSound,Block::PrimarySound,Block::ActionSound,Block::DestroySound,Block::DestroyEffect,Block::GenerateSound,Block::IdleSound,Block::OverlayTexture,Block::AutorotateMode,Block::MaterialSubtype)
            ret.readFloats(element, Block::RequiredPowerInput, Block::FuelCapacity, Block::FuelProductionToCapacityMultiplier, Block::BasePowerInput, Block::ConsumptionPower, Block::MaxPowerOutput, Block::StandbyPowerConsumption, Block::OperationalPowerConsumption, Block::PowerInput, Block::PowerConsumptionIdle, Block::PowerConsumptionMoving, Block::MaxPowerConsumption, Block::MinPowerConsumption, Block::MaxStoredPower, Block::InitialStoredPowerRatio, Block::FlameDamageLengthScale, Block::FlameLengthScale, Block::MinPlanetaryInfluence, Block::MaxPlanetaryInfluence, Block::EffectivenessAtMinInfluence, Block::EffectivenessAtMaxInfluence, Block::SlowdownFactor, Block::FlameGlareQuerySize, Block::RefineSpeed, Block::MaterialEfficiency, Block::SensorRadius, Block::SensorOffset, Block::CutOutRadius, Block::CutOutOffset, Block::SteeringSpeed, Block::RotationSpeed, Block::ElevationSpeed, Block::MinFov, Block::MaxFov, Block::MinHeight, Block::MaxHeight, Block::MaxForceMagnitude, Block::DangerousTorqueThreshold, Block::RadiusMultiplier, Block::MinimumAtmosphereLevel, Block::ReefAtmosphereLevel, Block::DragCoefficient)
            ret.readInts(element, Block::MinElevationDegrees,Block::MaxElevationDegrees,Block::MinAzimuthDegrees,Block::MaxAzimuthDegrees,Block::OreAmountPerPullRequest,Block::PowerNeededForJump,Block::MaxJumpDistance,Block::MaxJumpMass,Block::FlameVisibilityDistance,Block::MaximumRange,Block::PropulsionForce,Block::SafetyDetach,Block::SafetyDetachMax,Block::AxleFriction,Block::AirShockMinSpeed,Block::AirShockMaxSpeed,Block::AirShockActivationDelay,Block::MaterialDeployCost)




            ret.DamageEffectId = element.child(TAG_DAMAGE_EFFECT_ID)?.value{it.toIntOrNull()}
            ret.UseModelIntersection = element.child(TAG_USE_MODEL_INTERSECTION)?.value{it.toBoolean()} ?: ret.UseModelIntersection
            ret.NeedsAtmosphereForInfluence = element.child(TAG_NEEDS_ATMOSPHERE_FOR_INFLUENCE)?.value { it.toBoolean() } ?: ret.NeedsAtmosphereForInfluence


            ret.FlameFullColor = element.child(TAG_FLAME_FULL_COLOR)?.let { colorFromXML (it) }
            ret.FlameIdleColor = element.child(TAG_FLAME_IDLE_COLOR)?.let { colorFromXML (it) }



            ret.FlamePointMaterial = element.child(TAG_FLAME_POINT_MATERIAL)?.value() ?: ret.FlamePointMaterial
            ret.FlameLengthMaterial = element.child(TAG_FLAME_LENGTH_MATERIAL)?.value() ?: ret.FlameLengthMaterial
            ret.FlameFlare = element.child(TAG_FLAME_FLARE)?.value() ?: ret.FlameFlare




            ret.ForceMagnitude = element.child(TAG_FORCE_MAGNITUDE)?.value { it.toDouble() } ?: ret.ForceMagnitude
            ret.GeneralDamageMultiplier = element.child(TAG_GENERAL_DAMAGE_MULTIPLIER)?.value { it.toFloat() } ?: ret.GeneralDamageMultiplier



            ret.EmissiveColorPreset = element.child(TAG_EMISSIVE_COLOR_PRESET)?.value() ?: ret.EmissiveColorPreset

            ret.Capacity = element.child(TAG_CAPACITY)?.value { it.toLong() } ?: ret.Capacity


            ret.WeaponDefinitionId = element.child(TAG_WEAPON_DEFINITION_ID)?.attr("Subtype") { it } ?: ret.WeaponDefinitionId


            ret.IdleRotation = element.child(TAG_IDLE_ROTATION)?.value { it.toBoolean() } ?: ret.IdleRotation
            ret.MaxRangeMeters = element.child(TAG_MAX_RANGE_METERS)?.value { it.toInt() } ?: ret.MaxRangeMeters


            ret.RotorPart = element.child(TAG_ROTOR_PART)?.value () ?: ret.RotorPart

            ret.IceConsumptionPerSecond = element.child(TAG_ICE_CONSUMPTION_PER_SECOND)?.value{ it.toInt() } ?: ret.IceConsumptionPerSecond


            element.eachChildren {
                if(it is Element && !EXISTING_TAGS.contains(it.tagName)){
                    val append = it.write2().replace(REGEX_DOCTYPE, "")
                    ret.Raw = ret.Raw.let { if(it.isNullOrBlank()) append else it + "\n" + append }
                }
            }
            return ret
        }


        fun toXMLnoMove(block: Block, root: Element, doc: Document): Element {
            root.attrR("xsi:type", block.XsiType) { if(!it.isNullOrEmpty()) { "MyObjectBuilder_$it" } else { null } }
            val tagToElement = HashMap<String, Element>()
            root.eachChildren {
                if(it is Element) {
                    tagToElement[it.tagName] = it
                }
            }
            doc.addChangeOrDelExt(root, TAG_ID, block.BlockId, tagToElement) {
                doc.addOrChange(this, "TypeId", it.blockId) { value(it) }
                doc.addOrChange(this, "SubtypeId", it.blockSubtypeId) { value(it) }
            }
            doc.addChangeOrDelExt(root, TAG_DISPLAY_NAME, block.DisplayName, tagToElement) { value(it) }
            doc.addChangeOrDelExt(root, TAG_DESCRIPTION, block.Description, tagToElement) { value(it) }
            doc.addChangeOrDelExt(root, TAG_BLOCK_PAIR_NAME, block.BlockPairName, tagToElement) { value(it) }
            doc.addChangeOrDelExt(root, TAG_BLOCK_TOPOLOGY, block.BlockTopology.takeUnless { it.isNullOrBlank() }, tagToElement) { value(it) }
            doc.addChangeOrDelExt(root, TAG_EDGE_TYPE, block.EdgeType, tagToElement) { value(it) }
            doc.addChangeOrDelExt(root, TAG_RESOURCE_SOURCE_GROUP, block.ResourceSourceGroup, tagToElement) { value(it) }
            doc.addChangeOrDelExt(root, TAG_RESOURCE_SINK_GROUP, block.ResourceSinkGroup, tagToElement) { value(it) }
            doc.addChangeOrDelExt(root, TAG_THRUSTER_TYPE, block.ThrusterType, tagToElement) { value(it) }
            doc.addChangeOrDelExt(root, TAG_PUBLIC, block.IsPublic, tagToElement) { value(it) }
            doc.addChangeOrDelExt(root, TAG_GUI_VISIBLE, block.GuiVisible, tagToElement) { value(it) }
            doc.addChangeOrDelExt(root, TAG_SILENCEABLE_BY_SHIP_SOUND_SYSTEM, block.SilenceableByShipSoundSystem, tagToElement) { value(it) }
            doc.addChangeOrDelExt(root, TAG_BLOCK_VARIANTS, block.BlockVariants.takeUnless { it.isNullOrEmpty() }, tagToElement) {
                //??? В будущем возможно стоит сделать сверку ID
                changeSubValuesList("BlockVariant", it, { item ->
                    doc.addOrChange(this, "TypeId", item.blockId) { value(it) }
                    doc.addOrChange(this, "SubtypeId", item.blockSubtypeId) { value(it) }
                }, { item ->
                    addChildren {
                        arrayListOf(
                            element("TypeId").value(item.blockId),
                            element("SubtypeId").value(item.blockSubtypeId)
                        )
                    }
                })
            }

            doc.addChangeOrDelExt(root, TAG_PCU, block.PCU.takeIf { it != 1 }, tagToElement) { value(it) }
            doc.addOrChangeExt(root, TAG_BUILD_TIME_SECONDS, block.BuildTimeSeconds, tagToElement) { value(it) }//original has not contain ?.
            doc.addChangeOrDelExt(root, TAG_DISASSEMBLE_RATIO, block.DisassembleRatio, tagToElement) { value(it) }
            doc.addChangeOrDelExt(root, TAG_DEFORMATION_RATIO, block.DeformationRatio, tagToElement) { value(it) }
            doc.addOrChangeExt(root, TAG_INVENTORY_MAX_VOLUME, block.InventoryMaxVolume, tagToElement) { value(it) }//original has not contain ?.
            doc.addChangeOrDelExt(root, TAG_INVENTORY_SIZE, block.InventorySize, tagToElement) {
                doc.addOrChange(this, "X", it.x) { value(it) }
                doc.addOrChange(this, "Y", it.y) { value(it) }
                doc.addOrChange(this, "Z", it.z) { value(it) }
            }
            doc.addChangeOrDelExt(root, TAG_COMPONENTS, block.Component, tagToElement) {
                //??? В будущем возможно стоит сделать сверку ID
                changeSubValuesList("Component", it, { item ->
                    attr("Count", item.amount).attrR("Subtype", item.component)
                    doc.addChangeOrDel(this, "DeconstructId", item.deconstructId) {
                        doc.addOrChange(this, "TypeId", it.blockId) { value(it) }
                        doc.addOrChange(this, "SubtypeId", it.blockSubtypeId) { value(it) }
                    }
                }, { item ->
                    attr("Count", item.amount).attr("Subtype", item.component)
                    item.deconstructId?.let {
                        addChild {
                            element("DeconstructId").addChildren {
                                arrayListOf(
                                    element("TypeId").value(it.blockId),
                                    element("SubtypeId").value(it.blockSubtypeId)
                                )
                            }
                        }
                    }
                })
            }
            doc.addChangeOrDelExt(root, TAG_CRITICAL_COMPONENT, block.CriticalComponent, tagToElement) { 
                attr("Subtype", it.first).attr("Index", it.second)
            }

            doc.addChangeOrDelExt(root, TAG_ICON, block.Icon, tagToElement) { value(it) }
            doc.addChangeOrDelExt(root, TAG_MODEL, block.Model, tagToElement) { value(it) }
            doc.addChangeOrDelExt(root, TAG_BUILD_PROGRESS_MODELS, block.BuildProgressModels, tagToElement) {
                //??? В будущем возможно стоит сделать сверку File
                changeSubValuesList("Model", it, { item ->
                    attr("BuildPercentUpperBound", item.progress).attrR("File", item.file)
                }, { item ->
                    attr("BuildPercentUpperBound", item.progress).attrR("File", item.file)
                })
            }
            doc.addOrChangeExt(root, TAG_CUBE_SIZE, block.CubeSize, tagToElement) { value(it) }//original has not contain ?.
            doc.addChangeOrDelExt(root, TAG_IS_AIR_TIGHT, block.IsAirTight, tagToElement) { value(it) }//original has not contain ?.
            doc.addChangeOrDelExt(root, TAG_SIZE, block.Size, tagToElement) {
                attr("x", it.x).attr("y", it.y).attr("z", it.z)
            }
            doc.addChangeOrDelExt(root, TAG_MODEL_OFFSET, block.ModelOffset, tagToElement) {
                attr("x", it.x).attr("y", it.y).attr("z", it.z)
            }
            doc.addChangeOrDelExt(root, TAG_CENTER, block.Center, tagToElement) {
                attr("x", it.x).attr("y", it.y).attr("z", it.z)
            }
            block.Mirroring?.also {
                doc.addChangeOrDelExt(root, TAG_MIRRORING_X, it.x.takeIf { it.isNotBlank() }, tagToElement) { value(it) }
                doc.addChangeOrDelExt(root, TAG_MIRRORING_Y, it.y.takeIf { it.isNotBlank() }, tagToElement) { value(it) }
                doc.addChangeOrDelExt(root, TAG_MIRRORING_Z, it.z.takeIf { it.isNotBlank() }, tagToElement) { value(it) }
            }

            doc.addChangeOrDelExt(root, TAG_MOUNT_POINTS, block.MountPoints, tagToElement) {
                //??? В будущем возможно стоит сделать сверку Side, StartX, ..., EndY
                changeSubValuesList("MountPoint", it, { item ->
                    attr("Side", item.side)
                    .attr("StartX", item.startX)
                    .attr("StartY", item.startY)
                    .attr("EndX", item.endX)
                    .attr("EndY", item.endY)
                    .attrR("Default", item.default.takeIf { it })//true or null
                }, { item ->
                    attr("Side", item.side)
                    .attr("StartX", item.startX)
                    .attr("StartY", item.startY)
                    .attr("EndX", item.endX)
                    .attr("EndY", item.endY)
                    .attr("Default", item.default.takeIf { it })//true or null
                })
            }

            doc.addChangeOrDelExt(root, TAG_DAMAGE_EFFECT_ID, block.DamageEffectId, tagToElement) { value(it) }
            doc.addChangeOrDelExt(root, TAG_DAMAGE_EFFECT_NAME, block.DamageEffectName.takeUnless { it.isNullOrBlank() }, tagToElement) { value(it) }
            doc.addChangeOrDelExt(root, TAG_DAMAGED_SOUND, block.DamagedSound.takeUnless { it.isNullOrBlank() }, tagToElement) { value(it) }
            doc.addChangeOrDelExt(root, TAG_PRIMARY_SOUND, block.PrimarySound.takeUnless { it.isNullOrBlank() }, tagToElement) { value(it) }
            doc.addChangeOrDelExt(root, TAG_ACTION_SOUND, block.ActionSound.takeUnless { it.isNullOrBlank() }, tagToElement) { value(it) }
            doc.addChangeOrDelExt(root, TAG_DESTROY_SOUND, block.DestroySound.takeUnless { it.isNullOrBlank() }, tagToElement) { value(it) }
            doc.addChangeOrDelExt(root, TAG_DESTROY_EFFECT, block.DestroyEffect.takeUnless { it.isNullOrBlank() }, tagToElement) { value(it) }
            doc.addChangeOrDelExt(root, TAG_GENERATE_SOUND, block.GenerateSound.takeUnless { it.isNullOrBlank() }, tagToElement) { value(it) }
            doc.addChangeOrDelExt(root, TAG_IDLE_SOUND, block.IdleSound.takeUnless { it.isNullOrBlank() }, tagToElement) { value(it) }
            doc.addChangeOrDelExt(root, TAG_OVERLAY_TEXTURE, block.OverlayTexture.takeUnless { it.isNullOrBlank() }, tagToElement) { value(it) }
            doc.addOrChangeExt(root, TAG_USE_MODEL_INTERSECTION, block.UseModelIntersection, tagToElement) { value(it) }//original has not contain ?.
            doc.addChangeOrDelExt(root, TAG_AUTOROTATE_MODE, block.AutorotateMode.takeUnless { it.isNullOrBlank() }, tagToElement) { value(it) }

            doc.addChangeOrDelExt(root, TAG_REQUIRED_POWER_INPUT, block.RequiredPowerInput, tagToElement) { value(it) }
            doc.addChangeOrDelExt(root, TAG_BASE_POWER_INPUT, block.BasePowerInput, tagToElement) { value(it) }
            doc.addChangeOrDelExt(root, TAG_CONSUMPTION_POWER, block.ConsumptionPower, tagToElement) { value(it) }
            doc.addChangeOrDelExt(root, TAG_MAX_POWER_OUTPUT, block.MaxPowerOutput, tagToElement) { value(it) }
            doc.addChangeOrDelExt(root, TAG_STAND_BY_POWER_CONSUMPTION, block.StandbyPowerConsumption, tagToElement) { value(it) }
            doc.addChangeOrDelExt(root, TAG_OPERATIONAL_POWER_CONSUMPTION, block.OperationalPowerConsumption, tagToElement) { value(it) }
            doc.addChangeOrDelExt(root, TAG_POWER_INPUT, block.PowerInput, tagToElement) { value(it) }
            doc.addChangeOrDelExt(root, TAG_POWER_CONSUMPTION_IDLE, block.PowerConsumptionIdle, tagToElement) { value(it) }
            doc.addChangeOrDelExt(root, TAG_POWER_CONSUMPTION_MOVING, block.PowerConsumptionMoving, tagToElement) { value(it) }
            doc.addChangeOrDelExt(root, TAG_MAX_POWER_CONSUMPTION, block.MaxPowerConsumption, tagToElement) { value(it) }
            doc.addChangeOrDelExt(root, TAG_MIN_POWER_CONSUMPTION, block.MinPowerConsumption, tagToElement) { value(it) }
            doc.addChangeOrDelExt(root, TAG_MAX_STORED_POWER, block.MaxStoredPower, tagToElement) { value(it) }
            doc.addChangeOrDelExt(root, TAG_INITIAL_STORED_POWER_RATIO, block.InitialStoredPowerRatio, tagToElement) { value(it) }

            doc.addChangeOrDelExt(root, TAG_FLAME_FULL_COLOR, block.FlameFullColor, tagToElement) {
                doc.addOrChange(this, "X", it.red) { value(it) }
                doc.addOrChange(this, "Y", it.green) { value(it) }
                doc.addOrChange(this, "Z", it.blue) { value(it) }
                doc.addOrChange(this, "W", it.opacity) { value(it) }
            }
            doc.addChangeOrDelExt(root, TAG_FLAME_IDLE_COLOR, block.FlameIdleColor, tagToElement) {
                doc.addOrChange(this, "X", it.red) { value(it) }
                doc.addOrChange(this, "Y", it.green) { value(it) }
                doc.addOrChange(this, "Z", it.blue) { value(it) }
                doc.addOrChange(this, "W", it.opacity) { value(it) }
            }

            doc.addChangeOrDelExt(root, TAG_FLAME_DAMAGE_LENGTH_SCALE, block.FlameDamageLengthScale, tagToElement) { value(it) }
            doc.addChangeOrDelExt(root, TAG_FLAME_LENGTH_SCALE, block.FlameLengthScale, tagToElement) { value(it) }
            doc.addChangeOrDelExt(root, TAG_NEEDS_ATMOSPHERE_FOR_INFLUENCE, block.NeedsAtmosphereForInfluence, tagToElement) { value(it) }
            doc.addChangeOrDelExt(root, TAG_MIN_PLANETARY_INFLUENCE, block.MinPlanetaryInfluence, tagToElement) { value(it) }
            doc.addChangeOrDelExt(root, TAG_MAX_PLANETARY_INFLUENCE, block.MaxPlanetaryInfluence, tagToElement) { value(it) }
            doc.addChangeOrDelExt(root, TAG_EFFECTIVENESS_AT_MIN_INFLUENCE, block.EffectivenessAtMinInfluence, tagToElement) { value(it) }
            doc.addChangeOrDelExt(root, TAG_EFFECTIVENESS_AT_MAX_INFLUENCE, block.EffectivenessAtMaxInfluence, tagToElement) { value(it) }
            doc.addChangeOrDelExt(root, TAG_SLOWDOWN_FACTOR, block.SlowdownFactor, tagToElement) { value(it) }
            doc.addChangeOrDelExt(root, TAG_FLAME_POINT_MATERIAL, block.FlamePointMaterial, tagToElement) { value(it) }
            doc.addChangeOrDelExt(root, TAG_FLAME_LENGTH_MATERIAL, block.FlameLengthMaterial, tagToElement) { value(it) }
            doc.addChangeOrDelExt(root, TAG_FLAME_FLARE, block.FlameFlare, tagToElement) { value(it) }
            doc.addChangeOrDelExt(root, TAG_FLAME_VISIBILITY_DISTANCE, block.FlameVisibilityDistance, tagToElement) { value(it) }
            doc.addChangeOrDelExt(root, TAG_FLAME_GLARE_QUERY_SIZE, block.FlameGlareQuerySize, tagToElement) { value(it) }
            doc.addChangeOrDelExt(root, TAG_FORCE_MAGNITUDE, block.ForceMagnitude, tagToElement) { value(it) }
            doc.addChangeOrDelExt(root, TAG_GENERAL_DAMAGE_MULTIPLIER, block.GeneralDamageMultiplier, tagToElement) { value(it) }

            doc.addChangeOrDelExt(root, TAG_POWER_NEEDED_FOR_JUMP, block.PowerNeededForJump, tagToElement) { value(it) }
            doc.addChangeOrDelExt(root, TAG_MAX_JUMP_DISTANCE, block.MaxJumpDistance, tagToElement) { value(it) }
            doc.addChangeOrDelExt(root, TAG_MAX_JUMP_MASS, block.MaxJumpMass, tagToElement) { value(it) }
            doc.addChangeOrDelExt(root, TAG_REFINE_SPEED, block.RefineSpeed, tagToElement) { value(it) }
            doc.addChangeOrDelExt(root, TAG_MATERIAL_EFFICIENCY, block.MaterialEfficiency, tagToElement) { value(it) }
            doc.addChangeOrDelExt(root, TAG_SENSOR_RADIUS, block.SensorRadius, tagToElement) { value(it) }
            doc.addChangeOrDelExt(root, TAG_SENSOR_OFFSET, block.SensorOffset, tagToElement) { value(it) }
            doc.addChangeOrDelExt(root, TAG_CUT_OUT_RADIUS, block.CutOutRadius, tagToElement) { value(it) }
            doc.addChangeOrDelExt(root, TAG_CUT_OUT_OFFSET, block.CutOutOffset, tagToElement) { value(it) }
            doc.addChangeOrDelExt(root, TAG_EMISSIVE_COLOR_PRESET, block.EmissiveColorPreset, tagToElement) { value(it) }
            doc.addChangeOrDelExt(root, TAG_ORE_AMOUNT_PER_PULL_REQUEST, block.OreAmountPerPullRequest, tagToElement) { value(it) }
            doc.addChangeOrDelExt(root, TAG_CAPACITY, block.Capacity, tagToElement) { value(it) }
            doc.addChangeOrDelExt(root, TAG_MAXIMUM_RANGE, block.MaximumRange, tagToElement) { value(it) }

            doc.addChangeOrDelExt(root, TAG_WEAPON_DEFINITION_ID, block.WeaponDefinitionId, tagToElement) { attrR("Subtype", it) }
            doc.addChangeOrDelExt(root, TAG_MIN_ELEVATION_DEGREES, block.MinElevationDegrees, tagToElement) { value(it) }
            doc.addChangeOrDelExt(root, TAG_MAX_ELEVATION_DEGREES, block.MaxElevationDegrees, tagToElement) { value(it) }
            doc.addChangeOrDelExt(root, TAG_MIN_AZIMUTH_DEGREES, block.MinAzimuthDegrees, tagToElement) { value(it) }
            doc.addChangeOrDelExt(root, TAG_MAX_AZIMUTH_DEGREES, block.MaxAzimuthDegrees, tagToElement) { value(it) }
            doc.addChangeOrDelExt(root, TAG_ROTATION_SPEED, block.RotationSpeed, tagToElement) { value(it) }
            doc.addChangeOrDelExt(root, TAG_ELEVATION_SPEED, block.ElevationSpeed, tagToElement) { value(it) }
            doc.addChangeOrDelExt(root, TAG_IDLE_ROTATION, block.IdleRotation, tagToElement) { value(it) }
            doc.addChangeOrDelExt(root, TAG_MAX_RANGE_METERS, block.MaxRangeMeters, tagToElement) { value(it) }
            doc.addChangeOrDelExt(root, TAG_MIN_FOV, block.MinFov, tagToElement) { value(it) }
            doc.addChangeOrDelExt(root, TAG_MAX_FOV, block.MaxFov, tagToElement) { value(it) }

            doc.addChangeOrDelExt(root, TAG_MAX_FORCE_MAGNITUDE, block.MaxForceMagnitude, tagToElement) { value(it) }
            doc.addChangeOrDelExt(root, TAG_DANGEROUS_TORQUE_THRESHOLD, block.DangerousTorqueThreshold, tagToElement) { value(it) }
            doc.addChangeOrDelExt(root, TAG_ROTOR_PART, block.RotorPart, tagToElement) { value(it) }
            doc.addChangeOrDelExt(root, TAG_PROPULSION_FORCE, block.PropulsionForce, tagToElement) { value(it) }
            doc.addChangeOrDelExt(root, TAG_MIN_HEIGHT, block.MinHeight, tagToElement) { value(it) }
            doc.addChangeOrDelExt(root, TAG_MAX_HEIGHT, block.MaxHeight, tagToElement) { value(it) }
            doc.addChangeOrDelExt(root, TAG_SAFETY_DETACH, block.SafetyDetach, tagToElement) { value(it) }
            doc.addChangeOrDelExt(root, TAG_SAFETY_DETACHMAX, block.SafetyDetachMax, tagToElement) { value(it) }
            doc.addChangeOrDelExt(root, TAG_AXLE_FRICTION, block.AxleFriction, tagToElement) { value(it) }

            doc.addChangeOrDelExt(root, TAG_FUEL_PRODUCTION_TO_CAPACITY_MULTIPLIER, block.FuelProductionToCapacityMultiplier, tagToElement) { value(it) }
            doc.addChangeOrDelExt(root, TAG_FUEL_CAPACITY, block.FuelCapacity, tagToElement) { value(it) }

            doc.addChangeOrDelExt(root, TAG_STEERING_SPEED, block.SteeringSpeed, tagToElement) { value(it) }
            doc.addChangeOrDelExt(root, TAG_AIR_SHOCK_MIN_SPEED, block.AirShockMinSpeed, tagToElement) { value(it) }
            doc.addChangeOrDelExt(root, TAG_AIR_SHOCK_MAX_SPEED, block.AirShockMaxSpeed, tagToElement) { value(it) }

            doc.addChangeOrDelExt(root, TAG_RADIUS_MULTIPLIER, block.RadiusMultiplier, tagToElement) { value(it) }
            doc.addChangeOrDelExt(root, TAG_MINIMUM_ATMOSPHERE_LEVEL, block.MinimumAtmosphereLevel, tagToElement) { value(it) }
            doc.addChangeOrDelExt(root, TAG_REEF_ATMOSPHERE_LEVEL, block.ReefAtmosphereLevel, tagToElement) { value(it) }
            doc.addChangeOrDelExt(root, TAG_DRAG_COEFFICIENT, block.DragCoefficient, tagToElement) { value(it) }
            doc.addChangeOrDelExt(root, TAG_MATERIAL_SUBTYPE, block.MaterialSubtype, tagToElement) { value(it) }
            doc.addChangeOrDelExt(root, TAG_MATERIAL_DEPLOY_COST, block.MaterialDeployCost, tagToElement) { value(it) }

            doc.addChangeOrDelExt(root, TAG_AIR_SHOCK_ACTIVATION_DELAY, block.AirShockActivationDelay, tagToElement) { value(it) }

            doc.addChangeOrDelExt(root, TAG_ICE_CONSUMPTION_PER_SECOND, block.IceConsumptionPerSecond, tagToElement) { value(it) }
            block.Raw.takeUnless { it.isNullOrBlank() }?.also {
                parseXML("<pseudo_root>${it.trimEnd()}</pseudo_root>").firstChild.eachChildren {
                    if(it is Element){
                        doc.importNode(it, true).apply {
                            tagToElement[it.tagName]?.also { root.replaceChild(this, it) } ?: appendTo(root)
                            tagToElement.remove(it.tagName)
                        }
                    }
                }
            }
            if (tagToElement.isNotEmpty()){
                tagToElement.values.forEach {
                    root.removeChild(it)
                }
            }
            return root
        }
        
        private inline fun <T> Element.changeSubValuesList(tagName: String, newList: List<T>, change: Element.(item: T) -> Unit, add: Element.(item: T) -> Unit){
            val old = children(tagName).toList()
            for (i in 0 until min(old.size, newList.size)){
                old[i].change(newList[i])
            }
            if(old.size < newList.size) {
                addChildren {
                    newList.subList(old.size, newList.size).map { item ->
                        element(tagName).apply { add(item) }
                    }
                }
            } else if(newList.size < old.size) {
                for (i in newList.size until old.size) {
                    removeChild(old[i])
                }
            }
        }
    }
}