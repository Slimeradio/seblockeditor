package treedrag

import javafx.scene.control.TreeCell
import javafx.scene.control.TreeItem
import javafx.scene.control.TreeView
import javafx.scene.control.cell.TextFieldTreeCell
import javafx.scene.input.*
import javafx.util.Callback
import org.w3c.dom.Element
import sample.MainController
import sample.xml.ElementItem

class DraggableFactory (val main : MainController, val XmlTree: TreeView<Element>) : Callback<TreeView<Element>, TreeCell<Element>> {
    private var dropZone: TreeCell<Element>? = null
    private var draggedItem: TreeItem<Element>? = null

    override fun call(treeView: TreeView<Element>): TreeCell<Element> {
        var cell = TextFieldTreeCell<Element>().apply {
            converter = MainController.elementConverter
            treeItemProperty().addListener { _, oldValue, newValue ->
                if((oldValue as? ElementItem)?.type != (newValue as? ElementItem)?.type){
                    contextMenu = when((newValue as? ElementItem)?.type ?: -1){
                        MainController.TYPE_DEFINITION -> main.createDefinitionMenu(this)
                        MainController.TYPE_DEFINITION_PARENT -> main.createDefinitionParentMenu(this)
                        else -> null
                    }
                }
            }
        }


        cell.setOnDragDetected { event: MouseEvent -> dragDetected(event, cell, treeView) }
        cell.setOnDragOver { event: DragEvent -> dragOver(event, cell, treeView) }
        cell.setOnDragDropped { event: DragEvent -> drop(event, cell, treeView) }
        cell.setOnDragDone { event: DragEvent -> clearDropLocation() }

        return cell
    }

    private fun dragDetected(event: MouseEvent, treeCell: TreeCell<Element>, treeView: TreeView<Element>) {
        draggedItem = treeCell.treeItem

        // root can't be dragged
        if (draggedItem!!.parent == null) return
        val db = treeCell.startDragAndDrop(TransferMode.MOVE)

        val content = ClipboardContent()
        content[JAVA_FORMAT] = draggedItem!!.value
        db.setContent(content)
        db.dragView = treeCell.snapshot(null, null)
        event.consume()
    }

    private fun dragOver(event: DragEvent, treeCell: TreeCell<Element>, treeView: TreeView<Element>) {
        if (!event.dragboard.hasContent(JAVA_FORMAT)) return
        val thisItem = treeCell.treeItem

        // can't drop on itself
        if (draggedItem == null || thisItem == null || thisItem === draggedItem) return
        // ignore if this is the root
        if (draggedItem!!.parent == null) {
            clearDropLocation()
            return
        }

        event.acceptTransferModes(TransferMode.MOVE)
        if (dropZone != treeCell) {
            clearDropLocation()
            this.dropZone = treeCell
            dropZone!!.style = DROP_HINT_STYLE
        }
    }

    private fun drop(event: DragEvent, treeCell: TreeCell<Element>, treeView: TreeView<Element>) {
        val db = event.dragboard
        val success = false
        if (!db.hasContent(JAVA_FORMAT)) return

        val thisItem = treeCell.treeItem
        val droppedItemParent = draggedItem!!.parent

        // remove from previous location
        droppedItemParent.children.remove(draggedItem)

        // dropping on parent node makes it the first child
        if (droppedItemParent == thisItem) {
            thisItem.children.add(0, draggedItem)
            treeView.selectionModel.select(draggedItem)
        } else {
            // add to new location
            val indexInParent = thisItem.parent.children.indexOf(thisItem)
            thisItem.parent.children.add(indexInParent + 1, draggedItem)
        }
        treeView.selectionModel.select(draggedItem)
        event.isDropCompleted = success
    }

    private fun clearDropLocation() {
        if (dropZone != null) dropZone!!.style = ""
        XmlTree.selectionModel.clearSelection()
    }

    companion object {
        private val JAVA_FORMAT = DataFormat("application/x-java-serialized-object")
        private val DROP_HINT_STYLE = "-fx-border-color: #eea82f; -fx-border-width: 0 0 2 0; -fx-padding: 3 3 1 3"
    }
}
