package sample.main

import helpers.*
import javafx.fxml.FXMLLoader
import javafx.scene.Parent
import javafx.scene.Scene
import javafx.scene.control.TreeCell
import javafx.scene.input.Clipboard
import javafx.scene.input.DataFormat
import javafx.scene.paint.Color
import javafx.stage.Stage
import org.w3c.dom.Document
import org.w3c.dom.Element
import org.w3c.dom.Node
import sample.*
import sample.ABlockInputController
import sample.data.Bal
import sample.data.Block
import sample.data.BlockId
import sample.data.ComponentItem
import sample.xml.ElementItem
import java.io.ByteArrayOutputStream
import java.util.HashSet


inline fun TreeCell<Element>.getElementItem() = treeItem as ElementItem


private fun MainController.getMenuTargetElements(element: ElementItem): List<ElementItem> = XmlTree.selectionModel.selectedItems.let {
    if(it.contains(element)){
        it as List<ElementItem>
    } else {
        listOf(element)
    }
}

fun MainController.actionPaste (cell: TreeCell<Element>) {
    val element = cell.getElementItem()
    val string = Clipboard.getSystemClipboard().string.removePrefix("<?xml version=\"1.0\"?>")
    parseXML("<?xml version=\"1.0\"?>\n<doc>$string</doc>").apply {
        (firstChild as Element).children("Definition").forEach {
            val newElement = document.copy(it).appendTo(element.value)
            element.children.add(ElementItem.createElementItem(newElement))
            blocks.put (newElement, Block.fromXML(it))
        }
    }
}

fun MainController.actionNewBlock (cell: TreeCell<Element>) {
    val element = cell.getElementItem()
    val newElement = element.value.let {
        document.element("Definition").appendTo(it)
    }
    val newElementItem = ElementItem.createElementItem(newElement)
    element.children.add(newElementItem)
    XmlTree.selectionModel.select(newElementItem)
}



fun MainController.actionImportFromString(imp: String){
    parseXML(imp).run {
        document = this
        blocks.clear()
        XmlTree.root = document.firstChildren{
            it is Element
        }?.let { ElementItem.createElementItem(it as Element) }
        XmlTree.selectionModel.select((XmlTree.root as ElementItem).findItemRecursive { it.type == MainController.TYPE_DEFINITION })
    }
}

fun MainController.actionImportFromBlocksFile(imp: ConsoleMain.State.BlocksFile){
    document = imp.document
    imp.saveBlocksElementsToTags()
    blocks.clear()
    for((element, block, _) in imp.blocks){
        if(block != null){
            blocks[element] = block
        }
    }
    XmlTree.root = document.firstChildren{
        it is Element
    }?.let { ElementItem.createElementItem(it as Element) }
    XmlTree.selectionModel.select((XmlTree.root as ElementItem).findItemRecursive { it.type == MainController.TYPE_DEFINITION })
}

fun MainController.actionExportToString(selectedOnly: Boolean) {
    val data = document.run {
        applyBlocksChanges()

        if (selectedOnly) {
            val definition = document.childNodes.item(0).firstChildren { it.nodeName == "CubeBlocks" }!!
            val blockIds = HashSet<String>();
            for (x in XmlTree.selectionModel.selectedItems) {
                if (x.value is Element && x.value.nodeName == "Definition") {
                    blockIds.add(Block.fromXML(x.value).BlockId.toString())
                }
            }
            val extracted = ArrayList<Node>()
            val nodes = definition.childNodes
            while (nodes.length > 0) {
                val nd = nodes.item(0)
                nd.parentNode.removeChild(nd)

                if (nd is Element && nd.nodeName == "Definition" && blockIds.contains(Block.fromXML(nd).BlockId.toString())) {
                    nd.setAttribute("id", Block.fromXML(nd).BlockId.toString())
                    extracted.add(nd)
                }
            }

            for (x in extracted) {
                definition.appendChild(x)
            }
        } else {
            val definition = document.childNodes.item(0).firstChildren { it.nodeName == "CubeBlocks" }!!
            val nodes = definition.childNodes;
            for (x in 0..nodes.length) {
                val nd = nodes.item(x);
                if (nd is Element && nd.nodeName == "Definition") {
                    nd.setAttribute("id", Block.fromXML(nd).BlockId.toString())
                }
            }
        }

        applySortIds()

        XmlTree.selectionModel.clearSelection()

        write2()
    }

    Clipboard.getSystemClipboard().setContent(mapOf(DataFormat.PLAIN_TEXT to data))
}

fun MainController.applyChangesSimple(){
    applyBlocksChanges()
    applySortIds()
    XmlTree.selectionModel.clearSelection()
}

inline fun MainController.applyBlocksChanges(){
    blocks.forEach { e, b -> e.edit(b) }
    blocks.clear()
}

fun MainController.applySortIds(){
    if (sortIds.isNotEmpty()) {

        val definition = document.childNodes.item(0).firstChildren { it.nodeName == "CubeBlocks" }!!

        val nodes = definition.childNodes
        val extracted = ArrayList<Node>()
        while (nodes.length > 0) {
            val nd = nodes.item(0)
            nd.parentNode.removeChild(nd)
            extracted.add(nd)
        }

        val copy = ArrayList(sortIds)

        val foundedAll = ArrayList<Node>()
        while (copy.size > 0) {
            val id = copy[0]
            var found: Node? = null
            var foundAt: Int? = null
            for ((i, x) in extracted.withIndex()) {
                if (x is Element && x.nodeName == "Definition" && Block.fromXML(x).BlockId.toString() == id) {
                    found = x
                    foundAt = i
                    foundedAll.add(found)
                    break
                }
            }

            if (found != null) {
                extracted.removeAt(foundAt!!)
                System.out.println("Sorting:found")
            }

            copy.removeAt(0)
        }


        for (x in extracted) {
            if (x is Element && x.nodeName == "Definition") {
                definition.removeChild(x)
                foundedAll.add(x)
                System.out.println("Sorting:found:removing last")
            }
        }

        System.out.println("Sorting:finished finding")

        for (x in foundedAll) {
            definition.appendChild(x)
            System.out.println("Sorting:appending")
        }
    }
}

fun MainController.actionUpdateItemsComponents () {
    val loader = FXMLLoader(javaClass.getResource("UpdateItemsComponentsDialog.fxml"))
    val root = loader.load<Parent>()
    val stage = Stage()
    loader.getController<UpdateItemsComponentsDialog>().listener = { blockTypes ->
        applyBals (blockTypes)
        stage.hide()
    }
    stage.title = "SE Block Editor - change blocks components"
    stage.scene = Scene(root, 950.0, 600.0)
    stage.showAndWait()
}



fun MainController.actionRearraneIds () {
    sortIds = Clipboard.getSystemClipboard().string.reader().buffered().readText().split("\r\n")
}

fun MainController.actionUpdateItemsComponents2 () {
    val loader = FXMLLoader(javaClass.getResource("UpdateItemsComponentsDialog2.fxml"))
    val root = loader.load<Parent>()
    val stage = Stage()
    loader.getController<UpdateItemsComponentsDialog2>().listener = { blockTypes ->
        applyBals (blockTypes)
        stage.hide()
    }
    stage.title = "SE Block Editor - change blocks components"
    stage.scene = Scene(root, 950.0, 600.0)
    stage.showAndWait()
}

fun MainController.actionUpdateItemsComponents3 () {
    val loader = FXMLLoader(javaClass.getResource("UpdateItemsComponentsDialog3.fxml"))
    val root = loader.load<Parent>()
    val stage = Stage()
    loader.getController<UpdateItemsComponentsDialog3>().listener = { blockTypes ->
        applyBals (blockTypes)
        stage.hide()
    }
    stage.title = "SE Block Editor - change blocks components"
    stage.scene = Scene(root, 950.0, 600.0)
    stage.showAndWait()
}




val boxes : Map<String, Pair<String, Int>> = mapOf(
    Pair("SteelPlate", Pair("SteelPlateBox", 100)),
    Pair("SmallTube", Pair("SmallTubeBox", 100)),
    Pair("LargeTube", Pair("LargeTubeBox", 100)),
    Pair("Motor", Pair("MotorBox", 100)),
    Pair("MetalGrid", Pair("MetalGridBox", 100)),
    Pair("PowerCell", Pair("PowerCellBox", 100))

    )

val alwaysDeconstruct = setOf("AdminComponent", "PowerCell")

fun MainController.applyBals (blockTypes: List<Bal>) {
    val targetBlocksMap = blockTypes.map { it.blockId.value to it }.toMap()

    for (x in targetBlocksMap) {
        System.out.println("Edit : " + x.key)
    }

    var allDefinitions = (document.firstChildrenByType<Element> { it.tagName == "Definitions" } ?: document)?.firstChildrenByType<Element> { it.tagName == "CubeBlocks" };
    allDefinitions?.eachChildren {
        val e = it as? Element ?: return@eachChildren
        if (e.tagName == "Definition") {
            e.apply {
                val block = blocks[it] ?: Block.fromXML(it).apply { blocks[it] = this }
                val blockId = block.BlockId?.toString() ?: ""

                if (blockId.isBlank()) {
                    System.out.println("Skipping Id in blank!")
                    return@apply;
                }

                if (!targetBlocksMap.containsKey(blockId)) {
                    System.out.println("Skipping not needed block! " + blockId)
                    return@apply;
                }
                addCraft (block, blockId, targetBlocksMap)
            }
        }
    }

    editorController.blockExternalChanged()
}

fun addCraft (block : Block, blockId : String, targetBlocksMap : Map<String, Bal>) {
    val bal = targetBlocksMap.getValue(blockId)
    val sortedOrdersFunc = bal.companents.mapNotNull {
        when{
            it.value.fullCount.value <= 0 -> null
            it.value.additional.value >= it.value.fullCount.value -> null
            else -> it
        }
    }.sortedBy { it.value.priority.value }
    val sortedOrdersExtra = bal.companents.mapNotNull {
        when{
            it.value.fullCount.value <= 0 -> null
            it.value.additional.value <= 0 -> null
            else -> it
        }
    }.sortedBy { it.value.additionalPriority.value }
    val newSchema = ArrayList<Pair<String, Int>>(sortedOrdersFunc.size + sortedOrdersExtra.size)
    sortedOrdersFunc.forEach {
        var c = it.value.let { it.fullCount.value - it.additional.value }

        if (c >= 65000) {
            if (boxes.containsKey(it.key)) {
                val boxer = boxes[it.key];
                val boxes = c / boxer!!.second

                if (c % boxer.second != 0) newSchema += it.key to (c % boxer.second)
                newSchema += boxer.first to boxes

                return@forEach
            } else {
                while (c >= 65000) {
                    newSchema += it.key to 65000
                    c -= 65000
                }
            }
        }

        if (c > 0) {
            newSchema += it.key to c
        }
    }
    val newCriticalIdx = newSchema.size - 1
    sortedOrdersExtra.forEach {
        var c = it.value.additional.value

        if (c >= 65000) {
            if (boxes.containsKey(it.key)) {
                val boxer = boxes[it.key];
                val boxes = c / boxer!!.second



                if (c % boxer.second != 0) newSchema += it.key to (c % boxer.second)
                newSchema += boxer.first to boxes

                return@forEach
            } else {
                while (c >= 65000) {
                    newSchema += it.key to 65000
                    c -= 65000
                }
            }
        }

        if (c > 0) {
            newSchema += it.key to c
        }
    }
    (block.Component ?: ArrayList<ComponentItem>().apply { block.Component = this }).apply {
        clear()
        addAll(newSchema.map {
            if (!alwaysDeconstruct.contains(it.first)) {
                ComponentItem(it.second, it.first)
            } else {
                ComponentItem(it.second, it.first, BlockId("Ore", "Scrap"))
            }
        })
    }
    block.CriticalComponent = newCriticalIdx.takeIf { it >= 0 }?.let { MPair(newSchema[it].first, 0) }
}

fun String.toAnyFloatOrNull () : Float? {
    try {
        return toFloat()
    } catch (e:Exception ) {
        return this.replace(",", ".").toFloatOrNull()
    }
}

fun String.toAnyDoubleOrNull () : Double? {
    try {
        return toDouble()
    } catch (e:Exception ) {
        return this.replace(",", ".").toDoubleOrNull()
    }
}

fun Element.editNoMove(block:Block, document: Document){
    Block.toXMLnoMove(block, this, document)
}

fun MainController.actionUpdateItems () {
    val loader = FXMLLoader(javaClass.getResource("UpdateItemsDialog.fxml"))
    val root = loader.load<Parent>()
    val stage = Stage()
    loader.getController<UpdateItemsDialog>().listener = { blockTypes ->
        var needUpdateEditor = false

        val targetBlocksMap = blockTypes.map { it.blockId.value to it }.toMap()
        val editors = editorController.inputControllers//.values.map { it.name to it }.toMap()

        document.firstChildrenByType<Element> { it.tagName == "Definitions" }
            ?.firstChildrenByType<Element> { it.tagName == "CubeBlocks" }
            ?.eachChildren {
                (it as? Element)?.takeIf { it.tagName == "Definition" }?.also {
                    val block = blocks[it] ?: Block.fromXML(it).apply { blocks[it] = this }
                    val blockId = block.BlockId?.toString() ?: ""
                    if(blockId.isNotBlank() && targetBlocksMap.containsKey(blockId)){
                        if(editorController.getBlock() == block){
                            needUpdateEditor = true
                        }
                        targetBlocksMap.getValue(blockId).companents.entries.forEach {
                            val newValue = it.value.value
                            if(newValue != null) {
                                val editor = editors[it.key.toLowerCase()]
                                if (editor != null) {
                                    when (editor.getInputType()) {
                                        Boolean::class.java -> {
                                            (editor as ABlockInputController<*, Boolean>).applyExtBlockChange(block, (newValue == "true" || newValue == "1" || newValue == "ИСТИНА"))
                                        }
                                        String::class.java -> {
                                            (editor as ABlockInputController<*, String>).applyExtBlockChange(block, newValue.takeIf { it != MainController.STR_HARD_NULL } ?: "")
                                        }
                                        Color::class.java -> {
                                            (editor as ABlockInputController<*, Color>).applyExtBlockChange(block, Color.valueOf(newValue.takeIf { it != MainController.STR_HARD_NULL } ?: EditorController.emptyColor.toString()))
                                        }
                                        else -> {
                                            println("undefined type ${editor.getInputType()} with ${editor.name}")
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        if(needUpdateEditor){
            editorController.blockExternalChanged()
        }
        stage.hide()
    }
    stage.title = "SE Block Editor - change blocks components"
    stage.scene = Scene(root, 950.0, 600.0)
    stage.showAndWait()
}



fun MainController.actionGetItemValues () {
    val output = ByteArrayOutputStream()
    CSVWriter(output.bufferedWriter(), arrayOf("key", "input type"), '\t').use {
        it.startTable(0, null, arrayOf())
        editorController.inputControllers.values.forEach { controller ->
            it.putRow(0, mapOf("key" to controller.name, "input type" to controller.getInputType().simpleName))
        }
        it.flush()
        Clipboard.getSystemClipboard().setContent(mapOf(DataFormat.PLAIN_TEXT to output.toString(Charsets.UTF_8.name())))
    }
}

fun MainController.actionDelete (cell: TreeCell<Element>) {
    getMenuTargetElements(cell.getElementItem()).filter {
        it.type == MainController.TYPE_DEFINITION
    }.forEach { element ->
        element.parent.children.remove(element)
        element.value.let {
            it.parentNode.removeChild(it)
        }
    }
}

fun MainController.actionCopy (cell: TreeCell<Element>) {
    getMenuTargetElements(cell.getElementItem()).filter {
        it.type == MainController.TYPE_DEFINITION
    }.map { element ->
        val e = element.value
        var el = e
        blocks[e]?.let {
            Block.toXMLnoMove(it, document.element("Definition"), document).apply {
                e.replace(this, false)
                el = this
                blocks[el] = it
                element.value = el
            }
        }
        el
    }.joinToString(separator = "", prefix = "<?xml version=\"1.0\"?>\r\n<CubeBlocks>\r\n", postfix = "</CubeBlocks>") {
        it.write2().replace(Block.REGEX_DOCTYPE, "")
    }.let {
        Clipboard.getSystemClipboard().setContent(mapOf(DataFormat.PLAIN_TEXT to it))
    }
}

fun MainController.actionExport (cell: TreeCell<Element>) {
    var list = getMenuTargetElements(cell.getElementItem()).filter {
        it.type == MainController.TYPE_DEFINITION
    }

    val loader = FXMLLoader(javaClass.getResource("ExportSelectedValuesDialog.fxml"))
    val root = loader.load<Parent>()
    val stage = Stage()
    loader.getController<ExportSelectedValuesDialog>().apply {
        initColumns(editorController.inputControllers.values.toList())
        listener = { columns ->
            val blocks = list.map {
                blocks[it.value] ?: Block.fromXML(it.value).apply { blocks[it.value] = this }
            }
            val outputStream = ByteArrayOutputStream()
            CSVWriter(outputStream.bufferedWriter(), columns.map { it.first }.toTypedArray(), '\t').use {
                it.startTable(0, null, arrayOf())
                for ((i, block) in blocks.withIndex()) {
                    val map = columns.map {
                        it.first to (it.second(block) ?: MainController.STR_HARD_NULL)
                    }.toMap()
                    it.putRow(i, map)
                }
                it.flush()
                Clipboard.getSystemClipboard().setContent(mapOf(DataFormat.PLAIN_TEXT to outputStream.toString()))
            }
            stage.hide()
        }
    }
    stage.title = "SE Block Editor - change blocks components"
    stage.scene = Scene(root, 356.0, 320.0)
    stage.showAndWait()
}

fun MainController.actionExportComponents2 (cell: TreeCell<Element>) {

    val list = getMenuTargetElements(cell.getElementItem()).filter {
        it.type == MainController.TYPE_DEFINITION
    }
    val blocks = list.map {
        blocks[it.value] ?: Block.fromXML(it.value).apply { blocks[it.value] = this }
    }

    val allComponets = HashMap<String, Int>()

    for (x in blocks) {
        for (y in x.Component!!) {
            if (!allComponets.containsKey(y.component!!)) {
                allComponets[y.component!!] = 1
            } else {
                val z = allComponets[y.component!!]!!
                allComponets[y.component!!] = z + 1
            }
        }
    }

    val headers = ArrayList<String>()
    val subheaders1 = HashMap<String, Any>()
    val subheaders2 = HashMap<String, Any>()

    headers.add("blockId")

    val outputStream = ByteArrayOutputStream()
    CSVWriter(outputStream.bufferedWriter(),headers.toTypedArray(), '\t').use {
        it.startTable(0, null, arrayOf())

        it.putRow(0, subheaders1)
        it.putRow(1, subheaders2)

        for ((i, block) in blocks.withIndex()) {

            val value = HashMap<String, Any?>()

            value.put("blockId", block.BlockId!!.toString())

            val ci = block.criticalIndex;


            for ((j, x) in block.Component!!.withIndex()) {
                val id = x.component!!

                value.put(id+"_total", (value[id+"_total"] as Int? ?: 0) + x.amount)
                if (j <= ci) {
                    //value.put(id+"_prior", value[id+"_prior"] ?: j)
                } else {
                    value.put(id+"_extra", (value[id+"_extra"] as Int? ?: 0) + x.amount)
                }



            }
            it.putRow(i+2, value)
        }
        it.flush()
        Clipboard.getSystemClipboard().setContent(mapOf(DataFormat.PLAIN_TEXT to outputStream.toString()))
    }
}

fun MainController.actionExportComponents (cell: TreeCell<Element>) {

    val list = getMenuTargetElements(cell.getElementItem()).filter {
        it.type == MainController.TYPE_DEFINITION
    }
    val blocks = list.map {
        blocks[it.value] ?: Block.fromXML(it.value).apply { blocks[it.value] = this }
    }

    val allComponets = HashMap<String, Int>()

    for (x in blocks) {
        for (y in x.Component!!) {
            if (!allComponets.containsKey(y.component!!)) {
                allComponets[y.component!!] = 1
            } else {
                val z = allComponets[y.component!!]!!
                allComponets[y.component!!] = z + 1
            }
        }
    }

    val headers = ArrayList<String>()
    val subheaders1 = HashMap<String, Any>()
    val subheaders2 = HashMap<String, Any>()

    headers.add("blockId")

    val outputStream = ByteArrayOutputStream()
    CSVWriter(outputStream.bufferedWriter(),headers.toTypedArray(), '\t').use {
        it.startTable(0, null, arrayOf())

        it.putRow(0, subheaders1)
        it.putRow(1, subheaders2)

        for ((i, block) in blocks.withIndex()) {

            val value = HashMap<String, Any?>()

            value.put("blockId", block.BlockId!!.toString())

            val ci = block.criticalIndex;


            for ((j, x) in block.Component!!.withIndex()) {
                val id = x.component!!

                value.put(id+"_total", (value[id+"_total"] as Int? ?: 0) + x.amount)
                if (j <= ci) {
                    value.put(id+"_prior", value[id+"_prior"] ?: j)
                } else {
                    value.put(id+"_extra", (value[id+"_extra"] as Int? ?: 0) + x.amount)
                    value.put(id+"_extra_prior", value[id+"_extra_prior"] ?: j)
                }



            }
            it.putRow(i+2, value)
        }
        it.flush()
        Clipboard.getSystemClipboard().setContent(mapOf(DataFormat.PLAIN_TEXT to outputStream.toString()))
    }
}