package sample

import helpers.CSVReader
import javafx.fxml.FXML
import javafx.fxml.Initializable
import javafx.scene.control.MenuItem
import javafx.scene.control.TableColumn
import javafx.scene.input.Clipboard
import sample.data.Bal
import sample.data.BalItem
import java.net.URL
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap


class UpdateItemsComponentsDialog3: Initializable {
    @FXML
    lateinit var ImportPrioritiesClipboard: MenuItem

    @FXML
    lateinit var ImportComponentsClipboard: MenuItem

    lateinit var listener: (blockTypes: List<Bal>) -> Unit

    override fun initialize(location: URL?, resources: ResourceBundle?) {
        ImportPrioritiesClipboard.setOnAction {
            val csvReader = CSVReader(Clipboard.getSystemClipboard().string.reader().buffered(), '\t')
            Game.prior = HashMap()
            csvReader.first().apply {
                this.map {
                    it.entries.forEach { Game.prior!![it.key] = it.value as Long }
                }
            }
        }
        ImportComponentsClipboard.setOnAction {
            val csvReader = CSVReader(Clipboard.getSystemClipboard().string.reader().buffered(), '\t')
            val balls = Game.EditCrafts2 (csvReader);
            listener(balls)
        }
    }
}