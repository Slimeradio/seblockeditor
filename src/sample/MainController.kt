package sample

import helpers.*
import javafx.fxml.FXML
import javafx.fxml.FXMLLoader
import javafx.fxml.Initializable
import javafx.scene.control.*
import javafx.scene.input.Clipboard
import javafx.scene.layout.BorderPane
import javafx.util.StringConverter
import org.w3c.dom.Element
import sample.MenuController.Companion.EXPORT_TO_CLIPBOARD
import sample.MenuController.Companion.EXPORT_TO_CLIPBOARD_SELECTED
import sample.MenuController.Companion.GET_ITEMS_VALUES
import sample.MenuController.Companion.IMPORT_FROM_CLIPBOARD
import sample.MenuController.Companion.REARRANGEIDS
import sample.MenuController.Companion.UPDATE_ITEMS
import sample.MenuController.Companion.UPDATE_ITEMS_COMPONENTS
import sample.MenuController.Companion.UPDATE_ITEMS_COMPONENTS2
import sample.MenuController.Companion.UPDATE_ITEMS_COMPONENTS3
import sample.data.Block
import sample.main.*
import sample.xml.ElementItem
import treedrag.DraggableFactory
import java.net.URL
import java.util.*
import kotlin.collections.HashMap


class MainController: Initializable, MenuController.IListener {

    @FXML
    lateinit var root: BorderPane

    @FXML
    lateinit var search: TextField


    @FXML
    lateinit var XmlTree: TreeView<Element>

    lateinit var menuController: MenuController
    lateinit var editorController: EditorController
    var document = emptyDocument().apply {
        appendChild(
                element("Definitions")
                        .attr("xmlns:xsd", "http://www.w3.org/2001/XMLSchema")
                        .attr("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance")
                        .addChildren { listOf(element("CubeBlocks")) }
        )
    }
    var blocks = HashMap<Element, Block>()


    fun onSearch (s : String) {
        /*val defs = XmlTree.getTreeItem(0).children[0]

        for (x in defs.children) {
            x.value
        }*/
    }

    override fun initialize(location: URL?, resources: ResourceBundle?) {
        val menuLoader = FXMLLoader(javaClass.getResource("menu.fxml"))


        search.textProperty().addListener { _, _, newValue -> //??? (Без этого значение сохраняется только после нажатия "ENTER")
            onSearch(newValue)
        }

        root.top = menuLoader.load()
        menuController = menuLoader.getController()
        menuController.listener = this
        val editorLoader = FXMLLoader(javaClass.getResource("editor.fxml"))
        root.center = editorLoader.load()
        editorController = editorLoader.getController()
        XmlTree.setCellFactory  (DraggableFactory (this, XmlTree))
        root.center.isVisible = false
        XmlTree.selectionModel.selectionMode = SelectionMode.MULTIPLE
        XmlTree.selectionModel.selectedItemProperty().addListener { _, oldValue, newValue ->
            if(oldValue != null){
                if(oldValue !is ElementItem) {
                    throw IllegalStateException("${oldValue::class.java.name} is ot in ${ElementItem::class.java.name}")
                }
                if(oldValue.type == TYPE_DEFINITION){

                }
            }
            if(newValue != null) {
                if (newValue !is ElementItem) {
                    throw IllegalStateException("${newValue::class.java.name} is ot in ${ElementItem::class.java.name}")
                }
                if (newValue.type == TYPE_DEFINITION) {
                    val element = newValue.value
                    try{
                        val block = blocks[element] ?: Block.fromXML(element).apply { blocks[element] = this }
                        editorController.changeBlock(block)
                        root.center.isVisible = true
                    } catch (e: Exception){
                        System.out.println("Exception from parse / move to editor block ${getElementName(element)}")
                        e.printStackTrace()
                        root.center.isVisible = false
                    }
                } else {
                    root.center.isVisible = false
                }
            } else {
                root.center.isVisible = false
            }
        }
        if(ConsoleMain.lastFile != null){
            actionImportFromBlocksFile(ConsoleMain.lastFile!!)
            //ConsoleMain.lastFile = null//???
            Main.stopListeners.add {
                applyChangesSimple()//export to lastFile document
            }
        } else {
            XmlTree.root = document.firstChildren{ it is Element }?.let { ElementItem.createElementItem(it as Element) }
        }
    }

    override fun onAction(item: Int) {
        when(item){
            IMPORT_FROM_CLIPBOARD -> { actionImportFromString(Clipboard.getSystemClipboard().string ?: "<null_document />") }
            EXPORT_TO_CLIPBOARD -> { actionExportToString(false) }
            EXPORT_TO_CLIPBOARD_SELECTED -> { actionExportToString(true) }
            UPDATE_ITEMS_COMPONENTS -> actionUpdateItemsComponents ()
            UPDATE_ITEMS_COMPONENTS2 -> actionUpdateItemsComponents2 ()
            UPDATE_ITEMS_COMPONENTS3 -> actionUpdateItemsComponents3 ()
            REARRANGEIDS -> actionRearraneIds ()
            UPDATE_ITEMS -> actionUpdateItems ()
            GET_ITEMS_VALUES -> actionGetItemValues ()
        }
    }


    var sortIds : List<String> = ArrayList()


    fun Element.replace(new: Element, notifyBlocks: Boolean = true){
        this.parentNode.replaceChild(new, this)
        if(notifyBlocks){
            blocks[this]?.let {blocks[new] = it; blocks.remove(this) }
        }

        if (new.parentNode == null) throw IllegalArgumentException()
        (XmlTree.root as? ElementItem)?.let {
            it.findItemRecursive { it.value == this }?.value = new
        }
    }



    fun Element.edit(block:Block){
        editNoMove(block, document)//to Ext.kt
    }


    fun Element.replaceXML(data: String, notifyBlocks: Boolean = true){
        this.nodeValue = data;
        if(notifyBlocks){
            blocks[this]?.let { blocks.remove(this) }
        }
    }

    fun createDefinitionMenu(cell: TreeCell<Element>) =
        ContextMenu(createMenu ("Delete"){ actionDelete (cell) },
            createMenu ("Copy"){ actionCopy (cell) },
            createMenu ("Export data"){ actionExport (cell) },
            createMenu ("Export components"){ actionExportComponents (cell) },
            createMenu ("Export components2"){ actionExportComponents2 (cell) })

    fun createDefinitionParentMenu(cell: TreeCell<Element>) =
        ContextMenu(
            createMenu ("New block"){ actionNewBlock (cell) },
            createMenu ("Paste"){ actionPaste (cell) })




    companion object {
        const val TYPE_SIMPLE = 0
        const val TYPE_DEFINITION = 1
        const val TYPE_DEFINITION_PARENT = 2

        const val STR_HARD_NULL = "NULL"

        val elementConverter = object : StringConverter<Element>() {
            override fun toString(obj: Element?) = getElementName(obj)
            override fun fromString(string: String?) = null
        }

        fun createMenu (name : String, body : () -> Unit) = MenuItem(name).apply { setOnAction { body() } }
        //MenuItemBuilder.create().text(name).onAction { body() }.build()

        fun getElementName(element: Element?) = element?.let { it.tagName + it.firstChildren { it.nodeName == "Id" }.let {
                (it as? Element)?.let { it.child("SubtypeId")?.value()?.takeIf { it.isNotBlank() } ?: it.child("TypeId")?.value() }?.let { ": $it" } ?: ""
            }
        } ?: "<nanashi />"
    }
}
