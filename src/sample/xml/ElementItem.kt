package sample.xml

import helpers.eachChildren
import javafx.collections.ObservableList
import javafx.scene.control.TreeItem
import org.w3c.dom.Element
import sample.MainController

class ElementItem(value: Element, val type: Int = MainController.TYPE_SIMPLE, var childrenInited: Boolean = false): TreeItem<Element>(value){
    fun findItemRecursive(predicate: (ElementItem) -> Boolean): ElementItem?{
        if(predicate(this)){
            return this
        }
        for (child in children) {
            (child as? ElementItem)?.findItemRecursive(predicate)?.let {
                return it
            }
        }
        return null
    }

    override fun getChildren(): ObservableList<TreeItem<Element>> {
        initChildren()
        return super.getChildren()
    }

    override fun isLeaf(): Boolean {
        initChildren()
        return super.isLeaf()
    }

    private fun initChildren(){
        if(!childrenInited){
            childrenInited = true
            val observable = super.getChildren()
            value.eachChildren {
                (it as? Element)?.let {
                    observable.add(createElementItem(it))
                }
            }
        }
    }


    companion object {
        fun createElementItem(element: Element): ElementItem{
            return when(element.tagName){
                "Definition" -> ElementItem(element, MainController.TYPE_DEFINITION, true)
                "CubeBlocks" -> ElementItem(element, MainController.TYPE_DEFINITION_PARENT)
                else -> ElementItem(element)
            }
        }
    }

}