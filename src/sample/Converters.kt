package sample

import sample.main.toAnyDoubleOrNull
import sample.main.toAnyFloatOrNull
import java.text.ParseException


class IdentityConverter<T>(): IConverter<T, T> {
    override fun convert(value: T): T = value
    override fun reverce(value: T): T = value
}

class StringToStringNullableConverter(val nullValue: String = ""): IConverter<String?, String> {
    override fun convert(value: String?): String = value ?: nullValue
    override fun reverce(value: String): String? = value.takeIf { it != nullValue }
}

class IntToStringConverter(): IConverter<Int, String> {
    override fun convert(value: Int): String = value.toString()
    override fun reverce(value: String): Int = value.toIntOrNull() ?: throw ParseException("cannot convert $value to int", 0)
}

class IntToStringNullableConverter(): IConverter<Int?, String> {
    override fun convert(value: Int?): String = value?.toString() ?: ""
    override fun reverce(value: String): Int? = if(value.isBlank()) null else (value.toIntOrNull() ?: throw ParseException("cannot convert $value to int", 0))
}

class LongToStringConverter(): IConverter<Long, String> {
    override fun convert(value: Long): String = value.toString()
    override fun reverce(value: String): Long = value.toLongOrNull() ?: throw ParseException("cannot convert $value to long", 0)
}

class LongToStringNullableConverter(): IConverter<Long?, String> {
    override fun convert(value: Long?): String = value?.toString() ?: ""
    override fun reverce(value: String): Long? = if(value.isBlank()) null else (value.toLongOrNull() ?: throw ParseException("cannot convert $value to long", 0))
}

class FloatToStringConverter(): IConverter<Float, String> {
    override fun convert(value: Float): String = value.toString()
    override fun reverce(value: String): Float = value.toAnyFloatOrNull() ?: throw ParseException("cannot convert $value to float", 0)
}

class FloatToStringNullableConverter(): IConverter<Float?, String> {
    override fun convert(value: Float?): String = value?.toString() ?: ""
    override fun reverce(value: String): Float? = if(value.isBlank()) null else (value.toAnyFloatOrNull() ?: throw ParseException("cannot convert $value to float", 0))
}

class DoubleToStringNullableConverter(): IConverter<Double?, String> {
    override fun convert(value: Double?): String = value?.toString() ?: ""
    override fun reverce(value: String): Double? = if(value.isBlank()) null else (value.toAnyDoubleOrNull() ?: throw ParseException("cannot convert $value to double", 0))
}