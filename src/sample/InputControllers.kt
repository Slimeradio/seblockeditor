package sample

import javafx.beans.property.ObjectProperty
import javafx.beans.value.ObservableValue
import javafx.scene.control.*
import javafx.scene.paint.Color
import org.fxmisc.richtext.CodeArea
import sample.data.Block
import java.text.ParseException
import kotlin.reflect.KMutableProperty

interface IConverter<T, I>{
    fun convert(value: T): I
    fun reverce(value: I): T
}

abstract class ABlockInputController<T, I>(
    val name: String,
    val block: ObjectProperty<Block>,
    val converter: IConverter<T, I>
){
    private var initialize: Boolean = false

    init {
        block.addListener { _, oldValue, newValue ->
            if(oldValue != newValue){
                fireChange(newValue)
            }
        }
    }

    fun blockExternalChanged(){
        fireChange(block.value)
    }

    //change value with external block
    fun applyExtBlockChange(block: Block, newValue: I){
        try{
            converter.reverce(newValue)
        } catch (e: ParseException){
            println("${e.message} from block ${block.BlockId?.toString()} with field $name")
            return
        }.let {
            setBlockValue(block, it)
        }
    }

    fun getExtBlockValue(block: Block): I? {
        return converter.convert(getBlockValue(block))
    }

    abstract fun getInputType(): Class<I>

    private fun fireChange(newBlock: Block){
        initialize = true
        try {
            change(converter.convert(getBlockValue(newBlock)))
        } catch (e: ParseException){
            print(e.message)
        } finally {
            initialize = false
        }
    }

    protected abstract fun change(newValue: I)

    protected fun onChange(newValue: I){
        if(!initialize){
            try{
                converter.reverce(newValue)
            } catch (e: ParseException){
                println(e.message)
                return
            }.let {
                setBlockValue(block.value, it)
            }
        }
    }

    protected abstract fun getBlockValue(block: Block): T

    protected abstract fun setBlockValue(block: Block, newValue: T)

}

abstract class AListenerInputController<T, I>(
    name: String,
    block: ObjectProperty<Block>,
    converter: IConverter<T, I>
): ABlockInputController<T, I>(name, block, converter){
    private var control: IControlWrapper<I>? = null

    fun bindControl(control: IControlWrapper<I>) = apply {
        this.control?.unbind()
        this.control = control
        control.init { onChange(it) }
        if(block.value != null){
            blockExternalChanged()
        }
    }

    fun unbindControl() = apply {
        control?.unbind()
        control = null
    }

    override fun change(newValue: I) {
        control?.change(newValue)
    }

    interface IControlWrapper<I>{
        fun init(onChangeListener: (newValue: I) -> Unit)
        fun change(newValue: I)
        fun unbind()
    }
}

abstract class EasyStringLInputController<T> (val f : KMutableProperty<T>, block: ObjectProperty<Block>, converter: IConverter<T, String>) : AListenerInputController<T, String>(f.name, block, converter) {
    override fun getInputType(): Class<String> = String::class.java
}

class NStringLInputController (f : KMutableProperty<String?>, block: ObjectProperty<Block>) : EasyStringLInputController<String?>(f, block, StringToStringNullableConverter()) {
    override fun getBlockValue(block: Block): String? = f.getter.call(block)
    override fun setBlockValue(block: Block, newValue: String?) { f.setter.call (block, newValue) }
}

class BoolLInputController (val f : KMutableProperty<Boolean>, block: ObjectProperty<Block>) : AListenerInputController<Boolean, Boolean>(f.name, block, IdentityConverter()){
    override fun getInputType(): Class<Boolean> = Boolean::class.java

    override fun getBlockValue(block: Block): Boolean = f.getter.call(block)
    override fun setBlockValue(block: Block, newValue: Boolean) { f.setter.call (block, newValue) }
}

class NBoolLInputController (val f : KMutableProperty<Boolean?>, block: ObjectProperty<Block>, val def : Boolean) : AListenerInputController<Boolean, Boolean>(f.name, block, IdentityConverter()){
    override fun getInputType(): Class<Boolean> = Boolean::class.java

    override fun getBlockValue(block: Block): Boolean = f.getter.call(block) ?: def
    override fun setBlockValue(block: Block, newValue: Boolean) { f.setter.call (block, newValue) }
}

class IntLInputController (f : KMutableProperty<Int>, block: ObjectProperty<Block>) :  EasyStringLInputController<Int>(f, block, IntToStringConverter()) {
    override fun getBlockValue(block: Block): Int = f.getter.call(block)
    override fun setBlockValue(block: Block, newValue: Int) { f.setter.call (block, newValue) }
}

class NIntLInputController (f : KMutableProperty<Int?>, block: ObjectProperty<Block>) :  EasyStringLInputController<Int?>(f, block, IntToStringNullableConverter()) {
    override fun getBlockValue(block: Block): Int? = f.getter.call(block)
    override fun setBlockValue(block: Block, newValue: Int?) { f.setter.call (block, newValue) }
}

class LongLInputController (f : KMutableProperty<Long>, block: ObjectProperty<Block>) :  EasyStringLInputController<Long>(f, block, LongToStringConverter()) {
    override fun getBlockValue(block: Block): Long = f.getter.call(block)
    override fun setBlockValue(block: Block, newValue: Long) { f.setter.call (block, newValue) }
}

class NLongLInputController (f : KMutableProperty<Long?>, block: ObjectProperty<Block>) :  EasyStringLInputController<Long?>(f, block, LongToStringNullableConverter()) {
    override fun getBlockValue(block: Block): Long? = f.getter.call(block)
    override fun setBlockValue(block: Block, newValue: Long?) { f.setter.call (block, newValue) }
}

class FloatLInputController (f : KMutableProperty<Float>, block: ObjectProperty<Block>) :  EasyStringLInputController<Float>(f, block, FloatToStringConverter()) {
    override fun getBlockValue(block: Block): Float = f.getter.call(block)
    override fun setBlockValue(block: Block, newValue: Float) { f.setter.call (block, newValue) }
}

class NFloatLInputController (f : KMutableProperty<Float?>, block: ObjectProperty<Block>) :  EasyStringLInputController<Float?>(f, block, FloatToStringNullableConverter()) {
    override fun getBlockValue(block: Block): Float? = f.getter.call(block)
    override fun setBlockValue(block: Block, newValue: Float?) { f.setter.call (block, newValue) }
}

class NDoubleLInputController (f : KMutableProperty<Double?>, block: ObjectProperty<Block>) :  EasyStringLInputController<Double?>(f, block, DoubleToStringNullableConverter()) {
    override fun getBlockValue(block: Block): Double? = f.getter.call(block)
    override fun setBlockValue(block: Block, newValue: Double?) { f.setter.call (block, newValue) }
}

class FieldControlWrapper(val field: TextField): AListenerInputController.IControlWrapper<String>{
    private var changeListener: ((ObservableValue<out String>, String, String) -> Unit)? = null
    override fun init(onChangeListener: (newValue: String) -> Unit) {
        changeListener =
            { _, _, newValue -> //??? (Без этого значение сохраняется только после нажатия "ENTER")
                onChangeListener(newValue)
            }
        field.setOnAction { onChangeListener(field.text) }
        field.textProperty().addListener(changeListener)
    }

    override fun change(newValue: String) {
        field.text = newValue
    }

    override fun unbind() {
        field.textProperty().removeListener(changeListener)
        field.onAction = null
        changeListener = null
    }
}

inline fun TextField.controlWrapper() = FieldControlWrapper(this)

class CheckboxControlWrapper(val checkbox: CheckBox): AListenerInputController.IControlWrapper<Boolean>{
    override fun init(onChangeListener: (newValue: Boolean) -> Unit) {
        checkbox.setOnAction { onChangeListener(checkbox.isSelected) }
    }

    override fun change(newValue: Boolean) {
        checkbox.isSelected = newValue
    }

    override fun unbind() {
        checkbox.onAction = null
    }
}

inline fun CheckBox.controlWrapper() = CheckboxControlWrapper(this)

class ColorpickerControlWrapper(val colorPicker: ColorPicker): AListenerInputController.IControlWrapper<Color>{
    override fun init(onChangeListener: (newValue: Color) -> Unit) {
        colorPicker.setOnAction { onChangeListener(colorPicker.value) }
    }

    override fun change(newValue: Color) {
        colorPicker.value = newValue
    }

    override fun unbind() {
        colorPicker.onAction = null
    }
}

inline fun ColorPicker.controlWrapper() = ColorpickerControlWrapper(this)

class TextareaControlWrapper(val field: TextArea): AListenerInputController.IControlWrapper<String>{
    private var changeListener: ((ObservableValue<out String>, String, String) -> Unit)? = null
    override fun init(onChangeListener: (newValue: String) -> Unit) {
        changeListener =
            { _, _, newValue -> //??? (Без этого значение сохраняется только после нажатия "ENTER")
                onChangeListener(newValue)
            }
        field.textProperty().addListener(changeListener)
    }

    override fun change(newValue: String) {
        field.text = newValue
    }

    override fun unbind() {
        field.textProperty().removeListener(changeListener)
        changeListener = null
    }
}

inline fun TextArea.controlWrapper() = TextareaControlWrapper(this)

class CodeareaControlWrapper(val field: CodeArea): AListenerInputController.IControlWrapper<String>{
    private var changeListener: ((ObservableValue<out String>, String, String) -> Unit)? = null
    override fun init(onChangeListener: (newValue: String) -> Unit) {
        changeListener =
            { _, _, newValue -> //??? (Без этого значение сохраняется только после нажатия "ENTER")
                onChangeListener(newValue)
            }
        field.textProperty().addListener(changeListener)
    }

    override fun change(newValue: String) {
        field.replaceText(0, field.length, newValue)
    }

    override fun unbind() {
        field.textProperty().removeListener(changeListener)
        changeListener = null
    }
}

inline fun CodeArea.controlWrapper() = CodeareaControlWrapper(this)