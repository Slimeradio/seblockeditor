package sample

import helpers.CSVReader
import javafx.beans.property.SimpleStringProperty
import sample.data.Bal
import sample.data.BalItem
import java.lang.Exception
import kotlin.collections.Map.Entry

object Game {
    var prior : HashMap<String, Long>? = null

    fun EditCrafts (csvReader : CSVReader) : ArrayList<Bal> {
        val balls = ArrayList<Bal>();
        csvReader.first().apply {
            this.map {

                if (prior == null) return@apply

                val bal = Bal()
                bal.blockId.set(it["blockId"] as String)

                var tier = 1;
                if (it["tier"] != null) {
                    tier = (it["tier"] as String).toFloat().toInt();
                }


                it.entries.filter { it.key != "blockId" && it.key != "tier" }.forEach {
                    val extra: Boolean
                    var compId: String
                    var count = (it.value as Long?)?.toInt() ?: return@forEach
                    var bali : BalItem;
                    if (it.key.endsWith("_total") || it.key.endsWith("_extra")) {
                        if (it.key.endsWith("_total")) {
                            compId = it.key.substringBefore("_total")
                            extra = false
                        } else {
                            compId = it.key.substringBefore("_extra")
                            extra = true
                        }
                        bali = bal.companents.getOrPut(compId) { BalItem() }

                        if (extra) {
                            bali.additional.set(count)
                            val vv = prior!![compId + "_extra_prior"] ?: throw IllegalArgumentException (compId + "_prior Not Set");
                            bali.additionalPriority.set(vv.toInt())
                        } else {
                            bali.fullCount.set(count)
                            val vv = prior!![compId + "_prior"] ?: throw IllegalArgumentException (compId + "_prior Not Set");
                            bali.priority.set(vv.toInt())
                        }

                    } else {

                        compId = it.key;
                        var priorId = compId;
                        if (compId == "Upgrader") {
                            compId += "T" + (if (tier<10) "0" else "") + tier
                        }

                        bali = bal.companents.getOrPut(compId) { BalItem() }

                        bali.fullCount.set(count)
                        val vv = prior!![priorId + "_prior"] ?: throw IllegalArgumentException (priorId + "_prior Not Set");
                        bali.priority.set(vv.toInt())

                        if (compId == "SteelPlate") {
                            bali.additional.set((count*0.8f).toInt());
                            val vv = prior!![compId + "_extra_prior"] ?: throw IllegalArgumentException (priorId + "_prior Not Set");
                            bali.additionalPriority.set(vv.toInt())
                        }
                    }
                }

                balls.add(bal)
            }
        }

        return balls;
    }

    fun EditCrafts3 (csvReader : CSVReader, csvReader2 : CSVReader) : Pair<ArrayList<Bal>,Map<String, UpdateItemsDialog.Bal>> {

        var table1 = csvReader.first();
        var properties = table1.getHeader();

        /**
         * tier	MaxPowerConsumption	ForceMagnitude
         * 1	1	2.5
         * 2	0.8333333333	29.4
         * 3	0.6944444444	35.28
         */
        val tierUpgrades = HashMap<Int, Map<String, Double>>()
        for (row in table1) {
            val data = row.filter { it.key != "tier" }.mapValues { it.value.toString().toDouble() }
            val tier = (row["tier"] ?: row["Tier"]).toString().toInt();
            tierUpgrades [tier] = data
        }

        /**
        blockId	Mass	MaxPowerConsumption	ForceMagnitude	SteelPlate	InteriorPlate	Construction	Computer
        LargeBlockLargeAtmosphericThrust	6030.00	0.000006666666667	9.8	1380	0	450	0
        LargeBlockSmallAtmosphericThrust	402.00	0.000006666666667	9.8	92	0	30	0
        SmallBlockLargeAtmosphericThrust	80676.00	0.000006666666667	9.8	360	0	180	90
        SmallBlockLargeAtmosphericThrustSciFi	80676.00	0.000006666666667	9.8	360	0	180	90
        LargeBlockLargeAtmosphericThrustSciFi	6030.00	0.000006666666667	9.8	1380	0	450	0
        LargeBlockSmallAtmosphericThrustSciFi	402.00	0.000006666666667	9.8	92	0	30	0
         */

        val balls = ArrayList<Bal>();
        val editproperties = HashMap<String, UpdateItemsDialog.Bal>()

        val table2 = csvReader2.first();
        for (row in table2) {
            val mass = (row["Mass"] ?: row["mass"]).toString().toDouble()
            val blockId = row["blockId"].toString()
            val tiers = (row["tiers"] ?: row["Tiers"])?.toString()?.toInt() ?: 12
            val props = row.filter { properties.contains(it.key) && it.key != "Tiers" && it.key != "tiers" }
            val craft = row.filter { !properties.contains(it.key) && it.key != "Tiers" && it.key != "tiers" && it.key != "Mass" && it.key != "mass" && it.key != "blockId" && it.key != "size" && it.key != "Size" }

            for (tier in 1..tiers) {
                val tieredBlockId = blockId + Tier(tier)
                for (prop in properties) {

                    if (!tierUpgrades[tier]!!.containsKey(prop)) {
                        println ("Error: unknown property:$prop");
                        continue;
                    }

                    var v = tierUpgrades[tier]!![prop]!!
                    if (props.containsKey(prop)) {
                        v *= mass * props[prop].toString().toDouble();
                    }

                    val property = editproperties.getOrPut(tieredBlockId) { UpdateItemsDialog.Bal() }
                    val vv =  String.format("%.8f", v).replace("E", "E-");

                    property.companents[prop] = SimpleStringProperty (vv);
                }

                val bal = Bal()
                bal.blockId.set(tieredBlockId)

                craft.forEach {
                    val count = (it.value as Long?)?.toInt() ?: return@forEach
                    val bali : BalItem;
                    var compId: String = it.key;
                    val priorId = compId;
                    if (compId == "Upgrader") compId += Tier (tier, false);


                    bali = bal.companents.getOrPut(compId) { BalItem() }


                    bali.fullCount.set(count)
                    val vv = prior!![priorId + "_prior"] ?: throw IllegalArgumentException (priorId + "_prior Not Set");
                    bali.priority.set(vv.toInt())

                    if (compId == "SteelPlate") {
                        bali.additional.set((count*0.8f).toInt());
                        val vv = prior!![compId + "_extra_prior"] ?: throw IllegalArgumentException (priorId + "_prior Not Set");
                        bali.additionalPriority.set(vv.toInt())
                    }
                }

                balls.add (bal)
            }
        }

        return Pair(balls, editproperties);
    }

    fun Tier (tier : Int, skipT1: Boolean = true) : String {
        if (tier == 1 && skipT1) return "";
        return "T" + (if (tier<10) "0" else "") + tier;
    }

    fun EditCrafts2 (csvReader : CSVReader) : ArrayList<Bal> {
        val balls = ArrayList<Bal>();
        csvReader.first().apply {
            this.map {

                if (prior == null) return@apply

                val bal = Bal()
                bal.blockId.set(it["blockId"] as String)
                val tier = (it["lvl"] as Long?)?.toInt() ?: throw Exception ("Wrong lvl");

                it.entries.filter { it.key != "blockId" && it.key != "lvl"}.forEach {
                    val count = (it.value as Long?)?.toInt() ?: return@forEach

                    var compId: String = it.key;
                    var fcompId: String = compId;
                    if (tier != 1) {
                        fcompId += Tier(tier);
                    }

                    val bali = bal.companents.getOrPut(fcompId) { BalItem() }

                    bali.fullCount.set(count)
                    val vv = prior!![compId + "_prior"] ?: throw IllegalArgumentException (compId + "_prior Not Set");
                    bali.priority.set(vv.toInt())

                    if (compId == "SteelPlate") {
                        bali.additional.set((count*0.8f).toInt());
                        val vv = prior!![compId + "_extra_prior"] ?: throw IllegalArgumentException (compId + "_prior Not Set");
                        bali.additionalPriority.set(vv.toInt())
                    }
                }

                balls.add(bal)
            }
        }

        return balls;
    }
}