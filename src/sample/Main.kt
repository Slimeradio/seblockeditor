package sample

import javafx.application.Application
import javafx.fxml.FXMLLoader
import javafx.scene.Parent
import javafx.scene.Scene
import javafx.stage.Stage

class Main: Application() {

    override fun start(primaryStage: Stage) {
        val root = FXMLLoader.load<Parent>(javaClass.getResource("main.fxml"))
        primaryStage.title = "SE Block Editor"
        primaryStage.scene = Scene(root, 950.0, 600.0)
        primaryStage.show()
    }

    override fun stop() {
        super.stop()
        for(onStop in stopListeners){
            onStop()
        }
    }

    companion object{
        val stopListeners = ArrayList<() -> Unit>()
    }
}