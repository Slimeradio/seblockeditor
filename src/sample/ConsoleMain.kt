package sample

import helpers.children
import helpers.firstChildrenByType
import helpers.parseXML
import org.w3c.dom.Document
import org.w3c.dom.Element
import sample.console.*
import sample.data.Block
import sample.main.editNoMove
import java.io.File
import java.io.FileInputStream
import java.io.Reader

object ConsoleMain {

    val commands = HashMap<String, ICommands>().also { commands ->
        GlobalCommands.getCommands().forEach { commands[it] = GlobalCommands }
        FilesCommands.getCommands().forEach { commands[it] = FilesCommands }
        EditPropertiesCommands.getCommands().forEach { commands[it] = EditPropertiesCommands }
        GoogleLoaderCommands.getCommands().forEach { commands[it] = GoogleLoaderCommands }
    }

    var lastFile: State.BlocksFile? = null

    /**
     * run application in console mode
     * @param args - commands & arguments by format [command1, arg1, ..., argN, command2, arg1, ..., argM, ...]
     * @return true if run in console mode or false if need run in UI mode
     * */
    fun run(vararg args: String): Boolean {
        var argss : Array<out String> = args
        if (args.isNotEmpty() && args[0] == "commandlist") {
            argss = parseCommandList(args[1])
        }
        if(argss.isNotEmpty()){
            val state = State(arrayListOf(*argss))
            while (true){
                val c = state.getArg() ?: break
                val target = commands[c]
                if(target == null) {
                    System.err.println("$c is not a command")
                    continue
                }

                target.run(c, state)
            }
            return if(state.runGui) {
                state.saveFileToGlobal()
                false
            } else {
                true
            }
        }
        return false
    }

    private fun parseCommandList(fileName: String): Array<String>{
        val file = File(fileName).takeIf { it.exists() } ?: throw IllegalArgumentException("file \"$fileName\" not found")
        return file.bufferedReader().use { parseCommandList(it) }
    }

    private fun parseCommandList(reader: Reader): Array<String>{
        val STATE_DEFAULT = 0
        val STATE_SIMPLE = 1
        val STATE_QUOTES = 2
        var state = STATE_DEFAULT
        val ret = ArrayList<String>()
        val buf = StringBuilder()
        var cterm = ' '
        while (true){
            val c = reader.read().takeIf { it != -1 }?.toChar() ?: break
            when (state) {
                STATE_DEFAULT -> {
                    if(!c.isWhitespace()){
                        when (c) {
                            '\'', '"' -> {
                                cterm = c
                                state = STATE_QUOTES
                            }
                            else -> {
                                state = STATE_SIMPLE
                                buf.append(c)
                            }
                        }
                    }
                }
                STATE_SIMPLE -> {
                    if(c.isWhitespace()){
                        state = STATE_DEFAULT
                        ret.add(buf.toString())
                        buf.setLength(0)
                    } else {
                        buf.append(c)
                    }
                }
                STATE_QUOTES -> {
                    if(c == '\\'){
                        val c1i = reader.read()
                        if(c1i == -1){
                            buf.append('\\')
                        } else {
                            val c1 = c1i.toChar()
                            if(c1 != cterm && c1 != '\\'){
                                buf.append('\\')
                            }
                            buf.append(c1)
                        }
                    } else if(c == cterm){
                        state = STATE_DEFAULT
                        ret.add(buf.toString())
                        buf.setLength(0)
                    } else {
                        buf.append(c)
                    }
                }
            }
        }

        if(state == STATE_SIMPLE) {
            ret.add(buf.toString())
            buf.setLength(0)
        } else if(state == STATE_QUOTES){
            System.err.println("WARNING: unexpected end of quotes '$cterm' str: \"$buf\"")//may be throw exception?
            ret.add(buf.toString())
            buf.setLength(0)
        }
        return ret.toTypedArray()
    }

    class State(val args: ArrayList<String>){
        var curArg: Int = 0

        var file: BlocksFile? = null

        var debugMode: Boolean = false
        var csvDelimiter = ','
        var runGui: Boolean = false
        var guiRunned: Boolean = false

        //region args
        fun getArg() = if(curArg < args.size) {
            curArg++
            args[curArg - 1]
        } else {
            null//may be throw exception?
        }

        fun getArgs(size: Int) = if(curArg + size < args.size) {
            curArg += size
            args.subList(curArg - size, curArg)
        } else {
            val last = curArg
            curArg = args.size
            args.subList(last, curArg) + arrayOfNulls<String>(size - (args.size - last))//null//may be throw exception or return all last arguments?
        }

        fun revertGet(size: Int){
            if(curArg >= size) {
                curArg -= size
            }
        }
        //endregion args

        fun saveFileToGlobal(){
            if(debugMode) println("save file to global (${lastFile?.path}) -> (${file?.path})")
            lastFile = file
        }

        class BlocksFile(val f: File){
            var debugMode: Boolean = false
            val path: String by lazy { f.absolutePath }
            val document: Document by lazy {
                if(debugMode) println("parse xml from $path")
                FileInputStream(f).bufferedReader().use {
                    parseXML(it.readText())
                }
            }
            val blocksParent: Element? by lazy {
                if(debugMode) println("find definition parent")
                document.firstChildrenByType<Element> { it.tagName == "Definitions" }
                    ?.firstChildrenByType<Element> { it.tagName == "CubeBlocks" }
            }
            val blocks = ArrayList<Triple<Element, Block?, Boolean>>()

            fun loadBlocksElementsIfNotExists(){
                if (blocks.isEmpty()){
                    if(debugMode) println("find definitions from document")
                    blocksParent?.children("Definition")?.forEach {
                        if(debugMode) println("definition ${blocks.size} found")
                        blocks.add(Triple(it, null, false))
                    }
                }
            }

            fun saveBlocksElementsToTags(){
                if(debugMode) println("save blocks to xml elements")
                for(i in 0 until blocks.size){
                    val (element, block, changed) = blocks[i]
                    block.takeIf { changed }?.let {
                        if(debugMode) println("save $i definition (BlockId = ${it.BlockId}) (without move mode)")
                        element.editNoMove(it, document)
                        blocks[i] = Triple(element, block, false)
                    }
                }
            }
        }
    }
}

