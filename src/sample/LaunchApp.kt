package sample

import com.sun.javafx.application.LauncherImpl
import sample.Main
import java.io.Console
import kotlin.random.Random

fun main(args: Array<String>) {
    if(!ConsoleMain.run(*args)){
        LauncherImpl.launchApplication(Main::class.java, args)
    }
}