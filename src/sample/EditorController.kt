package sample

import helpers.MPair
import helpers.highlightXML
import javafx.beans.property.ObjectProperty
import javafx.beans.property.SimpleObjectProperty
import javafx.fxml.FXML
import javafx.fxml.Initializable
import javafx.scene.control.*
import javafx.scene.layout.Region
import javafx.scene.paint.Color
import org.fxmisc.richtext.CodeArea
import sample.data.*
import java.lang.IllegalArgumentException

import java.net.URL
import java.util.ResourceBundle
import kotlin.reflect.KMutableProperty1
import kotlin.reflect.full.declaredMemberProperties
import kotlin.reflect.jvm.javaType

public class EditorController() : Initializable {
    val block: ObjectProperty<Block> = SimpleObjectProperty(this, "block")

    val inputControllers = createControllersMap(block)

    fun getView (name : String) : Region {
        return EditorController::class.declaredMemberProperties.first { x -> x.name == "Bl$name" }.get(this) as Region
    }

    //region General
    @FXML lateinit var BlId: TextField
    @FXML lateinit var BlSubtypeId: TextField
    @FXML lateinit var BlDisplayName: TextField
    @FXML lateinit var BlDescription: TextArea
    @FXML lateinit var BlRaw: CodeArea//TextArea
    @FXML lateinit var BlBlockPairName: TextField
    @FXML lateinit var BlEdgeType: TextField
    @FXML lateinit var BlResourceSourceGroup: TextField
    @FXML lateinit var BlResourceSinkGroup: TextField
    @FXML lateinit var BlIsPublic: CheckBox
    @FXML lateinit var BlGuiVisible: CheckBox
    @FXML lateinit var BlSilenceableByShipSoundSystem: CheckBox
    @FXML lateinit var BlThrusterType: TextField
    @FXML lateinit var BlXsiType: TextField
    @FXML lateinit var BlBlockVariants: TextArea

        //endregion
        //region Balance
    @FXML lateinit var BlPCU: TextField
    @FXML lateinit var BlBuildTimeSeconds: TextField
    @FXML lateinit var BlDisassembleRatio: TextField
    @FXML lateinit var BlDeformationRatio: TextField
    @FXML lateinit var BlInventoryMaxVolume: TextField
    @FXML lateinit var BlInventorySizeX: TextField
    @FXML lateinit var BlInventorySizeY: TextField
    @FXML lateinit var BlInventorySizeZ: TextField
    @FXML lateinit var BlComponent: TextArea

    @FXML lateinit var BlFuelCapacity: TextField
    @FXML lateinit var BlFuelProductionToCapacityMultiplier: TextField

        //endregion
        //region Model
    @FXML lateinit var BlIcon: TextField
    @FXML lateinit var BlModel: TextField
    @FXML lateinit var BlBuildProgressModels: TextArea
    @FXML lateinit var BlCubeSize: CheckBox
    @FXML lateinit var BlIsAirTight: CheckBox
    @FXML lateinit var BlSizeX: TextField
    @FXML lateinit var BlSizeY: TextField
    @FXML lateinit var BlSizeZ: TextField
    @FXML lateinit var BlModelOffsetX: TextField
    @FXML lateinit var BlModelOffsetY: TextField
    @FXML lateinit var BlModelOffsetZ: TextField
    @FXML lateinit var BlCenterX: TextField
    @FXML lateinit var BlCenterY: TextField
    @FXML lateinit var BlCenterZ: TextField
    @FXML lateinit var BlMirroringX: TextField
    @FXML lateinit var BlMirroringY: TextField
    @FXML lateinit var BlMirroringZ: TextField
    @FXML lateinit var BlMountPoints: TextArea
    @FXML lateinit var BlBlockTopology: TextField
    @FXML lateinit var BlDamageEffectId: TextField
    @FXML lateinit var BlDamageEffectName: TextField
    @FXML lateinit var BlDamagedSound: TextField
    @FXML lateinit var BlPrimarySound: TextField
    @FXML lateinit var BlActionSound: TextField
    @FXML lateinit var BlDestroyEffect: TextField
    @FXML lateinit var BlDestroySound: TextField
    @FXML lateinit var BlGenerateSound: TextField
    @FXML lateinit var BlIdleSound: TextField
    @FXML lateinit var BlOverlayTexture: TextField
    @FXML lateinit var BlUseModelIntersection: CheckBox
    @FXML lateinit var BlAutorotateMode: TextField
        //endregion
        //region Extra
    @FXML lateinit var BlRequiredPowerInput: TextField
    @FXML lateinit var BlBasePowerInput: TextField
    @FXML lateinit var BlConsumptionPower: TextField
    @FXML lateinit var BlMaxPowerOutput: TextField
    @FXML lateinit var BlStandbyPowerConsumption: TextField
    @FXML lateinit var BlOperationalPowerConsumption: TextField
    @FXML lateinit var BlPowerInput: TextField
    @FXML lateinit var BlPowerConsumptionIdle: TextField
    @FXML lateinit var BlPowerConsumptionMoving: TextField
    @FXML lateinit var BlMaxPowerConsumption: TextField
    @FXML lateinit var BlMinPowerConsumption: TextField
    @FXML lateinit var BlMaxStoredPower: TextField
    @FXML lateinit var BlInitialStoredPowerRatio: TextField

    @FXML lateinit var BlIceConsumptionPerSecond: TextField
        //endregion
        //region Thrusters
    @FXML lateinit var BlFlameFullColorRGBO: ColorPicker
    @FXML lateinit var BlFlameIdleColorRGBO: ColorPicker
    @FXML lateinit var BlFlameDamageLengthScale: TextField
    @FXML lateinit var BlFlameLengthScale: TextField
    @FXML lateinit var BlNeedsAtmosphereForInfluence: CheckBox
    @FXML lateinit var BlMinPlanetaryInfluence: TextField
    @FXML lateinit var BlMaxPlanetaryInfluence: TextField
    @FXML lateinit var BlEffectivenessAtMinInfluence: TextField
    @FXML lateinit var BlEffectivenessAtMaxInfluence: TextField
    @FXML lateinit var BlSlowdownFactor: TextField
    @FXML lateinit var BlFlamePointMaterial: TextField
    @FXML lateinit var BlFlameLengthMaterial: TextField
    @FXML lateinit var BlFlameFlare: TextField
    @FXML lateinit var BlFlameVisibilityDistance: TextField
    @FXML lateinit var BlFlameGlareQuerySize: TextField
    @FXML lateinit var BlForceMagnitude: TextField
        //endregion
        //region Specials
    @FXML lateinit var BlPowerNeededForJump: TextField
    @FXML lateinit var BlMaxJumpDistance: TextField
    @FXML lateinit var BlMaxJumpMass: TextField
    @FXML lateinit var BlRefineSpeed: TextField
    @FXML lateinit var BlMaterialEfficiency: TextField
    @FXML lateinit var BlSensorRadius: TextField
    @FXML lateinit var BlSensorOffset: TextField
    @FXML lateinit var BlCutOutRadius: TextField
    @FXML lateinit var BlCutOutOffset: TextField
    @FXML lateinit var BlEmissiveColorPreset: TextField
    @FXML lateinit var BlOreAmountPerPullRequest: TextField
    @FXML lateinit var BlCapacity: TextField
    @FXML lateinit var BlMaximumRange: TextField
        //endregion
        //region Weap
    @FXML lateinit var BlWeaponDefinitionId: TextField
    @FXML lateinit var BlMinElevationDegrees: TextField
    @FXML lateinit var BlMaxElevationDegrees: TextField
    @FXML lateinit var BlMinAzimuthDegrees: TextField
    @FXML lateinit var BlMaxAzimuthDegrees: TextField
    @FXML lateinit var BlRotationSpeed: TextField
    @FXML lateinit var BlElevationSpeed: TextField
    @FXML lateinit var BlIdleRotation: CheckBox
    @FXML lateinit var BlMaxRangeMeters: TextField
    @FXML lateinit var BlMinFov: TextField
    @FXML lateinit var BlMaxFov: TextField
        //endregion
        //region Wheels
    @FXML lateinit var BlMaxForceMagnitude: TextField
    @FXML lateinit var BlDangerousTorqueThreshold: TextField
    @FXML lateinit var BlRotorPart: TextField
    @FXML lateinit var BlPropulsionForce: TextField
    @FXML lateinit var BlMinHeight: TextField
    @FXML lateinit var BlMaxHeight: TextField
    @FXML lateinit var BlSafetyDetach: TextField
    @FXML lateinit var BlSafetyDetachMax: TextField
    @FXML lateinit var BlAxleFriction: TextField
    @FXML lateinit var BlSteeringSpeed: TextField
    @FXML lateinit var BlAirShockMinSpeed: TextField
    @FXML lateinit var BlAirShockMaxSpeed: TextField
    @FXML lateinit var BlAirShockActivationDelay: TextField
    //endregion

    @FXML lateinit var BlMaterialSubtype: TextField
    @FXML lateinit var BlMaterialDeployCost: TextField
    @FXML lateinit var BlRadiusMultiplier: TextField
    @FXML lateinit var BlMinimumAtmosphereLevel: TextField
    @FXML lateinit var BlReefAtmosphereLevel: TextField
    @FXML lateinit var BlDragCoefficient: TextField
    @FXML lateinit var BlGeneralDamageMultiplier: TextField


    override fun initialize(location: URL?,  resources: ResourceBundle?) {

        BlRaw.highlightXML(Main::class.java)

        //region General accessors
        bindControllers(inputControllers
            , N_BLOCK_ID to BlId
            , N_BLOCK_SUBTYPE_ID to BlSubtypeId)


        bindControllers(inputControllers, Block::DisplayName)

        bindControllers(inputControllers
            , Block::Description
            , Block::Raw)

        bindControllers(inputControllers
            , Block::BlockPairName
            , Block::EdgeType
            , Block::ResourceSourceGroup
            , Block::ResourceSinkGroup)

        bindControllers(inputControllers, N_BLOCK_VARIANTS to BlBlockVariants)

        bindControllers(inputControllers
            , Block::IsPublic
            , Block::GuiVisible
            , Block::SilenceableByShipSoundSystem
            , Block::ThrusterType
            , Block::XsiType
            , Block::PCU
            , Block::BuildTimeSeconds
            , Block::DisassembleRatio
            , Block::DeformationRatio
            , Block::InventoryMaxVolume)

        bindControllers(inputControllers
            , N_BLOCK_INVENTORY_MAX_VOLUME to BlInventoryMaxVolume
            , N_INVENTARY_SIZE_X to BlInventorySizeX
            , N_INVENTARY_SIZE_Y to BlInventorySizeY
            , N_INVENTARY_SIZE_Z to BlInventorySizeZ
            , N_BLOCK_LIST to BlComponent)
        //endregion
        //region Model accessors

        bindControllers(inputControllers
            , N_ICON to BlIcon
            , N_MODEL to BlModel
            , N_BUILD_PROGRESS_MODELS to BlBuildProgressModels
            , N_CUBE_SIZE to BlCubeSize
            , N_IS_AIR_TIGHT to BlIsAirTight)

        bindControllers(inputControllers
            , N_SIZE_X to BlSizeX
            , N_SIZE_X to BlSizeX
            , N_SIZE_X to BlSizeX
            , N_MODEL_OFFSET_X to BlModelOffsetX
            , N_MODEL_OFFSET_Y to BlModelOffsetY
            , N_MODEL_OFFSET_Z to BlModelOffsetZ
            , N_CENTER_X to BlCenterX
            , N_CENTER_Y to BlCenterY
            , N_CENTER_Z to BlCenterZ
            , N_MIRRORING_X to BlMirroringX
            , N_MIRRORING_Y to BlMirroringY
            , N_MIRRORING_Z to BlMirroringZ)

        bindControllers(inputControllers, N_MOUNT_POINTS to BlMountPoints)

        bindControllers(inputControllers
            , Block::BlockTopology
            , Block::DamageEffectId
            , Block::DamageEffectName
            , Block::DamagedSound
            , Block::PrimarySound
            , Block::ActionSound
            , Block::DestroyEffect
            , Block::DestroySound
            , Block::IdleSound
            , Block::OverlayTexture)

        bindControllers(inputControllers
            , N_USE_MODEL_INTERSECTION to BlUseModelIntersection
            , N_AUTOROTATE_MODE to BlAutorotateMode)

        bindControllers(inputControllers
            , Block::RequiredPowerInput
            , Block::BasePowerInput
            , Block::ConsumptionPower
            , Block::MaxPowerOutput
            , Block::StandbyPowerConsumption
            , Block::OperationalPowerConsumption
            , Block::PowerInput
            , Block::PowerConsumptionIdle
            , Block::PowerConsumptionMoving
            , Block::MaxPowerConsumption
            , Block::MinPowerConsumption
            , Block::MaxStoredPower
            , Block::InitialStoredPowerRatio
            , Block::IceConsumptionPerSecond)

        bindControllers(inputControllers
            , N_FLAME_FULL_COLOR to BlFlameFullColorRGBO
            , N_FLAME_IDLE_COLOR to BlFlameIdleColorRGBO)

        bindControllers(inputControllers
            , Block::FlameDamageLengthScale
            , Block::FlameLengthScale)

        bindControllers(inputControllers, N_NEEDS_ATMOSPHERE_FOR_INFLUENCE to BlNeedsAtmosphereForInfluence)


        bindControllers(inputControllers
            , Block::FuelCapacity
            , Block::FuelProductionToCapacityMultiplier
            , Block::MinPlanetaryInfluence
            , Block::MaxPlanetaryInfluence
            , Block::EffectivenessAtMinInfluence
            , Block::EffectivenessAtMaxInfluence
            , Block::SlowdownFactor
            , Block::FlamePointMaterial
            , Block::FlameLengthMaterial
            , Block::FlameFlare
            , Block::FlameVisibilityDistance
            , Block::FlameGlareQuerySize
            , Block::ForceMagnitude
            , Block::PowerNeededForJump
            , Block::MaxJumpDistance
            , Block::MaxJumpMass
            , Block::RefineSpeed
            , Block::MaterialEfficiency
            , Block::SensorRadius
            , Block::SensorOffset
            , Block::CutOutRadius
            , Block::CutOutOffset
            , Block::EmissiveColorPreset
            , Block::OreAmountPerPullRequest
            , Block::Capacity
            , Block::MaximumRange
            , Block::WeaponDefinitionId
            , Block::MinElevationDegrees
            , Block::MaxElevationDegrees
            , Block::MinAzimuthDegrees
            , Block::MaxAzimuthDegrees
            , Block::RotationSpeed
            , Block::ElevationSpeed
            , Block::IdleRotation
            , Block::MaxRangeMeters
            , Block::MinFov
            , Block::MaxFov
            , Block::MaxForceMagnitude
            , Block::DangerousTorqueThreshold
            , Block::RotorPart
            , Block::PropulsionForce
            , Block::MinHeight
            , Block::MaxHeight
            , Block::SteeringSpeed
            , Block::SafetyDetach
            , Block::SafetyDetachMax
            , Block::AxleFriction
            , Block::SteeringSpeed
            , Block::AirShockMinSpeed
            , Block::AirShockMaxSpeed
            , Block::AirShockActivationDelay)

        bindControllers(inputControllers
            , Block::MaterialSubtype
            , Block::GeneralDamageMultiplier
            , Block::MaterialDeployCost
            , Block::RadiusMultiplier
            , Block::MinimumAtmosphereLevel
            , Block::ReefAtmosphereLevel
            , Block::DragCoefficient)

        //endregion
    }

    fun bindControllers(controllers: HashMap<String, AListenerInputController<*, *>>, vararg p: Pair<String, Region>){
        for ((name, control) in p){
            val controller = controllers[name.toLowerCase()] ?: throw IllegalArgumentException("controllers has not contain \"$name\"")
            when (control) {
                is TextField -> (controller as AListenerInputController<*, String>).bindControl(control.controlWrapper())
                is TextArea -> (controller as AListenerInputController<*, String>).bindControl(control.controlWrapper())
                is CodeArea -> (controller as AListenerInputController<*, String>).bindControl(control.controlWrapper())
                is CheckBox -> (controller as AListenerInputController<*, Boolean>).bindControl(control.controlWrapper())
                is ColorPicker -> (controller as AListenerInputController<*, Color>).bindControl(control.controlWrapper())
                else -> throw IllegalArgumentException (" Dont match: ${control.javaClass}")
            }
        }
    }

    fun bindControllers(controllers: HashMap<String, AListenerInputController<*, *>>, vararg p: KMutableProperty1<Block, *>){
        for (x in p){
            val name = x.name
            val controller = controllers[name.toLowerCase()] ?: throw IllegalArgumentException("controllers has not contain \"$name\"")
            when (val control = getView(name)) {
                is TextField -> (controller as AListenerInputController<*, String>).bindControl(control.controlWrapper())
                is TextArea -> (controller as AListenerInputController<*, String>).bindControl(control.controlWrapper())
                is CodeArea -> (controller as AListenerInputController<*, String>).bindControl(control.controlWrapper())
                is CheckBox -> (controller as AListenerInputController<*, Boolean>).bindControl(control.controlWrapper())
                is ColorPicker -> (controller as AListenerInputController<*, Color>).bindControl(control.controlWrapper())
                else -> throw IllegalArgumentException (" Dont match: ${control.javaClass}")
            }
        }
    }

    fun changeBlock(block: Block){
        this.block.value = block
    }

    fun getBlock() = this.block.value

    fun blockExternalChanged(){
        for (controller in inputControllers.values) {
            controller.blockExternalChanged()
        }
    }

    abstract class LinesRegexConverter<I: Any>(val reg: Regex): IConverter<MutableList<I>, String> {
        override fun convert(value: MutableList<I>): String = value.joinToString(separator = "\n") { toLine(it) }

        override fun reverce(value: String): MutableList<I> {
            return value.lineSequence().mapNotNull {
                reg.find(it)?.groups?.let {
                    fromGroups(it)
                }
            }.toMutableList()
        }
        protected abstract fun toLine(item: I): String
        protected abstract fun fromGroups(groups: MatchGroupCollection): I?
    }

    companion object {
        val emptyColor = Color.rgb(0, 0, 0, 0.0)

        fun createControllersMap(block: ObjectProperty<Block>): HashMap<String, AListenerInputController<*, *>>{
            var centerX: String = ""
            var centerY: String = ""
            var centerZ: String = ""
            val map = HashMap<String, AListenerInputController<*, *>>()
            //region General accessors
            map.add(object : AListenerInputController<String, String>(N_BLOCK_ID, block, IdentityConverter()){
                override fun getInputType(): Class<String> = String::class.java

                override fun getBlockValue(block: Block): String = block.BlockId?.blockId ?: ""

                override fun setBlockValue(block: Block, newValue: String) {
                    (block.BlockId ?: BlockId(null, null).apply { block.BlockId = this }).blockId = newValue
                }
            }).add(object : AListenerInputController<String, String>(N_BLOCK_SUBTYPE_ID, block, IdentityConverter()){
                override fun getInputType(): Class<String> = String::class.java

                override fun getBlockValue(block: Block): String = block.BlockId?.blockSubtypeId ?: ""

                override fun setBlockValue(block: Block, newValue: String) {
                    (block.BlockId ?: BlockId(null, null).apply { block.BlockId = this }).blockSubtypeId = newValue
                }
            }).addByField(block
                , Block::DisplayName
                , Block::Description
                , Block::Raw
                , Block::BlockPairName
                , Block::EdgeType
                , Block::ResourceSourceGroup
                , Block::ResourceSinkGroup
            ).add(object : AListenerInputController<MutableList<BlockId>, String>(
                N_BLOCK_VARIANTS,
                block,
                object : LinesRegexConverter<BlockId>(Regex("([a-zA-Z0-9_]*)\\/([a-zA-Z0-9_ ]*)")){
                    override fun toLine(item: BlockId): String = "${item.blockId}/${item.blockSubtypeId}"

                    override fun fromGroups(groups: MatchGroupCollection): BlockId? = BlockId(groups[1]?.value, groups[2]?.value)

                }){
                override fun getInputType(): Class<String> = String::class.java

                override fun getBlockValue(block: Block): MutableList<BlockId> = block.BlockVariants ?: arrayListOf()

                override fun setBlockValue(block: Block, newValue: MutableList<BlockId>) {
                    block.BlockVariants = newValue
                }
            }).addByField(block
                , Block::IsPublic
                , Block::GuiVisible
                , Block::SilenceableByShipSoundSystem
                , Block::ThrusterType
                , Block::XsiType
                , Block::PCU
                , Block::BuildTimeSeconds
                , Block::DisassembleRatio
                , Block::DeformationRatio
                , Block::InventoryMaxVolume
            ).add(object : AListenerInputController<Float, String>(N_BLOCK_INVENTORY_MAX_VOLUME, block, FloatToStringConverter()){
                override fun getInputType(): Class<String> = String::class.java

                override fun getBlockValue(block: Block): Float = block.InventoryMaxVolume

                override fun setBlockValue(block: Block, newValue: Float) {
                    block.InventoryMaxVolume = newValue
                }
            }).add(object : AListenerInputController<Float, String>(N_INVENTARY_SIZE_X, block, FloatToStringConverter()){
                override fun getInputType(): Class<String> = String::class.java

                override fun getBlockValue(block: Block): Float = block.InventorySize?.x ?: 0.0f

                override fun setBlockValue(block: Block, newValue: Float) {
                    (block.InventorySize ?: XYZFloat().apply { block.InventorySize = this }).x = newValue
                }
            }).add(object : AListenerInputController<Float, String>(N_INVENTARY_SIZE_Y, block, FloatToStringConverter()){
                override fun getInputType(): Class<String> = String::class.java

                override fun getBlockValue(block: Block): Float = block.InventorySize?.y ?: 0.0f

                override fun setBlockValue(block: Block, newValue: Float) {
                    (block.InventorySize ?: XYZFloat().apply { block.InventorySize = this }).y = newValue
                }
            }).add(object : AListenerInputController<Float, String>(N_INVENTARY_SIZE_Z, block, FloatToStringConverter()){
                override fun getInputType(): Class<String> = String::class.java

                override fun getBlockValue(block: Block): Float = block.InventorySize?.z ?: 0.0f

                override fun setBlockValue(block: Block, newValue: Float) {
                    (block.InventorySize ?: XYZFloat().apply { block.InventorySize = this }).z = newValue
                }
            })
            //endregion General accessors
            //region Model accessors
            map.add(object : AListenerInputController<EditorControllerBlockList, String>(N_BLOCK_LIST, block, object :
                IConverter<EditorControllerBlockList, String> {
                val regex = Regex("^(\\d+)? ([a-zA-Z0-9_]*)( -> ([a-zA-Z0-9_]*)\\/([a-zA-Z0-9_]*))?$")
                val delim = "---"

                override fun convert(value: EditorControllerBlockList): String {
                    val map = HashMap<String?, Int>()
                    return value.components.joinToString(separator = "\n") {
                        addDelimIfNeeded("${it.amount} ${it.component}${it.deconstructId?.let { " -> ${it.blockId}/${it.blockSubtypeId}" } ?: ""}", it, value.critical, map)
                    }
                }

                override fun reverce(value: String): EditorControllerBlockList {
                    //TODO if delimiter need positioned post component - save last component and make critical from him
                    val map = HashMap<String?, Int>()
                    var critical: MPair<String, Int>? = null
                    var last: ComponentItem? = null
                    return value.lineSequence().mapNotNull {
                        if(it == delim){
                            critical = MPair(last!!.component!!, map[last!!.component]!!)
                            null
                        } else {
                            regex.find(it)?.groups?.let {
                                ComponentItem(it[1]?.value?.toIntOrNull() ?: 0, it[2]?.value, it[3]?.run {
                                    BlockId(it[4]?.value, it[5]?.value)
                                }).apply {
                                    incrementById(this, map)
                                    last = this
                                }
                            }
                        }
                    }.toMutableList().let {
                        EditorControllerBlockList(it, critical)
                    }
                }

                inline fun addDelimIfNeeded(base: String, iter: ComponentItem, critical: MPair<String, Int>?, countMap: MutableMap<String?, Int>): String{
                    //TODO if delimiter need positioned post component - replace base <-> (critical.let...) and delim + "\n" -> "\n" + delim
                    incrementById(iter, countMap)
                    return base + (critical?.let {
                        if (iter.component == critical.first && countMap[critical.first] == critical.second) {
                            "\n" + delim
                        } else {
                            null
                        }
                    } ?: "")
                }

                inline fun incrementById(iter: ComponentItem, countMap: MutableMap<String?, Int>){
                    val idx = (countMap[iter.component] ?: -1) + 1
                    countMap[iter.component] = idx
                }
            }){
                override fun getInputType(): Class<String> = String::class.java

                override fun getBlockValue(block: Block): EditorControllerBlockList = EditorControllerBlockList(block.Component ?: arrayListOf(), block.CriticalComponent)

                override fun setBlockValue(block: Block, newValue: EditorControllerBlockList) {
                    block.Component = newValue.components
                    block.CriticalComponent = newValue.critical
                }
            }).add(object : AListenerInputController<String, String>(N_ICON, block, IdentityConverter()){
                override fun getInputType(): Class<String> = String::class.java

                override fun getBlockValue(block: Block): String = block.Icon ?: ""

                override fun setBlockValue(block: Block, newValue: String) {
                    block.Icon = newValue
                }
            }).add(object : AListenerInputController<String, String>(N_MODEL, block, IdentityConverter()){
                override fun getInputType(): Class<String> = String::class.java

                override fun getBlockValue(block: Block): String = block.Model ?: ""

                override fun setBlockValue(block: Block, newValue: String) {
                    block.Model = newValue
                }
            }).add(object : AListenerInputController<MutableList<BuildProgressModellItem>, String>(
                N_BUILD_PROGRESS_MODELS,
                block,
                object : LinesRegexConverter<BuildProgressModellItem>(Regex("^(\\d+(\\.\\d+)?)? (.*)\$")){
                    override fun toLine(item: BuildProgressModellItem): String = "${item.progress} ${item.file ?: ""}"

                    override fun fromGroups(groups: MatchGroupCollection): BuildProgressModellItem? = BuildProgressModellItem(groups[1]?.value?.toFloat() ?: 0.0f, groups[3]?.value)

                }){
                override fun getInputType(): Class<String> = String::class.java

                override fun getBlockValue(block: Block): MutableList<BuildProgressModellItem> = block.BuildProgressModels ?: arrayListOf()

                override fun setBlockValue(block: Block, newValue: MutableList<BuildProgressModellItem>) {
                    block.BuildProgressModels = newValue
                }
            }).add(object : AListenerInputController<Boolean, Boolean>(N_CUBE_SIZE, block, IdentityConverter()){
                override fun getInputType(): Class<Boolean> = Boolean::class.java

                override fun getBlockValue(block: Block): Boolean = block.CubeSize.equals("Large", true)

                override fun setBlockValue(block: Block, newValue: Boolean) {
                    block.CubeSize = if(newValue) "Large" else "Small"
                }
            }).add(object : AListenerInputController<Boolean, Boolean>(N_IS_AIR_TIGHT, block, IdentityConverter()){
                override fun getInputType(): Class<Boolean> = Boolean::class.java

                override fun getBlockValue(block: Block): Boolean = block.IsAirTight ?: false

                override fun setBlockValue(block: Block, newValue: Boolean) {
                    block.IsAirTight = newValue
                }
            }).add(object : AListenerInputController<Int, String>(N_SIZE_X, block, IntToStringConverter()){
                override fun getInputType(): Class<String> = String::class.java

                override fun getBlockValue(block: Block): Int = block.Size?.x ?: 0

                override fun setBlockValue(block: Block, newValue: Int) {
                    (block.Size ?: XYZInt().apply { block.Size = this }).x = newValue
                }
            }).add(object : AListenerInputController<Int, String>(N_SIZE_Y, block, IntToStringConverter()){
                override fun getInputType(): Class<String> = String::class.java

                override fun getBlockValue(block: Block): Int = block.Size?.y ?: 0

                override fun setBlockValue(block: Block, newValue: Int) {
                    (block.Size ?: XYZInt().apply { block.Size = this }).y = newValue
                }
            }).add(object : AListenerInputController<Int, String>(N_SIZE_Z, block, IntToStringConverter()){
                override fun getInputType(): Class<String> = String::class.java

                override fun getBlockValue(block: Block): Int = block.Size?.z ?: 0

                override fun setBlockValue(block: Block, newValue: Int) {
                    (block.Size ?: XYZInt().apply { block.Size = this }).z = newValue
                }
            }).add(object : AListenerInputController<Float, String>(N_MODEL_OFFSET_X, block, FloatToStringConverter()){
                override fun getInputType(): Class<String> = String::class.java

                override fun getBlockValue(block: Block): Float = block.ModelOffset?.x ?: 0f

                override fun setBlockValue(block: Block, newValue: Float) {
                    (block.ModelOffset ?: XYZFloat().apply { block.ModelOffset = this }).x = newValue
                }
            }).add(object : AListenerInputController<Float, String>(N_MODEL_OFFSET_Y, block, FloatToStringConverter()){
                override fun getInputType(): Class<String> = String::class.java

                override fun getBlockValue(block: Block): Float = block.ModelOffset?.y ?: 0f

                override fun setBlockValue(block: Block, newValue: Float) {
                    (block.ModelOffset ?: XYZFloat().apply { block.ModelOffset = this }).y = newValue
                }
            }).add(object : AListenerInputController<Float, String>(N_MODEL_OFFSET_Z, block, FloatToStringConverter()){
                override fun getInputType(): Class<String> = String::class.java

                override fun getBlockValue(block: Block): Float = block.ModelOffset?.z ?: 0f

                override fun setBlockValue(block: Block, newValue: Float) {
                    (block.ModelOffset ?: XYZFloat().apply { block.ModelOffset = this }).z = newValue
                }
            }).add(object : AListenerInputController<Int?, String>(N_CENTER_X, block, IntToStringNullableConverter()){
                override fun getInputType(): Class<String> = String::class.java

                override fun change(newValue: String) {
                    centerX = newValue
                    super.change(newValue)
                }

                override fun getBlockValue(block: Block): Int? = block.Center?.x

                override fun setBlockValue(block: Block, newValue: Int?) {
                    centerX = newValue?.toString() ?: ""
                    if(newValue != null || centerY.isNotBlank() || centerZ.isNotBlank()){
                        (block.Center ?: XYZInt().apply { block.Center = this }).x = newValue ?: 0
                    } else {
                        block.Center = null
                    }
                }
            }).add(object : AListenerInputController<Int?, String>(N_CENTER_Y, block, IntToStringNullableConverter()){
                override fun getInputType(): Class<String> = String::class.java

                override fun change(newValue: String) {
                    centerY = newValue
                    super.change(newValue)
                }

                override fun getBlockValue(block: Block): Int? = block.Center?.y

                override fun setBlockValue(block: Block, newValue: Int?) {
                    centerY = newValue?.toString() ?: ""
                    if(newValue != null || centerX.isNotBlank() || centerZ.isNotBlank()){
                        (block.Center ?: XYZInt().apply { block.Center = this }).y = newValue ?: 0
                    } else {
                        block.Center = null
                    }
                }
            }).add(object : AListenerInputController<Int?, String>(N_CENTER_Z, block, IntToStringNullableConverter()){
                override fun getInputType(): Class<String> = String::class.java

                override fun change(newValue: String) {
                    centerZ = newValue
                    super.change(newValue)
                }

                override fun getBlockValue(block: Block): Int? = block.Center?.z

                override fun setBlockValue(block: Block, newValue: Int?) {
                    centerZ = newValue?.toString() ?: ""
                    if(newValue != null || centerX.isNotBlank() || centerY.isNotBlank()){
                        (block.Center ?: XYZInt().apply { block.Center = this }).z = newValue ?: 0
                    } else {
                        block.Center = null
                    }
                }
            }).add(object : AListenerInputController<String, String>(N_MIRRORING_X, block, IdentityConverter()){
                override fun getInputType(): Class<String> = String::class.java
                override fun getBlockValue(block: Block): String = block.Mirroring?.x ?: ""
                override fun setBlockValue(block: Block, newValue: String) { block.Mirroring = (block.Mirroring ?: XYZAxis()).apply { x = newValue } }
            }).add(object : AListenerInputController<String, String>(N_MIRRORING_Y, block, IdentityConverter()){
                override fun getInputType(): Class<String> = String::class.java
                override fun getBlockValue(block: Block): String = block.Mirroring?.y ?: ""
                override fun setBlockValue(block: Block, newValue: String) { block.Mirroring = (block.Mirroring ?: XYZAxis()).apply { y = newValue } }
            }).add(object : AListenerInputController<String, String>(N_MIRRORING_Z, block, IdentityConverter()){
                override fun getInputType(): Class<String> = String::class.java
                override fun getBlockValue(block: Block): String = block.Mirroring?.z ?: ""
                override fun setBlockValue(block: Block, newValue: String) { block.Mirroring = (block.Mirroring ?: XYZAxis()).apply { z = newValue } }
            }).add(object : AListenerInputController<MutableList<MountPointItem>, String>(N_MOUNT_POINTS, block,
                object : LinesRegexConverter<MountPointItem>(Regex("^(Right|Left|Top|Bottom|Front|Back) sx=(\\d+(\\.\\d+)?)? sy=(\\d+(\\.\\d+)?)? ex=(\\d+(\\.\\d+)?)? ey=(\\d+(\\.\\d+)?)?( \\+)?\$")){
                    override fun toLine(item: MountPointItem): String = "${item.side} sx=${item.startX} sy=${item.startY} ex=${item.endX} ey=${item.endY}${if(item.default) " +" else ""}"
                    override fun fromGroups(groups: MatchGroupCollection): MountPointItem? = MountPointItem(groups[1]?.value!!, groups[2]?.value?.toFloat() ?: 0.0f, groups[4]?.value?.toFloat() ?: 0.0f, groups[6]?.value?.toFloat() ?: 0.0f, groups[8]?.value?.toFloat() ?: 0.0f, groups[10]?.value == " +")
                }){
                override fun getInputType(): Class<String> = String::class.java

                override fun getBlockValue(block: Block): MutableList<MountPointItem> = block.MountPoints ?: arrayListOf()

                override fun setBlockValue(block: Block, newValue: MutableList<MountPointItem>) {
                    block.MountPoints = newValue.takeIf { it.isNotEmpty() }
                }
            }).addByField(block
                , Block::BlockTopology
                , Block::DamageEffectId
                , Block::DamageEffectName
                , Block::DamagedSound
                , Block::PrimarySound
                , Block::ActionSound
                , Block::DestroyEffect
                , Block::DestroySound
                , Block::IdleSound
                , Block::OverlayTexture
            ).add(object : AListenerInputController<Boolean, Boolean>(N_USE_MODEL_INTERSECTION, block, IdentityConverter()){
                override fun getInputType(): Class<Boolean> = Boolean::class.java

                override fun getBlockValue(block: Block): Boolean = block.UseModelIntersection

                override fun setBlockValue(block: Block, newValue: Boolean) {
                    block.UseModelIntersection = newValue
                }
            }).add(object : AListenerInputController<String?, String>(N_AUTOROTATE_MODE, block, StringToStringNullableConverter()){
                override fun getInputType(): Class<String> = String::class.java

                override fun getBlockValue(block: Block): String? = block.AutorotateMode

                override fun setBlockValue(block: Block, newValue: String?) {
                    block.AutorotateMode = newValue
                }
            }).addByField(block
                , Block::RequiredPowerInput
                , Block::BasePowerInput
                , Block::ConsumptionPower
                , Block::MaxPowerOutput
                , Block::StandbyPowerConsumption
                , Block::OperationalPowerConsumption
                , Block::PowerInput
                , Block::PowerConsumptionIdle
                , Block::PowerConsumptionMoving
                , Block::MaxPowerConsumption
                , Block::MinPowerConsumption
                , Block::MaxStoredPower
                , Block::InitialStoredPowerRatio
            ).add(object : AListenerInputController<Color, Color>(N_FLAME_FULL_COLOR, block, IdentityConverter()){
                override fun getInputType(): Class<Color> = Color::class.java
                override fun getBlockValue(block: Block): Color = block.FlameFullColor ?: emptyColor
                override fun setBlockValue(block: Block, newValue: Color) { block.FlameFullColor = newValue.takeIf { it != emptyColor } }
            }).add(object : AListenerInputController<Color, Color>(N_FLAME_IDLE_COLOR, block, IdentityConverter()){
                override fun getInputType(): Class<Color> = Color::class.java
                override fun getBlockValue(block: Block): Color = block.FlameIdleColor ?: emptyColor
                override fun setBlockValue(block: Block, newValue: Color) { block.FlameIdleColor = newValue.takeIf { it != emptyColor } }
            }).addByField(block
                , Block::FlameDamageLengthScale
                , Block::FlameLengthScale
            ).add(object : AListenerInputController<Boolean, Boolean>(N_NEEDS_ATMOSPHERE_FOR_INFLUENCE, block, IdentityConverter()){
                override fun getInputType(): Class<Boolean> = Boolean::class.java

                override fun getBlockValue(block: Block): Boolean = block.NeedsAtmosphereForInfluence ?: false

                override fun setBlockValue(block: Block, newValue: Boolean) {
                    block.NeedsAtmosphereForInfluence = newValue
                }
            }).addByField(block
                , Block::FuelCapacity
                , Block::FuelProductionToCapacityMultiplier
                , Block::MinPlanetaryInfluence
                , Block::MaxPlanetaryInfluence
                , Block::EffectivenessAtMinInfluence
                , Block::EffectivenessAtMaxInfluence
                , Block::SlowdownFactor
                , Block::FlamePointMaterial
                , Block::FlameLengthMaterial
                , Block::FlameFlare
                , Block::FlameVisibilityDistance
                , Block::FlameGlareQuerySize
                , Block::ForceMagnitude
                , Block::PowerNeededForJump
                , Block::MaxJumpDistance
                , Block::MaxJumpMass
                , Block::RefineSpeed
                , Block::MaterialEfficiency
                , Block::SensorRadius
                , Block::SensorOffset
                , Block::CutOutRadius
                , Block::CutOutOffset
                , Block::EmissiveColorPreset
                , Block::OreAmountPerPullRequest
                , Block::Capacity
                , Block::MaximumRange
                , Block::WeaponDefinitionId
                , Block::MinElevationDegrees
                , Block::MaxElevationDegrees
                , Block::MinAzimuthDegrees
                , Block::MaxAzimuthDegrees
                , Block::RotationSpeed
                , Block::ElevationSpeed
                , Block::IdleRotation
                , Block::MaxRangeMeters
                , Block::MinFov
                , Block::MaxFov
                , Block::MaxForceMagnitude
                , Block::DangerousTorqueThreshold
                , Block::RotorPart
                , Block::PropulsionForce
                , Block::MinHeight
                , Block::MaxHeight
                , Block::SteeringSpeed
                , Block::SafetyDetach
                , Block::SafetyDetachMax
                , Block::AxleFriction
                , Block::SteeringSpeed
                , Block::AirShockMinSpeed
                , Block::AirShockMaxSpeed
                , Block::AirShockActivationDelay
            ).addByField(block
                , Block::MaterialSubtype
                , Block::GeneralDamageMultiplier
                , Block::MaterialDeployCost
                , Block::RadiusMultiplier
                , Block::MinimumAtmosphereLevel
                , Block::ReefAtmosphereLevel
                , Block::DragCoefficient
            )
            map.addByField(block, Block::IceConsumptionPerSecond)
            //endregion Model accessors
            return map
        }

        inline fun HashMap<String, AListenerInputController<*, *>>.add(c: AListenerInputController<*, *>) = apply {
            this[c.name.toLowerCase()] = c
        }

        fun HashMap<String, AListenerInputController<*, *>>.addByField(block: ObjectProperty<Block>, vararg p : KMutableProperty1<Block, *>) = apply {
            for (x in p) {
                var input = when (x.returnType.javaType) {
                    Int::class.javaObjectType -> NIntLInputController(x as KMutableProperty1<Block, Int?>, block);
                    Float::class.javaObjectType -> NFloatLInputController(
                        x as KMutableProperty1<Block, Float?>,
                        block
                    );
                    Boolean::class.javaObjectType -> NBoolLInputController(
                        x as KMutableProperty1<Block, Boolean?>,
                        block,
                        false
                    );
                    Long::class.javaObjectType -> NLongLInputController(x as KMutableProperty1<Block, Long?>, block);
                    Double::class.javaObjectType -> NDoubleLInputController(
                        x as KMutableProperty1<Block, Double?>,
                        block
                    );

                    Int::class.javaPrimitiveType -> IntLInputController(x as KMutableProperty1<Block, Int>, block);
                    Float::class.javaPrimitiveType -> FloatLInputController(
                        x as KMutableProperty1<Block, Float>,
                        block
                    );
                    Boolean::class.javaPrimitiveType -> BoolLInputController(
                        x as KMutableProperty1<Block, Boolean>,
                        block
                    );
                    Long::class.javaPrimitiveType -> LongLInputController(x as KMutableProperty1<Block, Long>, block);

                    String::class.javaObjectType -> NStringLInputController(
                        x as KMutableProperty1<Block, String?>,
                        block
                    );

                    else -> throw IllegalArgumentException(" Dont match:" + x.returnType.javaClass)
                }

                add(input)
            }
        }

        const val N_BLOCK_ID = "BlockId"
        const val N_BLOCK_SUBTYPE_ID = "BlockSubtypeId"
        const val N_BLOCK_VARIANTS = "BlockVariants"
        const val N_BLOCK_INVENTORY_MAX_VOLUME = "InventoryMaxVolume"//??old value is ""
        const val N_INVENTARY_SIZE_X = "InventorySizeX"
        const val N_INVENTARY_SIZE_Y = "InventorySizeY"
        const val N_INVENTARY_SIZE_Z = "InventorySizeZ"
        const val N_BLOCK_LIST = "BlockList"
        const val N_ICON = "Icon"
        const val N_MODEL = "Model"
        const val N_BUILD_PROGRESS_MODELS = "BuildProgressModels"
        const val N_CUBE_SIZE = "CubeSize"
        const val N_IS_AIR_TIGHT = "IsAirTight"
        const val N_SIZE_X = "SizeX"
        const val N_SIZE_Y = "SizeY"
        const val N_SIZE_Z = "SizeZ"
        const val N_MODEL_OFFSET_X = "ModelOffsetX"
        const val N_MODEL_OFFSET_Y = "ModelOffsetY"
        const val N_MODEL_OFFSET_Z = "ModelOffsetZ"
        const val N_CENTER_X = "CenterX"
        const val N_CENTER_Y = "CenterY"
        const val N_CENTER_Z = "CenterZ"
        const val N_MIRRORING_X = "MirroringX"
        const val N_MIRRORING_Y = "MirroringY"
        const val N_MIRRORING_Z = "MirroringZ"
        const val N_MOUNT_POINTS = "MountPoints"
        const val N_USE_MODEL_INTERSECTION = "UseModelIntersection"
        const val N_AUTOROTATE_MODE = "AutorotateMode"
        const val N_FLAME_FULL_COLOR = "FlameFullColor"
        const val N_FLAME_IDLE_COLOR = "FlameIdleColor"
        const val N_NEEDS_ATMOSPHERE_FOR_INFLUENCE = "NeedsAtmosphereForInfluence"
    }
}