package sample

import javafx.fxml.FXML
import javafx.fxml.Initializable
import javafx.scene.control.MenuItem
import java.net.URL
import java.util.*

class MenuController: Initializable {
    lateinit var listener: IListener

    @FXML
    lateinit var impFromClipboard: MenuItem

    @FXML
    lateinit var expToClipboard: MenuItem
    lateinit var expToClipboardSelected: MenuItem

    @FXML
    lateinit var updateItemsComponents: MenuItem

    @FXML
    lateinit var updateItemsComponents2: MenuItem

    @FXML
    lateinit var updateItemsComponents3: MenuItem

    @FXML
    lateinit var rearrangeIds: MenuItem

    @FXML
    lateinit var updateItems: MenuItem

    @FXML
    lateinit var getItemsValues: MenuItem

    override fun initialize(location: URL?, resources: ResourceBundle?) {
        impFromClipboard.setOnAction { listener.onAction(IMPORT_FROM_CLIPBOARD) }
        expToClipboard.setOnAction { listener.onAction(EXPORT_TO_CLIPBOARD) }
        expToClipboardSelected.setOnAction { listener.onAction(EXPORT_TO_CLIPBOARD_SELECTED) }
        updateItemsComponents.setOnAction { listener.onAction(UPDATE_ITEMS_COMPONENTS) }
        updateItemsComponents2.setOnAction { listener.onAction(UPDATE_ITEMS_COMPONENTS2) }
        updateItemsComponents3.setOnAction { listener.onAction(UPDATE_ITEMS_COMPONENTS3) }
        rearrangeIds.setOnAction { listener.onAction(REARRANGEIDS) }
        updateItems.setOnAction { listener.onAction(UPDATE_ITEMS) }
        getItemsValues.setOnAction { listener.onAction(GET_ITEMS_VALUES) }
    }

    interface IListener{
        fun onAction(item: Int)
    }

    companion object {
        const val IMPORT_FROM_CLIPBOARD = 1
        const val EXPORT_TO_CLIPBOARD = 2
        const val EXPORT_TO_CLIPBOARD_SELECTED = 3
        const val UPDATE_ITEMS_COMPONENTS = 100

        const val UPDATE_ITEMS = 101
        const val GET_ITEMS_VALUES = 102
        const val UPDATE_ITEMS_COMPONENTS2 = 103
        const val UPDATE_ITEMS_COMPONENTS3 = 105
        const val REARRANGEIDS = 104
    }
}
