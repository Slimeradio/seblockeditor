package sample.console

import helpers.CSVReader
import javafx.beans.property.SimpleObjectProperty
import javafx.beans.property.SimpleStringProperty
import javafx.scene.paint.Color
import sample.*
import sample.data.Bal
import sample.data.BalItem
import sample.data.Block
import sample.main.addCraft
import java.io.File
import java.lang.IllegalStateException

object EditPropertiesCommands: ICommands {
    const val EDIT_PROPERTIES = "editProperties"
    const val EDIT_CRAFTS = "editCrafts"
    const val EDIT_CRAFTS2 = "editCrafts2"
    const val EDIT_CRAFTS3 = "editCrafts3"
    const val SET_COMPONENTS_PRIORITIES = "setComponentsPriorities"

    val blockProp = SimpleObjectProperty<Block>()
    val editors: HashMap<String, AListenerInputController<*, *>> by lazy { EditorController.createControllersMap(blockProp) }

    fun getCommands() = arrayOf(EDIT_PROPERTIES, EDIT_CRAFTS, EDIT_CRAFTS2, EDIT_CRAFTS3, SET_COMPONENTS_PRIORITIES)

    override fun run(command: String, state: ConsoleMain.State) {
        when(command) {
            EDIT_PROPERTIES -> editProperties(state)
            SET_COMPONENTS_PRIORITIES -> setComponentsPriorities(state)
            EDIT_CRAFTS -> editCrafts(state)
            EDIT_CRAFTS2 -> editCrafts2(state)
            EDIT_CRAFTS3 -> editCrafts3(state)
            else -> System.err.println("$command is not command in ${this.javaClass.name}")
        }
    }

    private fun editProperties(state: ConsoleMain.State){
        val file = state.file ?: throw IllegalStateException("cannot edit zero file")
        file.loadBlocksElementsIfNotExists()

        val arg = state.getArg()
        if(arg == null) {
            System.err.println("file is required")
            return
        }
        val f = File(arg).takeIf { it.exists() }
        if(f == null) {
            System.err.println("$arg is not file")
            return
        }

        if(state.debugMode) println("start read csv from ${f.absolutePath}")
        val bals = CSVReader(f, state.csvDelimiter).use {
            val csv = it.first()
            val header = csv.getHeader()
            if(state.debugMode) println("csv header is [${header.joinToString()}]")
            csv.mapNotNull {
                if(it[header[0]] != null) {
                    UpdateItemsDialog.Bal().apply {
                        blockId.value = it[header[0]]?.toString()
                        for (i in 1 until header.size) {
                            (companents[header[i]] ?: SimpleStringProperty(this, header[i]).apply { companents[header[i]] = this }).value = it[header[i]]?.toString()
                        }
                        if(state.debugMode) println("found $this")
                    }
                } else {
                    if(state.debugMode) println("id is null from string ${it.map { "${it.key} = ${it.value.toString()}" }.joinToString()}")
                    null
                }
            }
        }
        if(state.debugMode) println("end read csv")
        val targetBlocksMap = bals.map { it.blockId.value to it }.toMap()
        if(state.debugMode) println("start update ${file.blocks.size} blocks")
        editProperties (file, targetBlocksMap, state.debugMode);
    }

    private fun editProperties (file : ConsoleMain.State.BlocksFile, targetBlocksMap : Map<String, UpdateItemsDialog.Bal>, debugMode : Boolean) {
        for (i in 0 until file.blocks.size) {
            val (element, oldBlock, _) = file.blocks[i]
            if(debugMode && oldBlock == null) println("parse block from xml definition $i")
            val block = oldBlock ?: Block.fromXML(element)
            val blockId = block.BlockId?.toString() ?: ""
            if(blockId.isNotBlank() && targetBlocksMap.containsKey(blockId)){
                if(debugMode) println("update $i definition (blockId = $blockId)")
                targetBlocksMap.getValue(blockId).companents.entries.forEach {
                    val newValue = it.value.value
                    if (debugMode) println("change ${it.key} = $newValue")
                    if (newValue != null) {
                        val editor = editors[it.key.toLowerCase()]
                        if (editor != null) {
                            when (editor.getInputType()) {
                                Boolean::class.java -> {
                                    (editor as ABlockInputController<*, Boolean>).applyExtBlockChange(block, (newValue == "true" || newValue == "1" || newValue == "ИСТИНА"))
                                }
                                String::class.java -> {
                                    (editor as ABlockInputController<*, String>).applyExtBlockChange(block, newValue.takeIf { it != MainController.STR_HARD_NULL } ?: "")
                                }
                                Color::class.java -> {
                                    (editor as ABlockInputController<*, Color>).applyExtBlockChange(block, Color.valueOf(newValue.takeIf { it != MainController.STR_HARD_NULL } ?: EditorController.emptyColor.toString()))
                                }
                                else -> {
                                    System.err.println("undefined type ${editor.getInputType()} with ${editor.name}")
                                }
                            }
                        } else {
                            System.err.println("cannot find editor for ${it.key}")
                        }
                    }
                }
                file.blocks[i] = Triple(element, block, true)
            } else if(oldBlock == null){
                if(debugMode) println("change block $i (blockId = $blockId) without update")
                file.blocks[i] = Triple(element, block, false)
            }
        }
    }

    private fun setComponentsPriorities(state: ConsoleMain.State){
        val arg = state.getArg()
        if(arg == null) {
            System.err.println("file is required")
            return
        }
        val f = File(arg).takeIf { it.exists() }
        if(f == null) {
            System.err.println("$arg is not file")
            return
        }
        if(state.debugMode) println("start read csv from ${f.absolutePath}")
        CSVReader(f, state.csvDelimiter).use {
            val csv = it.first()
            Game.prior = HashMap()
            csv.apply {
                this.map {
                    it.entries.forEach { Game.prior!![it.key] = it.value as Long }
                }
            }
        }
        if(state.debugMode) println("end read csv")
    }

    private fun editCrafts(state: ConsoleMain.State){
        val file = state.file ?: throw IllegalStateException("cannot edit zero file")
        file.loadBlocksElementsIfNotExists()

        val arg = state.getArg()
        if(arg == null) {
            System.err.println("file is required")
            return
        }
        val f = File(arg).takeIf { it.exists() }
        if(f == null) {
            System.err.println("$arg is not file")
            return
        }
        if (Game.prior == null) {
            System.err.println("need set components priorities before edit crafts")
            return
        }

        if(state.debugMode) println("start read csv from ${f.absolutePath}")

        val csvReader = CSVReader(f, state.csvDelimiter);
        val balls = Game.EditCrafts(csvReader);
        csvReader.close();

        if(state.debugMode) println("end read csv")
        val targetBlocksMap = balls.map { it.blockId.value to it }.toMap()
        for(i in 0 until file.blocks.size){
            val (element, oldBlock, _) = file.blocks[i]
            if(state.debugMode && oldBlock == null) println("parse block from xml definition $i")
            val block = oldBlock ?: Block.fromXML(element).apply { file.blocks[i] = Triple(element, this, false) }
            val blockId = block.BlockId?.toString() ?: ""

            if (blockId.isBlank()) {
                println("Skipping $i definition (Id is blank)")
                continue
            }

            if (!targetBlocksMap.containsKey(blockId)) {
                if(state.debugMode) println("Skipping not needed block $i! (blockId = $blockId)")
                continue
            }
            if(state.debugMode) println("add craft to $i definition (blockId = $blockId)")
            addCraft (block, blockId, targetBlocksMap)
            file.blocks[i] = Triple(element, block, true)
        }

    }

    private fun editCrafts2 (state: ConsoleMain.State){
        val file = state.file ?: throw IllegalStateException("cannot edit zero file")
        file.loadBlocksElementsIfNotExists()

        val arg = state.getArg()
        if(arg == null) {
            System.err.println("file is required")
            return
        }
        val f = File(arg).takeIf { it.exists() }
        if(f == null) {
            System.err.println("$arg is not file")
            return
        }
        if (Game.prior == null) {
            System.err.println("need set components priorities before edit crafts")
            return
        }

        if(state.debugMode) println("start read csv from ${f.absolutePath}")

        val csvReader = CSVReader(f, state.csvDelimiter);
        val balls = Game.EditCrafts2(csvReader);
        csvReader.close();

        if(state.debugMode) println("end read csv")
        val targetBlocksMap = balls.map { it.blockId.value to it }.toMap()
        for(i in 0 until file.blocks.size){
            val (element, oldBlock, _) = file.blocks[i]
            if(state.debugMode && oldBlock == null) println("parse block from xml definition $i")
            val block = oldBlock ?: Block.fromXML(element).apply { file.blocks[i] = Triple(element, this, false) }
            val blockId = block.BlockId?.toString() ?: ""

            if (blockId.isBlank()) {
                println("Skipping $i definition (Id is blank)")
                continue
            }

            if (!targetBlocksMap.containsKey(blockId)) {
                if(state.debugMode) println("Skipping not needed block $i! (blockId = $blockId)")
                continue
            }
            if(state.debugMode) println("add craft to $i definition (blockId = $blockId)")
            addCraft (block, blockId, targetBlocksMap)
            file.blocks[i] = Triple(element, block, true)
        }

    }


    private fun editCrafts3 (state: ConsoleMain.State){
        val file = state.file ?: throw IllegalStateException("cannot edit zero file")
        file.loadBlocksElementsIfNotExists()

        val arg = state.getArg()
        val arg2 = state.getArg()
        if(arg == null) {
            System.err.println("file is required")
            return
        }
        if(arg2 == null) {
            System.err.println("file is required")
            return
        }
        val f = File(arg).takeIf { it.exists() }
        if(f == null) {
            System.err.println("$arg is not file")
            return
        }
        val f2 = File(arg2).takeIf { it.exists() }
        if(f2 == null) {
            System.err.println("$arg is not file")
            return
        }

        if (Game.prior == null) {
            System.err.println("need set components priorities before edit crafts")
            return
        }

        if(state.debugMode) println("start read csv from ${f.absolutePath}")

        val csvReader = CSVReader(f, state.csvDelimiter);
        val csvReader2 = CSVReader(f2, state.csvDelimiter);
        val data = Game.EditCrafts3(csvReader, csvReader2);
        val balls = data.first;
        var props = data.second;
        csvReader.close();
        csvReader2.close();

        editProperties(file, props, true);

        if(state.debugMode) println("end read csv")
        val targetBlocksMap = balls.map { it.blockId.value to it }.toMap()
        for (i in 0 until file.blocks.size) {
            val (element, oldBlock, _) = file.blocks[i]
            if(state.debugMode && oldBlock == null) println("parse block from xml definition $i")
            val block = oldBlock ?: Block.fromXML(element).apply { file.blocks[i] = Triple(element, this, false) }
            val blockId = block.BlockId?.toString() ?: ""

            if (blockId.isBlank()) {
                println("Skipping $i definition (Id is blank)")
                continue
            }

            if (!targetBlocksMap.containsKey(blockId)) {
                if(state.debugMode) println("Skipping not needed block $i! (blockId = $blockId)")
                continue
            }
            if(state.debugMode) println("add craft to $i definition (blockId = $blockId)")
            addCraft (block, blockId, targetBlocksMap)
            file.blocks[i] = Triple(element, block, true)
        }

    }
}