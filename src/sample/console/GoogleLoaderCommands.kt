package sample.console

import sample.ConsoleMain
import java.io.File


object GoogleLoaderCommands: ICommands {
    const val LOAD_TO_CSV = "loadSaveCSV"


    val currentJarPath: String by lazy {
        File(this.javaClass.protectionDomain.codeSource.location.toURI()).let {
            if(it.isDirectory) it else it.parentFile
        }.canonicalPath//to run / debug on Idea is in .\out\production\SEBlockEditor
    }

    val javaHome: String by lazy { System.getProperty("java.home") }

    fun getCommands() = arrayOf(LOAD_TO_CSV)

    override fun run(command: String, state: ConsoleMain.State) {
        when(command) {
            LOAD_TO_CSV -> loadToCSV(state)
            else -> System.err.println("$command is not command in ${this.javaClass.name}")
        }
    }

    fun loadToCSV(state: ConsoleMain.State){
        val range = state.getArg() ?: run {
            System.err.println("$LOAD_TO_CSV need first param A1 notation range")
            return
        }

        val dst = state.getArg() ?: run {
            System.err.println("$LOAD_TO_CSV need second param output file")
            return
        }
        if(!dst.contains(File.separatorChar)){
            System.err.println("$LOAD_TO_CSV need second param output file. $dst is not a file path.")
            state.revertGet(1)
            return
        }

        val url = state.getArg() ?: run {
            System.err.println("$LOAD_TO_CSV need three param spreadsheet url")
            return
        }
        if(!url.contains("/spreadsheets/d/")){
            System.err.println("$LOAD_TO_CSV need three param spreadsheet url. $url is not valid.")
            state.revertGet(1)
            return
        }

        val csvDelimiter = when(state.csvDelimiter){
            '\t' -> "\\t"
            '\r' -> "\\r"
            '\n' -> "\\n"
            '\b' -> "\\b"
            else -> state.csvDelimiter.toString()
        }
        if(state.debugMode) println("prepare commands to GoogleLoader")
        val cmd = prepareLoadToCsvCommand(state.debugMode, csvDelimiter, range, dst, url)
        if(state.debugMode) println("run ${cmd.joinToString(separator = " ") { if(it == cmd[0]) it else "\"$it\"" }}")
        runCommand(*cmd)
        if(state.debugMode) println("end run")
    }

    private fun prepareLoadToCsvCommand(debug: Boolean, csvDelimiter: String, range: String, dst: String, url: String): Array<String> = arrayOf(
        "${javaHome}\\bin\\java.exe", "-jar", "$currentJarPath\\GoogleLoader\\GoogleLoader.jar"
        , "debug", if(debug) "on" else "off"
        , "setGS", "cred", "$currentJarPath\\GoogleLoader\\credentials.json", "tokens", "$currentJarPath\\GoogleLoader\\tokens", "appName", "SEBlockEditor"
        , "CSVD", csvDelimiter
        , "loadSaveCSV", range, dst, url)

    private fun runCommand(vararg command: String){
        val proc = ProcessBuilder(*command)
            .redirectInput(ProcessBuilder.Redirect.INHERIT)
            .redirectOutput(ProcessBuilder.Redirect.INHERIT)
            .redirectError(ProcessBuilder.Redirect.INHERIT)
            .start()
        proc.waitFor()
    }
}