package sample.console

import com.sun.javafx.application.LauncherImpl
import sample.ConsoleMain
import sample.Main

object GlobalCommands: ICommands {
    const val DEBUG_MODE = "debug"
    const val CHANGE_CSV_DELIMITER = "CSVD"
    const val RUN_GUI = "runGui"

    const val ARG_ON = "on"
    const val ARG_OFF = "off"

    const val ARG_NOW = "now"
    const val ARG_LATER = "later"
    fun getCommands() = arrayOf(DEBUG_MODE, CHANGE_CSV_DELIMITER, RUN_GUI)

    override fun run(command: String, state: ConsoleMain.State) {
        when(command){
            DEBUG_MODE -> setDebugMode(state)
            CHANGE_CSV_DELIMITER -> changeCsvDelimiter(state)
            RUN_GUI -> enableRunGui(state)
            else -> System.err.println("$command is not command in ${this.javaClass.name}")
        }
    }

    private fun setDebugMode(state: ConsoleMain.State){
        state.debugMode = when(state.getArg()){
            null -> !state.debugMode
            ARG_ON -> true
            ARG_OFF -> false
            else -> {
                state.revertGet(1)
                !state.debugMode
            }
        }
        state.file?.apply { debugMode = state.debugMode }
    }

    private fun changeCsvDelimiter(state: ConsoleMain.State){
        val delimArg = state.getArg()
        if(delimArg.isNullOrEmpty()){
            System.err.println("delimiter is required")
            return
        }

        if(delimArg.length != 1 && (delimArg.length != 2 || delimArg[0] != '\\')){
            System.err.println("\"$delimArg\" has more then 1 character")
            state.revertGet(1)
            return
        }

        val newDelim = if (delimArg.length == 1) {
            delimArg[0]
        } else {//delimArg.length == 2
            when (delimArg[1]) {//delimArg[0] == '\\'
                't' -> '\t'
                'r' -> '\r'//??
                'n' -> '\n'//??
                'b' -> '\b'
                else -> {
                    System.err.println("\"$delimArg\" has undefined character")
                    state.revertGet(1)
                    state.csvDelimiter
                }
            }
        }
        if(state.debugMode) println("change csv delimiter from '${state.csvDelimiter}' to '$newDelim'")
        state.csvDelimiter = newDelim
    }

    private fun enableRunGui(state: ConsoleMain.State){
        val now = when(state.getArg()){
            null -> false
            ARG_NOW -> true
            ARG_LATER -> false
            else -> {
                state.revertGet(1)
                false
            }
        }
        if(state.guiRunned){
            if(state.debugMode) System.err.println("cannot run gui more then once per start application")
            return
        }
        if(now) {
            if (state.debugMode) println("start gui now${if(state.runGui) " (cancel start gui later)" else ""}")
            state.runGui = false
            state.saveFileToGlobal()
            LauncherImpl.launchApplication(Main::class.java, arrayOf())
            state.guiRunned = true
        } else {
            if (state.debugMode) println("gui state ${state.runGui} -> true")
            state.runGui = true
        }
    }
}