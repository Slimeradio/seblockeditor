package sample.console

import helpers.firstChildren
import helpers.write2
import org.w3c.dom.Comment
import org.w3c.dom.Element
import sample.ConsoleMain
import sample.data.Block
import java.io.File

object FilesCommands : ICommands {
    const val OPEN_FILE = "importfile"
    const val SAVE_FILE = "exportfile"
    const val CLOSE_FILE = "closefile"
    const val SAVE_FILE2 = "exportfile-nocomments"

    const val ARG_SELF = "self"

    fun getCommands() = arrayOf(OPEN_FILE, SAVE_FILE, SAVE_FILE2, CLOSE_FILE)
    override fun run(command: String, state: ConsoleMain.State){
        when(command){
            OPEN_FILE -> openFile(state)
            SAVE_FILE -> saveFile(state)
            SAVE_FILE2 -> saveFile(state, true)
            CLOSE_FILE -> closeFile(state)
            else -> System.err.println("$command is not command in ${this.javaClass.name}")
        }
    }


    fun closeFile (state: ConsoleMain.State) {
        state.file = null;
    }

    fun openFile(state: ConsoleMain.State){
        val sfile = state.file

        if(sfile != null) {
            System.err.println("file already opened (${sfile.path})")
            return
        }
        val arg = state.getArg()
        if(arg == null) {
            System.err.println("file path is required")
            return
        }
        if(state.debugMode) println("try open file $arg")
        val f = File(arg).takeIf { it.exists() }
        if(f == null) {
            System.err.println("$arg is not file")
            return
        }

        val bf = ConsoleMain.State.BlocksFile(f)
        bf.debugMode = state.debugMode
        state.file = bf
        if(state.debugMode) println("file $arg opened")
    }

    fun saveFile(state: ConsoleMain.State, removeComment: Boolean = false){
        val sfile = state.file

        if(sfile == null) {
            System.err.println("cannot export undefined file")
            return
        }

        sfile.saveBlocksElementsToTags()

        if(state.debugMode) println("generate xml string")

        val definition = sfile.document.childNodes.item(0).firstChildren { it.nodeName == "CubeBlocks" }!!
        val nodes = definition.childNodes;
        for (x in 0..nodes.length) {
            val nd = nodes.item(x);
            if (nd is Element && nd.nodeName == "Definition") {
                nd.setAttribute("id", Block.fromXML(nd).BlockId.toString())
            }
        }

        if (removeComment) {
            var x=0;
            while (x<nodes.length) {
                val nd = nodes.item(x);
                if (nd is Comment) {
                    definition.removeChild(nd);
                } else {
                    x++;
                }
            }
        }


        val out = sfile.document.write2()

        val f = when(val arg = state.getArg()) { //by default export to self?
            null -> {
                if(state.debugMode) println("no file arg. Use export to self")
                File(sfile.path)
            }
            ARG_SELF -> {
                if(state.debugMode) println("Use export to self")
                File(sfile.path)
            }
            else -> if(arg.contains(File.separatorChar)) {
                if(state.debugMode) println("Use export to $arg")
                File(arg)
            } else {
                if(state.debugMode) println("$arg is not a file. Use export to self")
                state.revertGet(1)
                File(sfile.path)
            }
        }
        if(state.debugMode) println("create temp output file")
        val newFile = File.createTempFile("blocks_", null, f.parentFile)
        if(state.debugMode) println("write xml to ${newFile.absolutePath}")
        newFile.outputStream().bufferedWriter(Charsets.UTF_8).use {
            it.write(out)
            it.flush()
        }
        if(f.exists()) {
            if(state.debugMode) println("${f.absolutePath} already exists. Delete.")
            f.delete()
        }
        if(state.debugMode) println("rename ${newFile.absolutePath} to ${f.absolutePath}")
        newFile.renameTo(f)
    }
}