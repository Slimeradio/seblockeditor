package sample.console

import sample.ConsoleMain

interface ICommands {
    fun run(command: String, state: ConsoleMain.State)
}