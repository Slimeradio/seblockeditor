package sample

import helpers.CSVReader
import javafx.fxml.FXML
import javafx.fxml.Initializable
import javafx.scene.control.Button
import javafx.scene.control.MenuItem
import javafx.scene.control.TableColumn
import javafx.scene.control.TableView
import javafx.scene.input.Clipboard
import sample.data.Bal
import sample.data.BalItem
import java.net.URL
import java.util.*

class UpdateItemsComponentsDialog: Initializable {

    @FXML
    lateinit var bal: TableView<Bal>

    @FXML
    lateinit var ImportBalClipboard: MenuItem

    @FXML
    lateinit var btnApply: Button

    lateinit var listener: (blockTypes: List<Bal>) -> Unit

    override fun initialize(location: URL?, resources: ResourceBundle?) {
        val bal2ColCount = bal.columns.size
        bal.columns.apply {
            (find { it.id == "BLOCK" } as TableColumn<Bal, String>).setCellValueFactory{ obj -> obj!!.value.blockId }
        }
        ImportBalClipboard.setOnAction {
            val csvReader = CSVReader(Clipboard.getSystemClipboard().string.reader().buffered(), '\t')
            csvReader.first().apply {
                bal.items.clear()
                val header = getHeader()
                bal.columns.subList(bal2ColCount, bal.columns.size).clear()

                var last = ""
                var j = 0
                val prepHeader = header.copyOfRange(bal2ColCount, header.size).map {
                    if(it != "null"){
                        last = it
                        j = 0
                    }
                    last to when(j++){
                        0 -> Bal.STR_FULL_COUNT
                        1 -> Bal.STR_PRIORITY
                        2 -> Bal.STR_ADDITIONAL
                        3 -> Bal.STR_ADDITIONAL_PRIORITY
                        else -> ""
                    }
                }
                setHeader(arrayOf(*header.copyOfRange(0, bal2ColCount), *prepHeader.map { "${it.first}\t${it.second}" }.toTypedArray()))
                val splitHeader = prepHeader.groupBy { it.first }.mapValues { it.value.map { it.second } }
                for(sh in splitHeader){
                    bal.columns.add(bal2Cell(sh.key, sh.value))
                }
                bal.items.addAll(this.map {
                    Bal().apply {
                        blockId.value = it[header[0]]?.toString()
                        for(sh in splitHeader){
                            (companents[sh.key] ?: BalItem().apply { companents[sh.key] = this }).apply {
                                fullCount.value = (it["${sh.key}\t${Bal.STR_FULL_COUNT}"] as? Number ?: 0).toInt()
                                priority.value = (it["${sh.key}\t${Bal.STR_PRIORITY}"] as? Number ?: 0).toInt()
                                additional.value = (it["${sh.key}\t${Bal.STR_ADDITIONAL}"] as? Number ?: 0).toInt()
                                additionalPriority.value = (it["${sh.key}\t${Bal.STR_ADDITIONAL_PRIORITY}"] as? Number ?: 0).toInt()
                            }
                        }
                    }
                })
                println()
            }
            if(bal.items.isNotEmpty()){
                btnApply.isDisable = false
            }
        }
        btnApply.setOnAction {
            listener(bal.items)
        }
    }

    fun bal2Cell(name: String, names: List<String>): TableColumn<Bal, BalItem> = TableColumn<Bal, BalItem>(name).apply {
        names.filter { it in arrayOf(Bal.STR_FULL_COUNT, Bal.STR_PRIORITY, Bal.STR_ADDITIONAL, Bal.STR_ADDITIONAL_PRIORITY) }.map {
            TableColumn<Bal, Number>(it).apply {
                when(it) {
                    Bal.STR_FULL_COUNT -> {
                        setCellValueFactory { obj -> obj.value.companents[name]!!.fullCount }
                    }
                    Bal.STR_PRIORITY -> {
                        setCellValueFactory { obj -> obj.value.companents[name]!!.priority }
                    }
                    Bal.STR_ADDITIONAL -> {
                        setCellValueFactory { obj -> obj.value.companents[name]!!.additional }
                    }
                    Bal.STR_ADDITIONAL_PRIORITY -> {
                        setCellValueFactory { obj -> obj.value.companents[name]!!.additionalPriority }
                    }
                }
            }
        }.let { columns.addAll(it) }
    }



    
}