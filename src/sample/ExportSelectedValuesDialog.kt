package sample

import javafx.fxml.FXML
import javafx.fxml.Initializable
import javafx.scene.control.Button
import javafx.scene.control.CheckBox
import javafx.scene.layout.Pane
import javafx.scene.paint.Color
import sample.data.Block
import java.net.URL
import java.util.*

class ExportSelectedValuesDialog: Initializable {

    @FXML
    lateinit var cCheckboxContainer: Pane

    @FXML
    lateinit var chSelectAll: CheckBox

    @FXML
    lateinit var chTypeIdSubtypeId: CheckBox

    @FXML
    lateinit var btnApply: Button

    lateinit var exportColumns: ArrayList<Pair<CheckBox, (Block) -> Any?>>

    lateinit var listener: (List<Pair<String, (Block) -> Any?>>) -> Unit

    private var chUpdated = false

    override fun initialize(location: URL?, resources: ResourceBundle?) {
        exportColumns = arrayListOf(
                chTypeIdSubtypeId to { bl: Block -> bl.BlockId?.toString() }
        )

        chSelectAll.selectedProperty().addListener{_, _, newValue ->
            if(!chUpdated){
                exportColumns.forEach {
                    it.first.isSelected = newValue
                }
            }
        }

        updateApplyEnable()
        exportColumns.forEach {
            it.first.selectedProperty().addListener { _, _, _ ->
                updateApplyEnable()
            }
        }

        btnApply.setOnAction {
            listener(exportColumns.filter { it.first.isSelected }.map { it.first.text to it.second })
        }
    }

    fun initColumns(controllers: List<ABlockInputController<*, *>>){
        val exportColumns = controllers.map {
            val name = it.name
            val checkbox = CheckBox(name)
            checkbox.isSelected = true
            cCheckboxContainer.children.add(checkbox)
            checkbox to when(it.getInputType()){
                Color::class.java -> {bl: Block -> it.getExtBlockValue(bl).takeIf { it != EditorController.emptyColor }}
                Boolean::class.java -> {bl: Block -> it.getExtBlockValue(bl)}
                else -> {bl: Block -> it.getExtBlockValue(bl)?.takeIf { it.toString().isNotEmpty() }}
            }
        }
        this.exportColumns.addAll(exportColumns.sortedBy { it.first.text })
        exportColumns.forEach {
            it.first.selectedProperty().addListener { _, _, _ ->
                updateApplyEnable()
            }
        }
    }

    private fun updateApplyEnable(){
        btnApply.isDisable = !exportColumns.any { it.first.isSelected }
        chUpdated = true
        chSelectAll.isSelected = exportColumns.all { it.first.isSelected }
        chUpdated = false
    }
}